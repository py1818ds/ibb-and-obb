package game_object.item_object;

import controllers.AudioResourceController;
import controllers.SceneController;
import utils.Path;

import java.awt.*;

public class Axe extends Item {

    public Axe(int x, int y, int width, int height) {
        super(x, y, width, height,1,1);
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().objs().axe());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.AXE;
    }

    @Override
    public void shot() {
        AudioResourceController.getInstance().shot(new Path().sounds().coin());
    }

}
