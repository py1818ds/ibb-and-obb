package game_object.item_object;

import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GameObject;
import utils.Path;

import java.awt.*;

public class BellMagnet extends Item {

    public BellMagnet(int x, int y, int width, int height) {
        super(x, y, width, height,1, 1);
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().objs().bell());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.BELL_MAGNET;
    }

    @Override
    public void shot() {
        AudioResourceController.getInstance().shot(new Path().sounds().coin());
    }

}
