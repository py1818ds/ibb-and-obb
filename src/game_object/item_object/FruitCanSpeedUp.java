package game_object.item_object;

import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GameObject;
import utils.Delay;
import utils.Path;

import java.awt.*;

public class FruitCanSpeedUp extends Item {

    public FruitCanSpeedUp(int x, int y, int width, int height) {
        super(x, y, width, height,1,1);
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().objs().alcohol());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.FRUIT;
    }

    @Override
    public void shot() {
        AudioResourceController.getInstance().shot(new Path().sounds().coin());
    }
}
