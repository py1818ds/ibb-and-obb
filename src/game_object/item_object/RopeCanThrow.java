package game_object.item_object;

import controllers.SceneController;
import game_object.Rect;
import game_object.GravityState;

import game_object.map_object.FloorNormal;
import utils.Path;
import utils.Global.Direction;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class RopeCanThrow extends Item {

    private int speedX;
    private int speedY;
    private int acceleration;
    private HookState hookState;
    private final Direction direction;
    private final GravityState gravityState;
    private final int recordXForFly;
    private final int recordYForFly;


    public enum HookState{
        FISHING,
        GOTCHA,
        FLYING;
    }

    public RopeCanThrow(int x, int y, Direction direction, GravityState gravityState) {
        super(x, y-16, 20, 20,1,1);
        recordXForFly = x;
        recordYForFly = y;
        //預設往右上飛
        speedX = 10;
        speedY = -15;
        acceleration = 2;
        this.direction = direction;
        this.gravityState = gravityState;
        adjustFlyingDirection();
        hookState = HookState.FISHING;

    }
    private void adjustFlyingDirection(){
        if (direction == Direction.LEFT){
            speedX *= -1;
        }
        if (gravityState == GravityState.ANTI_GRAVITY){
            speedY *= -1;
            acceleration *= -1;
        }
    }

    public void hitFloors(ArrayList<FloorNormal> floors) {
        for (FloorNormal floor : floors) {
            //完全沒碰到就設定為掉落狀態
            if (isCollision(floor)) {
                Rect actorCollider = collider();
                Rect floorCollider = floor.collider();

                switch (floor.floorDirection()) {
                    case UP, LEFT_UP_CONCAVE, RIGHT_UP_CONCAVE -> {
                        translateY(-(actorCollider.bottom() - floorCollider.top()));
                        if(gravityState == GravityState.NORMAL_GRAVITY){
                            translateY(-1);
                        }else {
                            translateY(+1);
                        }
                    }
                    case DOWN, LEFT_DOWN_CONCAVE, RIGHT_DOWN_CONCAVE -> {//向下推
                        // System.out.println("=============下");
                        translateY(floorCollider.bottom() - actorCollider.top());
                        if(gravityState == GravityState.ANTI_GRAVITY){
                            translateY(1);
                        }else {
                            translateY(-1);
                        }
                    }
                    case LEFT -> {//向左推
                        // System.out.println("=============左");
                        translateX(-(actorCollider.right() - floorCollider.left())-1);
                    }
                    case RIGHT -> {//向右推
                        // System.out.println("=============右");
                        translateX(floorCollider.right() - actorCollider.left()+1);
                    }
                    case LEFT_UP_CONVEX -> {//判斷左上
//                        System.out.println("=============左 上 凸");
                        // 先判斷要往左還是上推
                        if (actorCollider.bottom() - speedY <= floorCollider.top()) {
//                            System.out.println("高於左上凸地板");
                            translateY(-(actorCollider.bottom() - floorCollider.top()));
                            if(gravityState == GravityState.NORMAL_GRAVITY){
                                translateY(-1);
                            }else {
                                translateY(+1);
                            }
                        } else {
                            //      System.out.println("向左噴");
                            translateX(-(actorCollider.right() - floorCollider.left()) - speedX - 1);
                        }
                    }
                    case RIGHT_UP_CONVEX -> {//判斷右上
                        //    System.out.println("=============右 上 凸");
                        if (actorCollider.bottom() - speedY <= floorCollider.top()) {
                            translateY(-(actorCollider.bottom() - floorCollider.top()));
                            if(gravityState == GravityState.NORMAL_GRAVITY){
                                translateY(-1);
                            }else {
                                translateY(1);
                            }
                        } else {
                            translateX(floorCollider.right() - actorCollider.left() + 1);
                        }
                    }
                    case LEFT_DOWN_CONVEX -> {//判斷左下
                        //    System.out.println("=============左 下 凸");
                        if (actorCollider.top() - speedY >= floorCollider.bottom()) {
                            translateY(floorCollider.bottom() - actorCollider.top());
                            if(gravityState == GravityState.NORMAL_GRAVITY){
                                translateY(1);
                            }else {
                                translateY(-1);
                            }
                        } else {
                            //       System.out.println("向左噴");
                            translateX(-(actorCollider.right() - floorCollider.left())-1);
                        }
                    }
                    case RIGHT_DOWN_CONVEX -> {//判斷右下
                        //  System.out.println("=============右 下 凸");
                        if (actorCollider.top() - speedY >= floorCollider.bottom()) {
                            translateY(floorCollider.bottom() - actorCollider.top());
                            if(gravityState == GravityState.NORMAL_GRAVITY){
                                translateY(1);
                            }else {
                                translateY(-1);
                            }
                        } else {
                            translateX(floorCollider.right() - actorCollider.left()+1);
                        }
                    }
                }//switch
                speedX = 0 ;
                speedY = 0 ;
                acceleration = 0;
//                recordXForFly = collider().left(); // 紀錄character當前的left
//                recordYForFly = collider().top(); //紀錄character當前的top
                hookState = HookState.GOTCHA;
            }//if 這一塊floor isNextCollision 就執行switch
        }//for floor
    }

    public int flyX(){
        return (collider().left() - recordXForFly) / 25;
    }

    public int flyY(){
        return (collider().top() - recordYForFly) / 6;
    }

    public HookState hookState(){
        return hookState;
    }
    public void setHookState(HookState hookState) {
        this.hookState = hookState;
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().actors().enemy_Pumpkin());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.ROPE_CAN_THROW;
    }

    @Override
    public void shot() {

    }

    @Override
    public void paintComponent(Graphics g) {
        if (hookState == HookState.FISHING) {
            AffineTransform affineTransform = AffineTransform.getTranslateInstance(painter().left(), painter().top());
            double arc = Math.atan((double)speedY/speedX);
            affineTransform.rotate(arc,(double) collider().getWidth()/2,(double)painter().getHeight()/2);
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(img,affineTransform,null);
        }else if (hookState == HookState.GOTCHA) {
            g.drawImage(img,painter().left(), painter().top(),painter().getWidth(),painter().getHeight(),null);
        }
    }

    @Override
    public void update() {
        if (hookState == HookState.FISHING) {
            translate(speedX, speedY);
            speedY += acceleration;
        }

    }

}
