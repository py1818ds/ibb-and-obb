package game_object.item_object;

import controllers.AudioResourceController;
import controllers.SceneController;
import utils.Path;

import java.awt.*;

public class Coin extends Item {

    public Coin(int x, int y, int width, int height) {
        super(x, y, width, height,6,8);
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().objs().coin());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.COIN;
    }

    @Override
    public void shot() {
        AudioResourceController.getInstance().shot(new Path().sounds().coin());
    }

}
