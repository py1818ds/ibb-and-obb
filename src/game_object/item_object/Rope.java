package game_object.item_object;

import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GameObject;
import utils.Path;

import java.awt.*;

public class Rope extends Item {


    public Rope(int x, int y, int width, int height) {
        super(x, y, width, height,1,1);
    }

    @Override
    public Image setImg() {
        return SceneController.instance().irc().tryGetImage(new Path().img().objs().hook());
    }

    @Override
    public ItemType setItemType() {
        return ItemType.ROPE;
    }

    @Override
    public void shot() {
        AudioResourceController.getInstance().shot(new Path().sounds().coin());
    }

}
