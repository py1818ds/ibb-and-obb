package game_object.item_object;

import game_object.GameObject;
import game_object.State;
import utils.Delay;
import utils.Global;


import java.awt.*;

public abstract class Item extends GameObject {

    protected final Image img;
    protected Delay delay;
    protected int count;//圖以X軸方向組合 ex: [img1,img2,img3,...,imgCount] >> 是一張圖
    protected final int imgNumber;
    protected final ItemType itemType;
    protected State itemState;


    public enum ItemType{
        AXE,
        BELL_MAGNET,
        COIN,
        FRUIT,
        ROPE,
        ROPE_CAN_THROW,
        STAR;
    }

    // 設定動畫
    // 輸入圖的數量以及更換頻率(幀)
    public Item(int x, int y, int width, int height, int imgNumber, int delayForAnimation) {
        super(x, y, width, height);
        this.img = setImg();
        count = 0;
        delay = new Delay(delayForAnimation);
        this.imgNumber = imgNumber;
        delay.loop();
        itemState = State.EXIST;
        itemType = setItemType();
    }
    //不需要動畫可用imgNumber=1才代替多載
//    public Item(int x, int y, int width, int height) {
//        super(x, y, width, height);
//        this.img = setImg();
//        imgNumber = 1;
//        itemState = ItemState.NORMAL;
//    }


    public abstract Image setImg();

    public abstract ItemType setItemType();

    public abstract void shot();

    @Override
    public void paintComponent(Graphics g) {
        //圖以X軸方向組合 ex: [img1,img2,img3,...,imgCount] >> 是一張圖
        g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                Global.MAP_UNIT_X * count,
                0,
                Global.MAP_UNIT_X * (count+1),
                Global.MAP_UNIT_Y,
                null);
    }

    @Override
    public void update() {
        //一張圖不用更新
        if(imgNumber==1){
            return;
        }
        //更新時間到就換圖
        if (delay.count()) {
            count++;
            count %= imgNumber;
        }
    }

    public ItemType itemType(){
        return itemType;
    }

}
