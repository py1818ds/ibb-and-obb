package game_object;

import utils.*;


import java.awt.*;

public abstract class GameObject implements GameKernel.GameInterface {

    private Rect collider;
    private Rect painter;

    public GameObject(int x, int y, int width, int height) {
        this(x, y, width, height, x, y, width, height);
    }

    public GameObject(int x, int y, int width, int height,
                      int x2, int y2, int width2, int height2) {
        collider = Rect.genWithCenter(x, y, width, height);
        painter = Rect.genWithCenter(x2, y2, width2, height2);
        collider.setCenter(painter.centerX(), painter.centerY());
    }

    public GameObject(Rect rect) {
        collider = rect.clone();
        painter = rect.clone();
    }

    public GameObject(Rect rect, Rect rect2) {
        collider = rect.clone();
        painter = rect2.clone();
        painter.setCenter(rect.centerX(), rect.centerY());
    }

    public void resetRect(Rect rect){
        painter = new Rect(rect);
        collider = new Rect(rect);
    }

    public boolean isCollision(GameObject obj) {
        return collider.overlap(obj.collider);
    }

    public final void translate(int x, int y) {
        collider.translate(x, y);
        painter.translate(x, y);

    }

    public final void translateX(int x) {
        collider.translateX(x);
        painter.translateX(x);
    }

    public final void translateY(int y) {
        collider.translateY(y);
        painter.translateY(y);
    }

    public Rect collider() {
        return this.collider;
    }

    public Rect painter() {
        return this.painter;
    }

    public boolean touchTop() {
        return collider.top() <= 0;
    }

    public boolean touchRight() {
        return collider.right() >= Global.SCREEN_X;
    }

    public boolean touchLeft() {
        return collider.left() <= 0;
    }

    public boolean touchBottom() {
        return collider.bottom() >= Global.SCREEN_Y;
    }

    @Override
    public void paint(Graphics g) {
        paintComponent(g);
        if (Global.IS_DEBUG) {
            g.setColor(Color.RED);
            collider.paint(g);
//            g.setColor(Color.GREEN);
//            painter.paint(g);
            g.setColor(Color.DARK_GRAY);
        }
    }


    public abstract void paintComponent(Graphics g);

    @Override
    public void update() {

    }
}
