package game_object;

import java.awt.*;

public class Rect {

    private int x;
    private int y;
    private int width;
    private int height;

    public Rect(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public final Rect translate(int x, int y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public final Rect translateX(int x) {
        this.x += x;
        return this;
    }

    public final Rect translateY(int y) {
        this.y += y;
        return this;
    }

    public Rect(Rect rect){
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
    }

    public final Rect scale(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public final Rect scaleX(int width) {
        this.width = width;
        return this;
    }

    public final Rect scaleY(int height) {
        this.height = height;
        return this;
    }

    public final int centerX() {
        return x + width / 2;
    }

    public final int centerY() {
        return y + height / 2;
    }

    public static Rect genWithCenter(int x, int y, int width, int height) {
        Rect rect = new Rect(x - width / 2, y - height / 2, width, height);
        return rect;
    }

    public final void setCenter(int x, int y) {
        this.x = x - width / 2;
        this.y = y - height / 2;
    }

    public final int getWidth() {
        return this.width;
    }

    public final int getHeight() {
        return this.height;
    }

    public final int left() {
        return this.x;
    }

    public final int right() {
        return this.x + width;
    }

    public final int top() {
        return this.y;
    }

    public final int bottom() {
        return this.y + height;
    }

    public final boolean overlap(Rect obj) {
        if (right() < obj.left()) {
            return false;
        }
        if (left() > obj.right()) {
            return false;
        }
        if (top() > obj.bottom()) {
            return false;
        }
        if (bottom() < obj.top()) {
            return false;
        }
        return true;
    }

    public final void paint(Graphics g) {
        g.drawRect(x, y, width, height);
    }

    public final Rect clone() {
        return new Rect(left(), right(), getWidth(), getHeight());
    }
    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }


}
