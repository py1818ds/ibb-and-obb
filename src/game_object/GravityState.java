package game_object;

public enum GravityState {
    NORMAL_GRAVITY(0),
    ANTI_GRAVITY(1);

    private final int value;
    private GravityState(int value){
        this.value = value;
    }
    public static GravityState getGravityState(int index){
        return index==0 ? NORMAL_GRAVITY : ANTI_GRAVITY;
    }
    public int value(){
        return value;
    }
}