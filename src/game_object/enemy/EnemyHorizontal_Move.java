package game_object.enemy;

import animation.Animation_pumpkin;
import controllers.SceneController;
import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;
import utils.Delay;
import utils.Global;
import utils.Global.Direction;
import utils.Path;

import java.awt.*;

public class EnemyHorizontal_Move extends Enemy {
    private Image img;
    private Direction dir;
    private Delay delay;
    private Delay delayWait;
    private int moveSpeed;
    private Delay delayForAnimation;
    private int count;
    private GameObject clone;
    private Image white;


    //跳蚤
    public EnemyHorizontal_Move(int x, int y, GravityState gravityState) {
        // 單位是32 但放大到48 所以位置會差16 置中要除2所以是-8
        super(x, y, 48, 48, x, y, 48, 48);
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().enemy_Pumpkin());
        white = SceneController.instance().irc().tryGetImage(new Path().img().actors().white());
        super.gravityState = gravityState;
        moveSpeed = 3;
        dir = Direction.RIGHT;
        delay = new Delay(60);
        delay.play();
        delayWait = new Delay(30);
        delayForAnimation = new Delay(20);
        delayForAnimation.loop();
        count = 0;
        if(gravityState == GravityState.NORMAL_GRAVITY){
            resetRect(new Rect(x-8,y-32,48,48));
            clone = new GameObject(x+16,y+96,32,32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white,collider().left(),collider().top(),32,32,null);
                }
                @Override
                public void update(){
                    if (dir == Direction.RIGHT) {
                        this.translateX(moveSpeed);
                    }
                    if(dir == Direction.LEFT && !touchLeft()){
                        this.translateX(-moveSpeed);
                    }
                }
            };
        }else {
            resetRect(new Rect(x-8,y-16,48,48));
            clone = new GameObject(x+16,y-96,32,32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white,collider().left(),collider().top(),32,32,null);
                }
                @Override
                public void update(){
                    if (dir == Direction.RIGHT) {
                        this.translateX(moveSpeed);
                    }
                    if(dir == Direction.LEFT && !touchLeft()){
                        this.translateX(-moveSpeed);
                    }
                }
            };
        }

        }

    public void move(){
        // 判斷移動多少速度
        if(delay.count()){
            changeDir();
            moveSpeed = 0;
            delay.stop();
            delayWait.play();
        }
        if(delayWait.count()){
            moveSpeed = 3;
            delay.play();
            delayWait.stop();
        }
        if (dir == Direction.RIGHT) {
            translateX(moveSpeed);
        }
        if(dir == Direction.LEFT && !touchLeft()){
            translateX(-moveSpeed);
        }
    }

    public GameObject clone() {
        return clone;
    }

    public void changeDir(){
        if (dir == Direction.LEFT) {
            dir = Direction.RIGHT;
        }
        else if (dir == Direction.RIGHT) {
            dir = Direction.LEFT;
        }
    }

    public void update(){
        if (delayForAnimation.count()) {
            count++;
            count %= 2;
        }
        clone.update();
        move();
    }



    @Override
    public void paintComponent(Graphics g) {
        clone.paint(g);
        if(gravityState == GravityState.NORMAL_GRAVITY){
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    0,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    null);
        }else {
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    0,
                    null);
        }

    }
    
}
