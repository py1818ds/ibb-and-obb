package game_object.enemy;

import java.awt.Graphics;

import controllers.SceneController;

import java.awt.*;

import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;
import utils.Delay;
import utils.Global;
import utils.Path;

import utils.Global.Direction;

public class EnemyHorizontal_Jump extends Enemy {
    private final Image img;
    private Direction dir;
    private final Delay delay;
    private final Delay delayWait;
    private int moveSpeed;
    private int vy;
    private int accelerate; //accelerate
    private final Delay delayForAnimation;
    private int count;

    //跳蚤
    public EnemyHorizontal_Jump(int x, int y, GravityState gravityState) {
        super(x, y, 48, 48, x, y, 48, 48);
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().enemyHorizontal());
        white = SceneController.instance().irc().tryGetImage(new Path().img().actors().white());
        super.gravityState = gravityState;
        moveSpeed = 8;
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            //長方形是以中心點位置new出來的,加上將本來32的物件放大至48,所以要出現在正確的線上要位移48/2 + 48-32/2
            resetRect(new Rect(x - 8, y - 32, 48, 48));
            accelerate = 1; // 加速度
            vy = -15; // 向上的力
            clone = new GameObject(x + 8, y + 96, 32, 32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white, collider().left(), collider().top(), 32, 32, null);
                }

                @Override
                public void update() {
                    if (dir == Direction.RIGHT) {
                        translateX(moveSpeed);
                    }
                    if (dir == Direction.LEFT) {
                        translateX(-moveSpeed);
                    }
                    translateY(-vy);
                }
            };
        } else {
            resetRect(new Rect(x - 8, y - 16, 48, 48));
            accelerate = -1; // 加速度
            vy = 15; // 向上的力
            clone = new GameObject(x + 8, y - 96, 32, 32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white, collider().left(), collider().top(), 32, 32, null);
                }

                @Override
                public void update() {
                    if (dir == Direction.RIGHT) {
                        translateX(moveSpeed);
                    }
                    if (dir == Direction.LEFT) {
                        translateX(-moveSpeed);
                    }
                    translateY(-vy);
                }
            };
        }

        dir = Direction.RIGHT;
        delay = new Delay(30);
        delay.play();
        delayWait = new Delay(90);
        delayForAnimation = new Delay(20);
        delayForAnimation.loop();
        count = 0;


    }

    public void move() {
        // 判斷移動多少速度
        translateY(vy);
        vy += accelerate;

        if (delay.count()) {
            changeDir();
            moveSpeed = 0;
            vy = 0;
            accelerate = 0;
            delay.stop();
            delayWait.play();
        }

        if (delayWait.count()) {
            if (gravityState == GravityState.NORMAL_GRAVITY) {
                accelerate = 1;
                vy = -15;
            } else {
                accelerate = -1;
                vy = 15;
            }
            moveSpeed = 8;
            delay.play();
            delayWait.stop();
        }

        if (dir == Direction.RIGHT) {
            translateX(moveSpeed);
        }
        if (dir == Direction.LEFT) {
            translateX(-moveSpeed);
        }
    }

    public void changeDir() {
        if (dir == Direction.LEFT) {
            dir = Direction.RIGHT;

        } else if (dir == Direction.RIGHT) {
            dir = Direction.LEFT;
        }
    }

    public GameObject clone() {
        return clone;
    }

    public void update() {
        if (delayForAnimation.count()) {
            count++;
            count %= 2;
        }
        clone.update();
        move();
    }

    @Override
    public void paintComponent(Graphics g) {
        clone.paint(g);
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    0,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    null);
        } else {
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    0,
                    null);
        }
    }

}
