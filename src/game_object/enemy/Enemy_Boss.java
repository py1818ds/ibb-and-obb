package game_object.enemy;

import controllers.SceneController;
import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;
import utils.Delay;
import utils.Global;
import utils.Global.Direction;
import utils.Path;

import java.awt.*;

public class Enemy_Boss extends Enemy {
    private final Image img;
    private Direction dir;
    private final Delay delay;
    private final Delay delayWait;
    private int moveSpeed;
    private int vy;
    private int accelerate;
    private final Delay delayForAnimation;
    private int count;

    //跳蚤
    public Enemy_Boss(int x, int y, GravityState gravityState) {
        super(x, y - 160, 400, 480, x, y - 160, 480, 480);
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().boss());
        white = SceneController.instance().irc().tryGetImage(new Path().img().actors().white());
        moveSpeed = 20;
        accelerate = 2; // 加速度
        vy = -30; // 向上的力
        clone = new GameObject(x, y + 224, 160, 160) {
            @Override
            public void paintComponent(Graphics g) {
                g.drawImage(white, collider().left(), collider().top(), 160, 160, null);
            }

            @Override
            public void update() {
                if (dir == Direction.RIGHT) {
                    translateX(moveSpeed);
                }
                if (dir == Direction.LEFT) {
                    translateX(-moveSpeed);
                }
                translateY(-vy);
            }
        };


        dir = Direction.RIGHT;
        delay = new Delay(30);
        delay.play();
        delayWait = new Delay(90);
        delayForAnimation = new Delay(20);
        delayForAnimation.loop();
        count = 0;


    }

    public void move() {
        // 判斷移動多少速度
        translateY(vy);
        vy += accelerate;

        if (delay.count()) {
            changeDir();
            moveSpeed = 0;
            vy = 0;
            accelerate = 0;
            delay.stop();
            delayWait.play();
        }

        if (delayWait.count()) {

            accelerate = 2;
            vy = -30;

            moveSpeed = 20;
            delay.play();
            delayWait.stop();
        }

        if (dir == Direction.RIGHT) {
            translateX(moveSpeed);
        }
        if (dir == Direction.LEFT) {
            translateX(-moveSpeed);
        }
    }

    public void changeDir() {
        if (dir == Direction.LEFT) {
            dir = Direction.RIGHT;

        } else if (dir == Direction.RIGHT) {
            dir = Direction.LEFT;
        }
    }

    public GameObject clone() {
        return clone;
    }

    public void update() {
        if (delayForAnimation.count()) {
            count++;
            count %= 2;
        }
        clone.update();
        move();
    }

    @Override
    public void paintComponent(Graphics g) {
        clone.paint(g);
        g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                160 * count, 0, 160 + 160 * count, 160,
                null);

    }

}
