package game_object.enemy;

import java.awt.Graphics;

import controllers.SceneController;

import java.awt.*;

import game_object.characters.Character;
import utils.Delay;
import utils.Path;

import utils.Global.Direction;

public class EnemyChaser extends Enemy {

    private int moveOnX;
    private int moveOnY;
    private int moveDistance;
    private Character player;
    private Image img;
    private Direction dir;
    private Delay delay;
    private int moveSpeed;
    private int id;
    private boolean isChaser;
    private boolean isChase;
    private float nearest;

    public EnemyChaser(int x, int y, Character actor) {
        super(x, y, 32, 32, x, y, 32, 32);
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().enemyChaser());
        this.player = actor;
        delay = new Delay(180);
        delay.loop();

        isChaser = true;
        isChase = false;
        nearest = 50000f;

        moveSpeed = 4;
        dir = Direction.UP;
    } 

    protected void changeDir(float moveOnX) {
        if (moveOnX > 0) {
            dir = Direction.RIGHT;
        } else {
            dir = Direction.LEFT;
        }
    }


    public boolean isNear(Character player) {
        float dx = Math.abs(player.collider().centerX() - painter().centerX());
        float dy = Math.abs(player.collider().bottom() - painter().centerY());
        float dc = (float) Math.sqrt(dx * dx + dy * dy);//計算怪物與人物的距離
        if (dc < 600) {
            return true;
        }
        return false;
    }

    public void move() {
        if(collider().left() <= player.collider().left()){
            isChaser = false;
        }
        if (isChaser) {
            if(isNear(player)){
                translateX(-moveSpeed);
            }
        }
    }


    public void update(){
        move();
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, painter().left(), painter().top(), painter().getWidth(), painter().getHeight(), null);
    }
    
}
