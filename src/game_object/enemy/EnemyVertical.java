package game_object.enemy;

import java.awt.Graphics;

import controllers.SceneController;

import java.awt.*;

import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;
import utils.Delay;
import utils.Global;
import utils.Path;

import utils.Global.Direction;

public class EnemyVertical extends Enemy {

    private final Image img;
    private Direction dir;
    private final Delay delay;
    private final Delay delayWait;
    private double moveSpeed;
    private final double acceleration;
    private final int originalY;
    private final Delay delayForAnimation;
    private int count;
    private GameObject clone;
    private Image white;

    //垂直跳跳怪
    public EnemyVertical(int x, int y, GravityState gravityState) {
        super(x, y, 48, 48, x, y, 48, 48);
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().enemy_Fly());
        white = SceneController.instance().irc().tryGetImage(new Path().img().actors().white());
        originalY = y;
        delay = new Delay(60);
        delay.loop();
        delayWait = new Delay(30);
        this.gravityState = gravityState;
        if (this.gravityState == GravityState.NORMAL_GRAVITY) {
            resetRect(new Rect(x, y - 32, 48, 48));
            moveSpeed = -10;
            acceleration = 0.5;
            dir = Direction.UP;
            clone = new GameObject(x+24, y + 96, 32, 32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white, collider().left(), collider().top(), 32, 32, null);
                }

                @Override
                public void update() {
                    translateY(-(int) moveSpeed);
                }
            };
        } else {
            resetRect(new Rect(x, y - 16, 48, 48));
            moveSpeed = 10;
            acceleration = -0.5;
            dir = Direction.DOWN;
            clone = new GameObject(x+24, y - 96, 32, 32) {
                @Override
                public void paintComponent(Graphics g) {
                    g.drawImage(white, collider().left(), collider().top(), 32, 32, null);
                }

                @Override
                public void update() {
                    translateY(-(int) moveSpeed);
                }
            };
        }

        delayForAnimation = new Delay(20);
        delayForAnimation.loop();
        count = 0;

    }

    public void move() {

        moveSpeed += acceleration;
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            if (this.collider().top() >= originalY - this.collider().getHeight() / 2 - 16) {
                jump();
            }
        } else {
            if (this.collider().top() <= originalY - this.collider().getHeight() / 2 + 16) {
                jump();
            }
        }

        translateY((int) moveSpeed);

    }

    public void jump() {
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            moveSpeed = -10;
        } else {
            moveSpeed = 10;
        }

    }

    public GameObject clone() {
        return clone;
    }

    public void changeDir() {
        if (dir == Direction.DOWN) {

            dir = Direction.UP;
            //System.out.println(dir);

        } else if (dir == Direction.UP) {

            dir = Direction.DOWN;
            // System.out.println(dir);
        }
    }

    public void update() {
        if (delayForAnimation.count()) {
            count++;
            count %= 2;
        }
        clone.update();
        move();
    }

    @Override
    public void paintComponent(Graphics g) {
        clone.paint(g);
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    0,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    null);
        } else {
            g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                    Global.MAP_UNIT_X * count,
                    Global.MAP_UNIT_Y,
                    Global.MAP_UNIT_X + Global.MAP_UNIT_X * count,
                    0,
                    null);
        }

    }

}
