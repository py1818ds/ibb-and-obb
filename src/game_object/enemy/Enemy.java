package game_object.enemy;

import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;

import java.awt.*;

public abstract class Enemy extends GameObject {

    protected GameObject clone;
    protected Image white;
    protected GravityState gravityState;


    public Enemy(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public Enemy(int x, int y, int width, int height,
                 int x2, int y2, int width2, int height2) {
        super(x, y, width, height, x2, y2, width2, height2);
    }

    public Enemy(Rect rect) {
        super(rect);
    }

    public Enemy(Rect rect, Rect rect2) {
        super(rect, rect2);
    }

    public GameObject clone() {
        return clone;
    }

    @Override
    public void paintComponent(Graphics g) {

    }
}
