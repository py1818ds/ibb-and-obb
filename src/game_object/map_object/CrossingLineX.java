package game_object.map_object;

import game_object.GameObject;
import animation.AnimationCross;

import java.awt.*;

public class CrossingLineX extends GameObject {

    // private Image img;
    private AnimationCross animationCross;

    public CrossingLineX(int x, int y, int w, int h, int x2, int y2, int w2, int h2) {
        super(x, y, w, h, x2, y2, w2, h2);
        //this.img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().canCross());
        animationCross = new AnimationCross();
    }

    @Override
    public void update() {
        animationCross.update();
    }

    @Override
    public void paintComponent(Graphics g) {
        animationCross.paint(painter().left(), painter().top(), painter().right(), painter().bottom(), g);
    }
}
