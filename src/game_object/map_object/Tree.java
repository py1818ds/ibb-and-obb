package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.Global;
import utils.Path;

import java.awt.*;

public class Tree extends GameObject {

    private final Image img;

    public Tree(int x, int y, int width, int height) {
        super(x, y, Global.UNIT_X, height, x, y, width, height);
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().tree());

    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, painter().left(), painter().top(), painter().getWidth(), painter().getHeight(), null);
    }

    @Override
    public void update() {

    }
}
