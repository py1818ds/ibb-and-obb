package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.Delay;
import utils.Global;
import utils.Path;

import java.awt.*;

public class GravityBall extends GameObject {
    private Image img;

    private final int gravity;
    private Delay delayAnima;
    private int count;
    
    public GravityBall(int x, int y, int width, int height) {
        super(x, y, width , height );
        gravity = -2;
        delayAnima = new Delay(15);
        delayAnima.loop();
        count = 0;
        img = SceneController.instance().irc().tryGetImage(new Path().img().objs().gravityBall());

    }

    public int getGravity(){
        return gravity;
    }

    @Override
    public void update() {
        if (delayAnima.count()) {
            count++;
            count %= 3;
        }
        //  System.out.println("c="+count);

    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                collider().getWidth() * count,
                0,
                collider().getWidth() + collider().getWidth() * count,
                collider().getHeight(),
                null);
    }
    
}
