package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.*;

import java.awt.*;

public class FloorNormal extends GameObject {

    private FloorDirection floorDirection;
    private final Image img;
    private int level;

    public FloorNormal(int x, int y, FloorDirection floorDirection, int level) {
        super(x, y, 32, 32, x, y, 32, 32);
        this.floorDirection = floorDirection;
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().floor());
        this.level = level;
    }

    public FloorDirection floorDirection() {
        return floorDirection;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, painter().left(), painter().top(), painter().right(), painter().bottom(),
                floorDirection.index() * (Global.MAP_UNIT_X),
                Global.MAP_UNIT_Y * level,
                floorDirection.index() * (Global.MAP_UNIT_X) + Global.MAP_UNIT_X,
                Global.MAP_UNIT_Y + Global.MAP_UNIT_Y * level,
                null);
    }
}
