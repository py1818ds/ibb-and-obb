package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.Path;

import java.awt.*;

public class JumpFloor extends GameObject {
    public enum State{
        NONE,
        AMY_JUMP,
        JIMMY_JUMP;
    }


    private Image image;
    private State state;

    public JumpFloor(int x, int y, int width, int height) {
        super(x, y, width, height+4,x, y, width, height);
        state = State.NONE;
        image = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().jumpFloor());
    }

    public void setState(State state){
        this.state = state;
    }
    public State state(){
        return state;
    }


    public boolean sthOnTop(GameObject player){
        boolean onTop = player.collider().top() < collider().top();
        return onTop;
    }

    public boolean sthOnBottom(GameObject player){
        boolean onBottom = player.collider().bottom() > collider().bottom();
        return onBottom;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(image,painter().left(), painter().top(), painter().getWidth(), painter().getHeight(),null);
    }
}