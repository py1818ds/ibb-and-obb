
package game_object.map_object;


import game_object.GameObject;
import game_object.GravityState;
import game_object.characters.Character;
import utils.Global;

import java.awt.*;

public class RebornPoint extends GameObject {


    private GravityState gravityState;
    private boolean canFalling;
    private int coordinateX;
    private int coordinateY;

    public RebornPoint(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public void rebornRecord(Character player){
        gravityState = player.gravityState();
        canFalling = player.isCanFalling();
        coordinateX = player.collider().left();
        coordinateY = player.collider().top();
        System.out.println(coordinateX);
        System.out.println(coordinateY);
    }

    public void giveBackState(Character player, Character partner){
        player.setGravityState(gravityState);
        player.setCanFalling(canFalling);
        player.translateX(coordinateX - player.collider().left() +10);
        player.translateY(coordinateY - player.collider().top() + 8);

        partner.setGravityState(gravityState);
        partner.setCanFalling(canFalling);
        partner.translateX(coordinateX - partner.collider().left()+10);
        partner.translateY(coordinateY - partner.collider().top() + 8) ;


    }
    
    public void giveBackState(Character player){
        player.setGravityState(gravityState);
        player.setCanFalling(canFalling);
        player.translateX(coordinateX - player.collider().left() +10);
        player.translateY(coordinateY - player.collider().top() + 8);

    }



    @Override
    public void paintComponent(Graphics g) {

    }

    @Override
    public void update() {

    }
    
}
