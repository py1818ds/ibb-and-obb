package game_object.map_object;

import animation.Animation_BlackHole;
import game_object.GameObject;


import java.awt.*;

public class CrossingLineY extends GameObject {
    private final Animation_BlackHole animationCross;

    public CrossingLineY(int x, int y, int w, int h, int x2, int y2, int w2, int h2) {
        super(x, y, w, h, x2, y2, w2, h2);
        animationCross = new Animation_BlackHole();
    }

    @Override
    public void update() {
        animationCross.update();
    }

    @Override
    public void paintComponent(Graphics g) {
        animationCross.paint(painter().left(), painter().top(), painter().right(), painter().bottom(), g);
    }
}
