package game_object.map_object;


public enum FloorDirection {
    UP(0),
    DOWN(1),
    LEFT(2),
    RIGHT(3),
    LEFT_UP_CONVEX(4),
    RIGHT_UP_CONVEX(5),
    LEFT_DOWN_CONVEX(6),
    RIGHT_DOWN_CONVEX(7),
    LEFT_UP_CONCAVE(8),
    RIGHT_UP_CONCAVE(9),
    LEFT_DOWN_CONCAVE(10),
    RIGHT_DOWN_CONCAVE(11);

    private final int index;

    private FloorDirection(int index) {
        this.index = index;
    }

    public int index() {
        return index;
    }
}

