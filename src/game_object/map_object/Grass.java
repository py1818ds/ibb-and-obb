package game_object.map_object;

import controllers.SceneController;
import utils.Global;
import utils.Path;

import java.awt.*;

public class Grass {

    private int x;
    private int y;
    private int width;
    private int height;
    private final Image img;

    public Grass(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().grass());

    }


    public void paint(Graphics g) {
        g.drawImage(img, x, y, width, height, null);
    }

}
