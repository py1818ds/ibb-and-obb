package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.Delay;
import utils.Path;

import java.awt.*;

public class VictoryPoint extends GameObject {

    private Image img;
    private Delay delay;
    private int count;

    public VictoryPoint(int x, int y, int width, int height) {
        super(x, y, width, height);
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().flag());


    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, painter().left(), painter().top(), painter().getWidth(), painter().getHeight(),
                null);
    }

    @Override
    public void update() {

    }
}
