package game_object.map_object;

import controllers.SceneController;
import game_object.GameObject;
import utils.Delay;
import utils.Path;

import java.awt.*;

public class TrapFloor extends GameObject {
    public enum State{
        NORMAL,
        CRASHED,
        REMOVED;
    }

    private Image image;
    private State state;
    private Delay delay;

    public TrapFloor(int x, int y, int width, int height) {
        super(x, y, width, height);
        delay = new Delay(30);
        state = State.NORMAL;
        image = SceneController.instance().irc().tryGetImage(new Path().img().objs().trapFloor());
    }

    public void changeState(){
        state = State.CRASHED;
    }

    public State state(){
        return state;
    }

    public boolean sthOnTop(GameObject player){
        boolean onTop = player.collider().top() < collider().top();
        return onTop ? true : false;
    }

    public boolean sthOnBottom(GameObject player){
        boolean onBottom = player.collider().bottom() > collider().bottom();
        return onBottom ? true : false;
    }

    public boolean sthOnRight(GameObject player){

        boolean onRight = player.collider().left() < collider().left();
        return onRight ? true : false;
    }

    public boolean sthOnLeft(GameObject player){
        boolean onLeft = player.collider().right() > collider().right();
        return onLeft ? true : false;
    }

    @Override
    public void update(){
        if(state == State.CRASHED){
            delay.play();
        }
        if(delay.count()){
            state = State.REMOVED;
        }
    }
    @Override
    public void paintComponent(Graphics g) {
        if(state != State.REMOVED){
            g.drawImage(image,painter().left(), painter().top(), painter().getWidth(), painter().getHeight(),null);
        }
    }
}