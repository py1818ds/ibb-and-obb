package game_object.characters;

import animation.AnimationRole;
import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GameObject;
import game_object.GravityState;
import game_object.Rect;
import game_object.item_object.Item;
import game_object.map_object.*;
import game_object.item_object.RopeCanThrow;
import scene.TutorialScene;
import utils.Delay;
import utils.Global;
import utils.Global.*;
import utils.Path;

import java.awt.*;
import java.util.ArrayList;

import static utils.Global.MAX_DISTANCE;
import static utils.Global.ROLE_SPEED_X;

public abstract class Character extends GameObject {
    /* 基本屬性 8 + Enum*/
    private int id;
    private int speedX; //x軸速度
    private int speedY; //y軸速度
    private final int jumpSpeed; //跳躍速度
    private final int acceleration;
    private boolean canFalling; //判斷是否會落下 可以有加速度的狀態
    private boolean canJump; //判斷是否能跳 不能二段跳
    private boolean isTouchGBall;
    private GravityState gravityState;


    /* 動畫狀態 3 + Enum */
    private final AnimationRole animationRole;//根據走路狀態抓圖
    private Direction direction; // 判斷走路+動畫方向
    private AnimationState animationState;
    private AnimationState lastAnimationState;//暫存用
    private AnimationState lastTooFar;

    public enum AnimationState {//對應的動畫
        STAND(0),
        WALK(1),
        DEAD(2),
        VICTORY(3),
        SPEED_UP(4),
        INVINCIBLE(5),
        AXE(6),
        CUT_TREE(7),
        RING_BELL(8),
        BE_RING_BELL(9),
        SPYDER_MAN_NORMAL(10),
        SPYDER_MAN_FLYING(11),
        PARTNER_TOO_LEFT(12),
        PARTNER_TOO_RIGHT(13);
        private final int animationX;

        AnimationState(int inputAnimationX) {
            this.animationX = inputAnimationX;
        }

        public int animationX() {
            return animationX;
        }
        public static AnimationState getAnimationState(int inputIndex){
            return switch (inputIndex) {
                case 0 -> STAND;
                case 1 -> WALK;
                case 2 -> DEAD;
                case 3 -> VICTORY;
                case 4 -> SPEED_UP;
                case 5 -> INVINCIBLE;
                case 6 -> AXE;
                case 7 -> CUT_TREE;
                case 8 -> RING_BELL;
                case 9 -> BE_RING_BELL;
                case 10 -> SPYDER_MAN_NORMAL;
                case 11 -> SPYDER_MAN_FLYING;
                case 12 -> PARTNER_TOO_LEFT;
                case 13 -> PARTNER_TOO_RIGHT;
                default -> WALK;
            };
        }
    }

    /* 能力狀態 2 + Enum */
    private AbilityState abilityState;
    private GameObject abilityItem;


    public enum AbilityState {//具有的能力
        NORMAL,
        INVINCIBLE,
        AXE,
        SPEED_UP,
        RING_BELL,
        SPYDER_MAN;
    }

    /* 穿越狀態 3 + Enum */
    private boolean isTouchCrossingLine; //是否碰到穿越線X
    private boolean isCrossingSuccess; //是否成功穿越
    private CrossingState crossingFrom; //從哪個方向來

    public enum CrossingState {
        CROSSING_FROM_UP,
        CROSSING_FROM_DOWN,
        CROSSING_FROM_LEFT,
        CROSSING_FROM_RIGHT,
        None;
    }

    /* 存各種碰撞物 */
    //3
    private CrossingLineX X;
    private CrossingLineY Y;
    private GravityBall gravityBall;

    /* 道具碰撞需要的屬性 */
    //4
    private Delay delayToNormalAbility;
    private Delay delayFinishAbility;
    private final Delay delayRopeCoolDown;
    private Delay delayRope;
    private boolean canRope;
    private int point;

    /* Partner */
    private Character partner;

    //
    private String name;

    // 建構子
    public Character(int x, int y, AnimationRole.AnimationArea inputArea) {
        super(x, y, 42, 48, x, y, 48, 48);
        //基本屬性
        speedX = 0;
        speedY = 0;//往下速度2
        jumpSpeed = 15; //起跳速度固定(由gravityState決定跳的方向)
        acceleration = 1;//起始加速度
        canFalling = true;
        isTouchGBall = false;

        //重力場
        gravityState = GravityState.NORMAL_GRAVITY;

        //動畫
        direction = Direction.RIGHT;//起始面向右
        animationState = AnimationState.STAND; //設定基本動畫狀態
        animationRole = new AnimationRole(inputArea); //處理動畫用類別

        //能力
        abilityState = AbilityState.NORMAL; //無能力

        //穿越
        crossingFrom = CrossingState.None;
        isTouchCrossingLine = false;
        isCrossingSuccess = false;

        //待分類
        delayRopeCoolDown = new Delay(180);
        canRope = true;
        point = 0;
    }

    // 道具相關 //add
//    public boolean hasEatenFruit(){
//        return hasEatenFruit;
//    }

    public int point() {
        return point;
    }
      /* 連線用 */
      public void setID(int id) {
        this.id = id;
    }
    public int id(){
        return id;
    }


    public void resetPoint() {
        this.point = 0;
    }

    public void setPoint(int point) {
        this.point += point;
    }

    public void setPosition(int x, int y) {
        collider().setPosition(x, y);
        painter().setPosition(x, y);
    }


    //下一幀替身碰撞
    public boolean isNextCollision(GameObject obj) {
        Rect inputObj = obj.collider();
        if (collider().right() + speedX < inputObj.left()) {
            return false;
        }
        if (collider().left() + speedX > inputObj.right()) {
            return false;
        }
        if (collider().top() + speedY > inputObj.bottom()) {
            return false;
        }
        if (collider().bottom() + speedY < inputObj.top()) {
            return false;
        }
        return true;
    }


    /* 移動相關 */
    public void move() {

        if (animationState == AnimationState.DEAD) {
            return;
        }
        //移動x方向 鍵盤不操作就不動
        if ((direction == Direction.RIGHT && collider().right() < Global.MAP1_X) || (direction == Direction.LEFT && collider().left() > 0)) {
            translateX(speedX);
        }
        if (canFalling) {
            if (gravityState == GravityState.NORMAL_GRAVITY) {
                speedY = Math.min(speedY + acceleration, 32); //設上限避免穿牆
                // speedY += acceleration;
            } else {
                speedY = Math.max(speedY - acceleration, -32);
                // speedY -= acceleration;
            }
        }
        //反方向碰到重力球會越抖越大
        if (isTouchGBall && gravityState == GravityState.NORMAL_GRAVITY) {
            speedY += gravityBall.getGravity();
        } else if (isTouchGBall && gravityState == GravityState.ANTI_GRAVITY) {
            speedY -= gravityBall.getGravity();
        }
        translateY(speedY);
    } //用拿到的x速度y速度移動

    public void jump() {
        if (canJump) {
            AudioResourceController.getInstance().shot(new Path().sounds().jump());
            //根據重力場決定跳的方向
            if (gravityState == GravityState.NORMAL_GRAVITY) {
                speedY = -jumpSpeed;
            } else {
                speedY = jumpSpeed;
            }
            canJump = false;
            canFalling = true;
        }
    } //操作者執行jump


    //執行不同案件時觸發
    public void pressLeft() {
        direction = Direction.LEFT;
        if (animationState == AnimationState.STAND) {
            animationState = AnimationState.WALK;
        }
        if (animationState == AnimationState.SPYDER_MAN_FLYING || animationState == AnimationState.PARTNER_TOO_RIGHT) {
            speedX = 0;
        } else if (animationState == AnimationState.SPEED_UP) {
            speedX = -(int) (1.5d * ROLE_SPEED_X);
        } else {
            speedX = -ROLE_SPEED_X;
        }
    }

    public void pressRight() {
        direction = Direction.RIGHT;
        if (animationState == AnimationState.STAND) {
            animationState = AnimationState.WALK;
        }
        if (animationState == AnimationState.SPYDER_MAN_FLYING || animationState == AnimationState.PARTNER_TOO_LEFT) {
            speedX = 0;
        } else if (animationState == AnimationState.SPEED_UP) {
            speedX = (int) (1.5d * ROLE_SPEED_X);
        } else {
            speedX = ROLE_SPEED_X;
        }
    }

    public void pressUp() {
        jump();
    }

    public void releaseKey() {
        if (animationState == AnimationState.WALK) {
            animationState = AnimationState.STAND;
        }
        if (partner!=null && partner.animationState == AnimationState.BE_RING_BELL) {
            partner.giveBackAnimationState();
        }

        if (abilityItem != null && abilityItem.getClass() == RopeCanThrow.class) {
            RopeCanThrow tmp = (RopeCanThrow) abilityItem;
            if (tmp.hookState() != RopeCanThrow.HookState.FLYING) {
                speedX = 0;
                canFalling = true;
            }
        } else {
            speedX = 0;
        }
    }

    public void useAbility() {
        //能力順序可能需要調整
        switch (abilityState) {
            case AXE -> {
                AudioResourceController.getInstance().play(new Path().sounds().useAxe());
                if (animationState != AnimationState.CUT_TREE) {
                    delayFinishAbility = new Delay(30);
                    delayFinishAbility.play();
                    changeAndSaveAnimationState(AnimationState.CUT_TREE);
                }

            }
            case RING_BELL -> {
                if(animationState != AnimationState.PARTNER_TOO_LEFT && animationState != AnimationState.PARTNER_TOO_RIGHT){
                    AudioResourceController.getInstance().play(new Path().sounds().bell());
                    partner.changeAndSaveAnimationState(AnimationState.BE_RING_BELL);
                    if (collider().centerX() > partner.collider().centerX()) {
                        partner.translateX(5);
                    } else if (collider().centerX() < partner.collider().centerX()) {
                        partner.translateX(-5);
                    }
                }
            }
            case SPYDER_MAN -> {
//                AnimationState.SPY
                if (abilityItem == null && canRope) {
                    abilityItem = new RopeCanThrow(collider().left(), collider().top(), direction, gravityState);
                    canRope = false;//技能CD boolean
                    delayRopeCoolDown.play(); //冷卻倒數
                    delayRope = new Delay(240);
                    delayRope.play(); //太久沒勾到要讓它消失
                } else if (abilityItem != null && abilityItem.getClass() == RopeCanThrow.class) {
                    RopeCanThrow tmp = (RopeCanThrow) abilityItem;
                    if (tmp.hookState() == RopeCanThrow.HookState.GOTCHA) {
                        tmp.setHookState(RopeCanThrow.HookState.FLYING);
                        canFalling = false;
                    }
                }
            }
        }
    }

//    private void checkFalling(){
//        if(objectForCheckFalling.size()==0){
//            return;
//        }
//
//        for (int i=0; i<objectForCheckFalling.size(); i++){
//            if(isCollision(objectForCheckFalling.get(0))){
//                objectForCheckFalling.remove(i--);
//            }
//        }
//
//        for (GameObject gameObject : objectForCheckFalling) {
//            if (isCollision(gameObject)) {
//                return;
//            }
//        }
//        canFalling = true;
//    }

//    public void checkFalling(GameObject inputObject){
//        objectForCheckFalling.add(inputObject);
//    }

    private void cancelRope() {
        if (abilityItem != null && abilityItem.getClass() == RopeCanThrow.class) {
            RopeCanThrow tmp = (RopeCanThrow) abilityItem;
            if (tmp.hookState() == RopeCanThrow.HookState.FLYING) {
                abilityItem = null;
            }
        }
    }


    /* 鏡像畫圖ok */
    @Override
    public void paintComponent(Graphics g) {
        if (gravityState == GravityState.NORMAL_GRAVITY && direction == Direction.RIGHT) {
            animationRole.paint(animationState,
                    painter().left(), painter().top(),
                    painter().right(), painter().bottom(), g);
        }
        if (gravityState == GravityState.ANTI_GRAVITY && direction == Direction.RIGHT) {
            animationRole.paint(animationState,
                    painter().left(), painter().bottom(),
                    painter().right(), painter().top(), g);
        }
        if (gravityState == GravityState.NORMAL_GRAVITY && direction == Direction.LEFT) {
            animationRole.paint(animationState,
                    painter().right(), painter().top(),
                    painter().left(), painter().bottom(), g);
        }
        if (gravityState == GravityState.ANTI_GRAVITY && direction == Direction.LEFT) {
            animationRole.paint(animationState,
                    painter().right(), painter().bottom(),
                    painter().left(), painter().top(), g);
        }

        Image holdingItem = SceneController.instance().irc().tryGetImage(new Path().img().objs().bell());
        if (abilityState == AbilityState.SPYDER_MAN) {
        }

//        if (gravityState == GravityState.ANTI_GRAVITY && direction == Direction.LEFT) {
//            g.drawImage(holdingItem,
//                    painter().right(), painter().bottom(),
//                    painter().left(), painter().top(), null);
//        }


        //畫出持有道具

        if (abilityItem != null && abilityItem.getClass() == RopeCanThrow.class) {
            RopeCanThrow tmp = (RopeCanThrow) abilityItem;
            tmp.paint(g);
            if (tmp.hookState() == RopeCanThrow.HookState.FLYING) {
                Graphics2D g2d = (Graphics2D) g;
                g2d.setColor(Color.WHITE);
                g2d.setStroke(new BasicStroke(2));
                g2d.drawLine(painter().left(), painter().top(), tmp.painter().left(), tmp.painter().top());
                g.setColor(Color.BLACK);
            }
        }

    }

    /* 更新角色 */
    @Override
    public void update() {
        animationRole.update();//動畫抖動用

        if (isCrossingSuccess) {
            setCrossingSuccess(false);
            setCrossingFrom(CrossingState.None);
        }

        /* 超出地圖從上面 或下面 掉下來 */
        if (this.collider().top() + this.collider().getHeight() / 2 < 0) {
            translateY(Global.MAP2_Y);
            changeGravityState(); //預設會換重力場
        }
        if (this.collider().top() + this.collider().getHeight() / 2 > 1920) {
            translateY(-Global.MAP2_Y);
            changeGravityState();//預設會換重力場
        }


        //變正常的delay不為null就開始倒數
        if (delayToNormalAbility != null && delayToNormalAbility.count()) {
            animationState = AnimationState.STAND;
            delayToNormalAbility = null;
        }

        if (delayFinishAbility != null && delayFinishAbility.count()) {
            giveBackAnimationState();
            delayFinishAbility = null;
        }

        //CD時間
        if (delayRopeCoolDown.count()) {
            delayRopeCoolDown.stop();
            canRope = true;
        }

        //繩子移動
        if (abilityItem != null) {
            abilityItem.update();
            if (abilityItem.getClass() == RopeCanThrow.class) {
                RopeCanThrow tmp = (RopeCanThrow) abilityItem;
                if (tmp.hookState() == RopeCanThrow.HookState.FLYING) {
                    translate(tmp.flyX(), tmp.flyY());
                    if (delayRope.count()) {
                        abilityItem = null;
                        delayRope.stop();
                    }
                }
            }
        }

        move();
    }


    /**
     * 碰撞用 function
     */

    /* 碰撞一般地板ok */
    public void hitFloors(ArrayList<FloorNormal> floors) {
        for (FloorNormal floor : floors) {
            //完全沒碰到就設定為掉落狀態
            if (isCollision(floor)) {
                Rect actorCollider = collider();
                Rect floorCollider = floor.collider();

                //額外功能 撞到左右下牆會取消繩子

                switch (floor.floorDirection()) {
                    case UP -> {
                        //速度0 + 向上推
                        setSpeedY(0);
                        translateY(-(actorCollider.bottom() - floorCollider.top()));
                        //上重力場
                        if (gravityState() == GravityState.NORMAL_GRAVITY) {
                            setCanJump(true);
                            setCanFalling(false);
                        } else {
                            cancelRope();
                        }
                    }
                    case DOWN -> {//向下推
                        // System.out.println("=============下");
                        setSpeedY(0);
                        translateY(floorCollider.bottom() - actorCollider.top());
                        if (gravityState() == GravityState.ANTI_GRAVITY) {
                            setCanJump(true);
                            setCanFalling(false);
                        } else {
                            cancelRope();
                        }
                    }
                    case LEFT -> {//向左推
                        // System.out.println("=============左");
                        translateX(-(actorCollider.right() - floorCollider.left()));
                        cancelRope();
                    }
                    case RIGHT -> {//向右推
                        // System.out.println("=============右");
                        translateX(floorCollider.right() - actorCollider.left());
                        cancelRope();
                    }
                    case LEFT_UP_CONVEX -> {//判斷左上
                        //System.out.println("=============左 上 凸");
                        // 先判斷要往左還是上推
                        if (actorCollider.bottom() - speedY() <= floorCollider.top()) {
                            translateY(-(actorCollider.bottom() - floorCollider.top()));
                            setSpeedY(0);
                            if (gravityState() == GravityState.NORMAL_GRAVITY) {
                                setCanJump(true);
                                setCanFalling(false);
                                if (actorCollider.right() == floorCollider.left()) {
                                    translateX(-1);
                                }
                            } else {
                                cancelRope();
                            }

                        } else {
                            //      System.out.println("向左噴");
                            translateX(-(actorCollider.right() - floorCollider.left()) - speedX);
                            cancelRope();
                        }

                    }
                    case RIGHT_UP_CONVEX -> {//判斷右上
                        //    System.out.println("=============右 上 凸");
                        if (actorCollider.bottom() - speedY <= floorCollider.top()) {
                            translateY(-(actorCollider.bottom() - floorCollider.top()));
                            setSpeedY(0);
                            if (gravityState == GravityState.NORMAL_GRAVITY) {
                                setCanJump(true);
                                setCanFalling(false);
                                if (actorCollider.left() == floorCollider.right()) {
                                    translateX(1);
                                }
                            } else {
                                cancelRope();
                            }
                        } else {
                            translateX(floorCollider.right() - actorCollider.left());
                            cancelRope();
                        }

                    }
                    case LEFT_DOWN_CONVEX -> {//判斷左下
                        //    System.out.println("=============左 下 凸");
                        if (actorCollider.top() - speedY() >= floorCollider.bottom()) {
                            translateY(floorCollider.bottom() - actorCollider.top());
                            setSpeedY(0);
                            if (gravityState() == GravityState.ANTI_GRAVITY) {
                                setCanJump(true);
                                setCanFalling(false);
                                if (actorCollider.right() == floorCollider.left()) {
                                    translateX(-1);
                                }
                            } else {
                                cancelRope();
                            }
                        } else {
                            translateX(-(actorCollider.right() - floorCollider.left()));
                            cancelRope();
                        }

                    }
                    case RIGHT_DOWN_CONVEX -> {//判斷右下
                        if (actorCollider.top() - speedY() >= floorCollider.bottom()) {
                            translateY(floorCollider.bottom() - actorCollider.top());
                            setSpeedY(0);
                            if (gravityState() == GravityState.ANTI_GRAVITY) {
                                setCanJump(true);
                                setCanFalling(false);
                                if (actorCollider.left() == floorCollider.right()) {
                                    translateX(1);
                                }
                            } else {
                                cancelRope();
                            }
                        } else {
                            translateX(floorCollider.right() - actorCollider.left());
                            cancelRope();
                        }
                    }
                    case LEFT_UP_CONCAVE, RIGHT_UP_CONCAVE -> {
                        //速度0 + 向上推
                        setSpeedY(0);
                        translateY(-(actorCollider.bottom() - floorCollider.top()));
                        //上重力場
                        if (gravityState() == GravityState.NORMAL_GRAVITY) {
                            setCanJump(true);
                            setCanFalling(false);
                        } else {
                            System.out.println("下重力場的左上凹、右上凹地板不用做事");
                        }
                        cancelRope();
                    }
                    case LEFT_DOWN_CONCAVE, RIGHT_DOWN_CONCAVE -> {
                        setSpeedY(0);
                        translateY(floorCollider.bottom() - actorCollider.top());
                        if (gravityState() == GravityState.ANTI_GRAVITY) {
                            setCanJump(true);
                            setCanFalling(false);
                        } else {
                            System.out.println("上重力場的左下凹、右下凹地板不做事");
                        }
                        cancelRope();
                    }
                }//switch

//                objectForCheckFalling.add(floor);

            }//if 這一塊floor isNextCollision 就執行switch

            //之後改成所有碰撞在判斷是否能falling
            if (floor == floors.get(floors.size() - 1)) {
                setCanFalling(true);
            }
        }//for floor
    }

    /* 碰撞樹 */
    public void hitTrees(ArrayList<Tree> trees) {
        for (int i = 0; i < trees.size(); i++) {
            //完全沒碰到就設定為掉落狀態
            Tree tree = trees.get(i);
            if (isCollision(tree)) {
                Rect actorCollider = collider();
                Rect treeCollider = tree.collider();
                if (animationState == AnimationState.CUT_TREE) {
                    trees.remove(i);
                    break;
                }
                if (actorCollider.right() - speedX <= treeCollider.left()) {
                    translateX(-(actorCollider.right() - treeCollider.left()));
                } else if (actorCollider.left() - speedX >= treeCollider.right()) {
                    translateX(treeCollider.right() - actorCollider.left());
                }
            }//if 這一塊floor isNextCollision
        }//for floor
    }

    /* 碰撞穿越線ok */
    public void hitCrossingFloors(ArrayList<CrossingLineX> crossingX, ArrayList<CrossingLineY> crossingY) {

        for (CrossingLineX tmpCrossingX : crossingX) {
            if (X != null) {
                break;
            }//break表示存在穿越線X
            if (this.isCollision(tmpCrossingX) && !this.isCrossingSuccess()) {
                X = tmpCrossingX;
                AudioResourceController.getInstance().play(new Path().sounds().gravityBallSound());
                //原本用位置來判斷方向
                //this.collider().top() < crossingLineX.collider().top()
                if (this.speedY() > 0) {
                    //往下吸
                    this.setCrossingFrom(Character.CrossingState.CROSSING_FROM_UP);
                }
                //原本用位置來判斷
                //this.collider().bottom() > crossingLineX.collider().bottom()
                else if (this.speedY() < 0) {
                    // this.translateY(-50);
                    this.setCrossingFrom(Character.CrossingState.CROSSING_FROM_DOWN);
                }else if(collider().top()<X.collider().bottom() && collider().bottom()>X.collider().bottom()){
                    crossingFrom = CrossingState.CROSSING_FROM_DOWN;
                }else if (collider().bottom()>X.collider().top() && collider().top()<X.collider().top()){
                    crossingFrom = CrossingState.CROSSING_FROM_UP;
                }
                this.setTouchCrossingLine(true);
                break;
            }

        }


        /* Y軸穿越線碰撞 存取Y軸穿越線 */
        for (CrossingLineY tmpCrossingY : crossingY) {
            if (Y != null) {
                break;
            }//break表示存在穿越線Y
            if (this.isCollision(tmpCrossingY)) {
                Y = tmpCrossingY;
                AudioResourceController.getInstance().play(new Path().sounds().gravityBallSound());
                //原本用位置判斷
                //this.collider().right() > crossingLineY.collider().right()
                //用當前速度判斷來的方向
                if (this.collider().left() + this.collider().getWidth() / 2 > Y.collider().left() + Y.collider().getWidth() / 2) {
                    this.setCrossingFrom(Character.CrossingState.CROSSING_FROM_RIGHT);
                    this.translateX(-Global.JIMMY_X-48);
                }
                //原本用位置判斷
                //this.collider().left() < crossingLineY.collider().left()
                //現在用速度
                else if (this.collider().left() + this.collider().getWidth() / 2 < Y.collider().left() + Y.collider().getWidth() / 2) {
                    this.setCrossingFrom(Character.CrossingState.CROSSING_FROM_LEFT);
                    this.translateX(Global.JIMMY_X+48);
                }
                this.setTouchCrossingLine(true);
                break;
            }
            tmpCrossingY.update();
        }

        /* 判斷穿越 */

        if (this.isTouchCrossingLine()) {
            if (X != null) { //判斷X軸穿越線
                //角色是否從上穿越至下
                boolean characterCrossingFromUp = this.isTouchCrossingLine()
                        && this.collider().top() + this.collider().getHeight() / 2 > X.collider().bottom() - X.collider().getHeight() / 2
                        && this.crossingFrom() == Character.CrossingState.CROSSING_FROM_UP;
                //角色是否從下穿越至上
                boolean characterCrossingFromDown = this.isTouchCrossingLine()
                        && this.collider().top() + this.collider().getHeight() / 2 < X.collider().top() + X.collider().getHeight() / 2
                        && this.crossingFrom() == Character.CrossingState.CROSSING_FROM_DOWN;

                if (characterCrossingFromUp || characterCrossingFromDown) { //穿越成功
                    this.setCrossingSuccess(true);
                    this.setCrossingFrom(Character.CrossingState.None);
                    this.changeGravityState();
                }
                if (this.collider().bottom() < X.collider().top() || this.collider().top() > X.collider().bottom()) {
                    X = null;
                    this.setCrossingSuccess(false);
                    this.setTouchCrossingLine(false);
                    this.setCrossingFrom(Character.CrossingState.None);
                }

            } else if (Y != null) { // 判斷Y軸穿越線
                //角色是否從左穿越至右
                boolean characterCrossingFromLeft = this.collider().left() >= Y.collider().right()
                        && this.crossingFrom() == Character.CrossingState.CROSSING_FROM_LEFT;
                //角色是否從右穿越至左
                boolean characterCrossingFromRight = this.collider().right() <= Y.collider().left()
                        && this.crossingFrom() == Character.CrossingState.CROSSING_FROM_RIGHT;

                //有五個東西要改
                if (characterCrossingFromLeft || characterCrossingFromRight) { //穿越成功
                    this.setCrossingSuccess(true);
                    this.setCrossingFrom(Character.CrossingState.None);
                    this.changeGravityState();

                }
                if (Y != null && !this.isCollision(Y)) {
                    Y = null;
                    this.setTouchCrossingLine(false);
                    this.setCrossingFrom(Character.CrossingState.None);
                }

            } else {
                System.out.println("有BUG");
            }
        }
    }

    /* 碰撞重力球ok */
    public void hitGravityBalls(ArrayList<GravityBall> gravityBalls) {
        for (GravityBall gBall : gravityBalls) {
            if (gravityBall != null) {
                break;
            }
            if (this.isCollision(gBall)) {
                AudioResourceController.getInstance().play(new Path().sounds().gravityBallSound());
                gravityBall = gBall;
                isTouchGBall = true;
            }
        }
        if (gravityBall != null && !this.isCollision(gravityBall)) {
            isTouchGBall = false;
            gravityBall = null;
        }
    }

    /* 碰撞道具 */
    public void hitItem(ArrayList<Item> items) {
        Item tmpItem;
        for (int i = 0; i < items.size(); i++) {
            tmpItem = items.get(i);
            if (!isCollision(tmpItem)) {
                continue;
            }
            //碰到item
            switch (tmpItem.itemType()) {
                case AXE -> {
                    //獲得斧頭能力
                    AudioResourceController.getInstance().play(new Path().sounds().equip());
                    animationState = AnimationState.AXE;
                    abilityState = AbilityState.AXE;
                }
                case BELL_MAGNET -> {
                    //獲得搖鈴能力
                    AudioResourceController.getInstance().play(new Path().sounds().bell());
                    animationState = AnimationState.RING_BELL;
                    abilityState = AbilityState.RING_BELL;
                }
                case COIN -> {
                    point += 10;
                }
                case FRUIT -> {
                    //獲得加速能力
                    AudioResourceController.getInstance().play(new Path().sounds().speedUP());
                    animationState = AnimationState.SPEED_UP;
                    abilityState = AbilityState.SPEED_UP;
                    delayToNormalAbility = new Delay(300);
                    delayToNormalAbility.play();
                }
                case ROPE -> {
                    //獲得蜘蛛人能力(完成80%)
                    animationState = AnimationState.SPYDER_MAN_NORMAL;
                    abilityState = AbilityState.SPYDER_MAN;
                }
                case STAR -> {
                    //獲得無敵能力
                    AudioResourceController.getInstance().play(new Path().sounds().powerUp());
                    animationState = AnimationState.INVINCIBLE;
                    abilityState = AbilityState.INVINCIBLE;
                    delayToNormalAbility = new Delay(600);
                    delayToNormalAbility.play();//限時能力
                }
            }
            tmpItem.shot();
            items.remove(i);
            break;
        }
    }

    /* 碰撞敵人ok */


    /**
     * 所有Getter Setter
     */

    /* 移動改變方向 Getter Setter*/
    //2
    public abstract void changeDirAndSelectAnimation(int code);

    /* 動畫狀態 Getter Setter*/
    //2
    public AnimationState animationState() {
        return animationState;
    }
    public void setAnimationState(AnimationState inputAnimationState) {
        this.animationState = inputAnimationState;
    }
    public void changeAndSaveAnimationState(AnimationState inputAnimationState) {
        if(animationState == inputAnimationState ){
            return;
        }
        lastAnimationState = animationState;
        animationState = inputAnimationState;
    }
    public void giveBackAnimationState() {

        animationState = lastAnimationState;
    }

    /* 能力狀態 Getter Setter*/
    //2
    public AbilityState abilityState() {
        return abilityState;
    }
    public GameObject abilityItem() {
        return abilityItem;
    }
    public void setAbilityState(AbilityState abilityState) {
        this.abilityState = abilityState;
    }
    public void setAbilityItem(GameObject abilityItem) {
        this.abilityItem = abilityItem;
    }


    /* 重力場 Getter Setter*/
    //3
    public GravityState gravityState() {
        return gravityState;
    }
    public void setGravityState(GravityState gravityState) {
        this.gravityState = gravityState;
    }
    public void changeGravityState() {
        if (gravityState == GravityState.NORMAL_GRAVITY) {
            gravityState = GravityState.ANTI_GRAVITY;
        } else {
            gravityState = GravityState.NORMAL_GRAVITY;
        }
    }

    /*基本速度 Getter Setter*/
    //8
    public int speedX() {
        return speedX;
    }
    public int speedY() {
        return speedY;
    }
    public boolean isCanFalling() {
        return canFalling;
    }
    public boolean canJump() {
        return canJump;
    }
    public void setSpeedX(int inputVX) {
        speedX = inputVX;
    }
    public void setSpeedY(int inputVY) {
        speedY = inputVY;
    }
    public void setCanFalling(boolean canFalling) {
        this.canFalling = canFalling;
    }
    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }


    /*穿越用 Getter Setter */
    //6
    public boolean isTouchCrossingLine() {
        return isTouchCrossingLine;
    }
    public CrossingState crossingFrom() {
        return crossingFrom;
    }
    public boolean isCrossingSuccess() {
        return isCrossingSuccess;
    }
    public void setTouchCrossingLine(boolean isCross) {
        this.isTouchCrossingLine = isCross;
    }
    public void setCrossingSuccess(boolean crossingSuccess) {
        isCrossingSuccess = crossingSuccess;
    }
    public void setCrossingFrom(CrossingState crossingFrom) {
        this.crossingFrom = crossingFrom;
    }

    /* 設定同伴 */
    public void setPartner(Character inputPartner) {
        partner = inputPartner;
    }
    public boolean checkPosition(){

        if(animationState!=AnimationState.PARTNER_TOO_LEFT && animationState!=AnimationState.PARTNER_TOO_RIGHT){
            if (partner.collider().centerX() - collider().centerX() > MAX_DISTANCE) {//p2在右邊
                if(animationState == AnimationState.BE_RING_BELL){
                    giveBackAnimationState();
                }
                changeAndSaveAnimationState(AnimationState.PARTNER_TOO_RIGHT);
                return true;
            }
            if (partner.collider().centerX() - collider().centerX() <= -MAX_DISTANCE) {//p1在右邊
                if(animationState == AnimationState.BE_RING_BELL){
                    giveBackAnimationState();
                }
                changeAndSaveAnimationState(AnimationState.PARTNER_TOO_LEFT);
                return true;
            }
        }else if(Math.abs(partner.collider().centerX() - collider().centerX()) <= MAX_DISTANCE){
            //如果暫存有東西，且距離正常時，就把狀態還回去
            giveBackAnimationState();
            return true;
        }
        return false;
    }

    /* 連線用 */
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

}
