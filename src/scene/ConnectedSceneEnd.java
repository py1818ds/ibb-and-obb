package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import internet.Client.ClientClass;
import internet.Server.Server;
import menu.BackgroundType.BackgroundImage;
import menu.Label.ClickedAction;
import menu.CustomFont;
import menu.Style;
import menu.Button;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;
import static utils.Global.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.util.ArrayList;

public class ConnectedSceneEnd extends Scene {

    private Image img;
    private CustomFont cf;
    private BufferedReader br;
    private ArrayList<Button> buttons;
    private String winner;

    public ConnectedSceneEnd(String name){
        winner = name;
    }

    @Override
    public void sceneBegin() {
        cf = new CustomFont();
        buttons = new ArrayList<>();

        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground());

        buttons.add(new Button(SCREEN_X / 2 - 100 ,SCREEN_Y / 2 - 90, new Style.StyleRect(50, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().crown())))));
        buttons.add(new Button(50 , 50, new Style.StyleRect(30, 30, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
        .setText("X")
        .setTextFont(new Font("Zpix", Font.BOLD, 40))
        .setTextColor(Color.white)));

        buttons.get(1).setStyleHover(new Style.StyleRect(30, 30, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
        .setText("X")
        .setTextFont(new Font("Zpix", Font.PLAIN, 40))
        .setTextColor(Color.pink));

        buttons.get(1).setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicTwo());
                ClientClass.getInstance().disConnect();
                if(SceneTool.instance().getSelf().id() == 100){
                    Server.instance().close();
                }
                SceneController.instance().changeScene(new MenuScene());
            }
        });     
    }

    @Override
    public void sceneEnd() {
        img = null;
        buttons = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,null);
        g.setColor(Color.white);
        g.setFont(new Font("Zpix", Font.PLAIN, 80));
        g.drawString("WINNER", SCREEN_X / 2 - 120, 100);
        g.setFont(new Font("Zpix", Font.PLAIN, 50));
        g.drawString(winner, SCREEN_X / 2 - 40, SCREEN_Y / 2 - 50);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
    }
  
    @Override
    public void update() {
       
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                for(int i = 0; i < buttons.size(); i++){
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

