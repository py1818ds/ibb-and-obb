package scene;

import controllers.AudioResourceController;
import utils.GameKernel;
import game_object.characters.Character;
import game_object.characters.Character.AnimationState;
import game_object.map_object.CrossingLineX;
import game_object.map_object.CrossingLineY;
import game_object.map_object.FloorNormal;

import animation.AnimationRole;

import controllers.SceneController;
import game_object.*;

import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;

import internet.Client.ClientClass;
import internet.Client.CommandReceiver;
import internet.Server.Server;
import utils.Path;

import static utils.Global.*;


public class SceneTool implements GameKernel.GameInterface {

    private boolean isConnect;

    private Character self;
    private ArrayList<Character> players;
    private ArrayList<FloorNormal> floors;
    private ArrayList<CrossingLineX> crossingXLines;
    private ArrayList<CrossingLineY> crossingYLines;

    private static SceneTool sceneTool;


    private SceneTool() {
        players = new ArrayList<>();
        floors = new ArrayList<>();
        crossingXLines = new ArrayList<>();
        crossingYLines = new ArrayList<>();
    }

    public static SceneTool instance() {
        if (sceneTool == null) {
            sceneTool = new SceneTool();
        }
        return sceneTool;
    }

    public void setPlayers(ArrayList<Character> players) {
        this.players = players;
    }

    public void setFloors(ArrayList<FloorNormal> floors) {
        this.floors = floors;
    }

    public void setCrossingXLines(ArrayList<CrossingLineX> crossingXLines) {
        this.crossingXLines = crossingXLines;
    }

    public void setCrossingYLines(ArrayList<CrossingLineY> crossingYLines) {
        this.crossingYLines = crossingYLines;
    }


    public void setSelf(Character self) {
        this.self = self;
    }

    public void setIsConnect(boolean isConnect) {
        this.isConnect = isConnect;
    }

    public Character getSelf() {
        return self;
    }

    public ArrayList<Character> getPlayers() {
        return players;
    }


    public void createRoom(int port) {
        Server.instance().create(port);
        Server.instance().start();
    }

    public void connect(String host, int port) throws IOException {
        ClientClass.getInstance().connect(host, port);
        if (self != null) {
            self.setID(ClientClass.getInstance().getID());
        }
        isConnect = true;
    }

    @Override
    public void update() {

    }

    public void consume() {
        if (isConnect) {
            ClientClass.getInstance().consume(new CommandReceiver() {
                @Override
                public void receive(int serialNum, int commandCode, ArrayList<String> strs) {
                    switch (commandCode) {
                        case CONNECT:
                            boolean isburn = false;
                            //連線時，如果本地端的players沒有加過就創建一隻
                            for (int i = 0; i < players.size(); i++) {
                                if (players.get(i).id() == serialNum) {
                                    isburn = true;
                                    break;
                                }
                            }
                            if (!isburn) {
                                if (serialNum != 100) {
                                    Character newPlayer = new Character(125, 600, AnimationRole.AnimationArea.JIMMY) {
                                        @Override
                                        public void changeDirAndSelectAnimation(int code) {
                                            if (code == LEFT) {
                                                this.pressLeft();
                                            } else if (code == RIGHT) {
                                                this.pressRight();
                                            } else if (code == UP) {
                                                this.pressUp();
                                            } else if (code == NONE) {
                                                this.releaseKey();
                                            } else if (code == SPACE) {
                                                this.useAbility();
                                            }
                                        }
                                    };
                                    newPlayer.setID(serialNum);
                                    newPlayer.setName(strs.get(0));
                                    players.add(newPlayer);
                                    ClientClass.getInstance().sent(CONNECT, bale(self.getName()));
                                } else {
                                    Character newPlayer = new Character(200, 200, AnimationRole.AnimationArea.AMY) {
                                        @Override
                                        public void changeDirAndSelectAnimation(int code) {
                                            if (code == LEFT) {
                                                this.pressLeft();
                                            } else if (code == RIGHT) {
                                                this.pressRight();
                                            } else if (code == UP) {
                                                this.pressUp();
                                            } else if (code == NONE) {
                                                this.releaseKey();
                                            } else if (code == SPACE) {
                                                this.useAbility();
                                            }
                                        }
                                    };
                                    newPlayer.setID(serialNum);
                                    newPlayer.setName(strs.get(0));
                                    players.add(newPlayer);
                                    ClientClass.getInstance().sent(CONNECT, bale(self.getName()));
                                }
                            }
                            break;
                        case MOVE:
                            for (int i = 0; i < players.size(); i++) {
                                if (players.get(i).id() == serialNum) {
                                    players.get(i).changeDirAndSelectAnimation(Integer.parseInt(strs.get(0)));
                                }
                            }
                            break;

                        case CHECK_POSITION:
                            Character player = getPlayers().get(1);
                            player.setAnimationState(AnimationState.getAnimationState(Integer.parseInt(strs.get(0))));
                            // checkPosition(self,player);
                            break;

                        case HIT_FLOORS:
                            System.out.println("撞地板");
                            for (int i = 0; i < players.size(); i++) {
                                if (players.get(i).id() == serialNum) {
                                    int x = Integer.parseInt(strs.get(0));
                                    int y = Integer.parseInt(strs.get(1));
                                    players.get(i).setPosition(x, y);
                                    // players.get(i).setGravityState(GravityState.getGravityState(Integer.parseInt(strs.get(2))));
                                }
                            }
                            break;

                        case HIT_CROSSING_FLOORS:
                            for (Character character : players) {
                                if (character.id() == serialNum) {
                                    int x = Integer.parseInt(strs.get(0));
                                    int y = Integer.parseInt(strs.get(1));
                                    character.setPosition(x, y);
                                    character.setAnimationState(Character.AnimationState.getAnimationState(Integer.parseInt(strs.get(2))));
                                }
                            }
                            break;

                        case STAND_ON_EACH_OTHER:
                            System.out.println("stand on each other");
                            for (Character character : players) {
                                if (character.id() == serialNum) {
                                    int x = Integer.parseInt(strs.get(0));
                                    int y = Integer.parseInt(strs.get(1));
                                    character.setPosition(x, y);
                                    character.setAnimationState(Character.AnimationState.getAnimationState(Integer.parseInt(strs.get(2))));
                                    // character.setGravityState(GravityState.getGravityState(Integer.parseInt(strs.get(3))));
                                }
                            }
                            break;

                        case PUSH_EACH_OTHER:
                            System.out.println("push each other");
                            for (Character character : players) {
                                if (character.id() == serialNum) {
                                    int x = Integer.parseInt(strs.get(0));
                                    int y = Integer.parseInt(strs.get(1));
                                    character.setPosition(x, y);
                                    character.setAnimationState(Character.AnimationState.getAnimationState(Integer.parseInt(strs.get(2))));
                                    // character.setGravityState(GravityState.getGravityState(Integer.parseInt(strs.get(3))));
                                }
                            }
                            break;

                        case UPDATE_FRAME:
                            System.out.println("Update Frame");
                            for (Character character : players) {
                                if (ClientClass.getInstance().getID() == serialNum) {
                                    return;
                                }
                                if (character.id() == serialNum) {
                                    int x = Integer.parseInt(strs.get(0));
                                    int y = Integer.parseInt(strs.get(1));
                                    character.setPosition(x, y);
                                    character.setAnimationState(AnimationState.getAnimationState(Integer.parseInt(strs.get(2))));
                                    character.setGravityState(GravityState.getGravityState(Integer.parseInt(strs.get(3))));
                                }
                            }
                            break;
                        case START:
                            if (self.id() != 100) {
                                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
                                SceneController.instance().changeScene(new ConnectedScene());
                            }
                    }
                }
            });
        }
    }

    @Override
    public void paint(Graphics g) {

    }
}
