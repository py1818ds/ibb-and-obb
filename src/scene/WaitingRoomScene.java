package scene;

import animation.AnimationRole;
import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.characters.Character;
import internet.Client.ClientClass;
import internet.Server.Server;
import menu.BackgroundType.BackgroundImage;
import menu.Label.ClickedAction;
import menu.CustomFont;
import menu.Style;
import menu.Button;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;
import static utils.Global.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class WaitingRoomScene extends Scene {

    private Image icon;

    private Button start;
    private Button back;
    private CustomFont cf;
    private String name;
    private String ip;
    private int port;

    public WaitingRoomScene(String name, String ip, int port){
 
        if(ip.equals("127.0.0.1")){
            Character amy = new Character(200, 200, AnimationRole.AnimationArea.AMY) {
                @Override
                public void changeDirAndSelectAnimation(int code) {
                    if (code == LEFT) {
                        this.pressLeft();
                    } else if (code == RIGHT) {
                        this.pressRight();
                    } else if (code == UP) {
                        this.pressUp();
                    } else if (code == NONE) {
                        this.releaseKey();
                    } else if(code == SPACE){
                        this.useAbility();
                    }
                }
            };
            amy.setName(name);
            SceneTool.instance().setSelf(amy);
            SceneTool.instance().getPlayers().add(amy);
        }
        else{
            Character jimmy = new Character(125, 600, AnimationRole.AnimationArea.JIMMY) {
                @Override
                public void changeDirAndSelectAnimation(int code) {
                    if (code == LEFT) {
                        this.pressLeft();
                    } else if (code == RIGHT) {
                        this.pressRight();
                    } else if (code == UP) {
                        this.pressUp();
                    } else if (code == NONE) {
                        this.releaseKey();
                    } else if(code == SPACE) {
                        this.useAbility();
                    }
                }
            };
            jimmy.setName(name);
            SceneTool.instance().setSelf(jimmy);
            SceneTool.instance().getPlayers().add(jimmy);
        }
        this.ip = ip;
        this.port = port;
        this.name = name;
    }

    @Override
    public void sceneBegin() {
       try {
           SceneTool.instance().connect(ip, port);
        } catch (IOException e) {
           e.printStackTrace();
        }
        
        cf = new CustomFont();
        icon = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground());
        start = new Button(SCREEN_X - 200, SCREEN_Y - 150, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Start")
            .setTextFont(new Font("Nagurigaki Crayon", Font.PLAIN, 30))
            .setTextColor(Color.white));
        start.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Start")
            .setTextFont(new Font("Nagurigaki Crayon", Font.PLAIN, 30))
            .setTextColor(Color.pink));
        back = new Button( 150, SCREEN_Y - 150, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Nagurigaki Crayon", Font.PLAIN, 30))
            .setTextColor(Color.white));
        back.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Nagurigaki Crayon", Font.PLAIN, 30))
            .setTextColor(Color.pink));

        start.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
                SceneController.instance().changeScene(new ConnectedScene());
                ClientClass.getInstance().sent(START, bale(""));
            }
        });
        
        back.setClickedActionPerformed(new ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                ClientClass.getInstance().disConnect();
                if(SceneTool.instance().getSelf().id() == 100){
                    Server.instance().close();
                }
                SceneController.instance().changeScene(new MenuScene());
            }
        });  
    }

    @Override
    public void sceneEnd() {

    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(icon, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,null);

        // paint name
        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.white);
        g.drawString(name, SCREEN_X / 2, SCREEN_Y / 2);

        // paint buttons
       if(SceneTool.instance().getSelf().id() == 100){
           start.paint(g);
           back.paint(g);
       }

       // paint characters
       for(int i = 0; i < SceneTool.instance().getPlayers().size(); i++){
           SceneTool.instance().getPlayers().get(i).paint(g);
       }
    }
  

    @Override
    public void update() {
       ClientClass.getInstance().sent(CONNECT, bale(name));
       SceneTool.instance().consume();
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(start, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyReleased(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyTyped(char c, long trigTime) {

            }
        };
    }

}

