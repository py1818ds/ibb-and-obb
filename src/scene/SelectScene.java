package scene;

import utils.CommandSolver;
import utils.Global;

import java.awt.*;

public class SelectScene extends Scene {


    @Override
    public void sceneBegin() {

    }

    @Override
    public void sceneEnd() {

    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.ORANGE);
        g.drawString("按下a開啟飛機遊戲，按下b開啟蝙蝠俠遊戲", Global.SCREEN_X / 2 -150, Global.SCREEN_Y / 2);
    }

    @Override
    public void update() {

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
                if (commandCode == 1) {
                 //   SceneController.instance().changeScene(new MainScene());
                }


            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {

            }

            @Override
            public void keyTyped(char c, long trigTime) {


            }
        };
    }
}
