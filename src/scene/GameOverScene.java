package scene;

import utils.CommandSolver;
import utils.Global;

import java.awt.*;

public class GameOverScene extends Scene {

    private Scene scene;
    private long end;

    public GameOverScene(Scene scene) {
        this.scene = scene;
    }

    @Override
    public void sceneBegin() {
        end = scene.endTime;
    }

    @Override
    public void sceneEnd() {

    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.ORANGE);
        if (end != 0) {
            g.drawString(String.valueOf(end / 1000000000), 10, 32);
        }
        g.drawString("遊戲結束", Global.SCREEN_X / 2, Global.SCREEN_Y / 2);
    }

    @Override
    public void update() {

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}
