package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import menu.BackgroundType;
import menu.Button;
import menu.CustomFont;
import menu.Label;
import menu.Style;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.Global;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static utils.Global.SCREEN_X;

public class BetweenScene extends Scene {
    private Image img;
    private ArrayList<Button> buttons;
    private CustomFont cf;
    private Scene lastScene;

    public BetweenScene(Scene scene) {
        lastScene = scene;
    }

    @Override
    public void sceneBegin() {

        this.allPoint = lastScene.allPoint;
        this.name = lastScene.name;

        AudioResourceController.getInstance().loop(new Path().sounds().menuMusicOne(), -1);

        cf = new CustomFont();
        buttons = new ArrayList<>();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().titleBackground());

        buttons.add(new Button(SCREEN_X / 2 - 150, 180, new Style.StyleRect(340, 73, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("下一關")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25)) //Zpix SC
                .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 290, new Style.StyleRect(340, 73, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("返回主選單")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25))
                .setTextColor(Color.WHITE)));

        buttons.get(0).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button_Hover())))
                .setText("下一關")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25))
                .setTextColor(new Color(3, 30, 45)));
        buttons.get(1).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button_Hover())))
                .setText("返回主選單")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25))
                .setTextColor(new Color(3, 30, 45)));

        buttons.get(0).setClickedActionPerformed((x, y) -> {
            AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
            if (lastScene.getClass() == LevelOne.class) {
                SceneController.instance().changeScene(new LevelTwo(allPoint, name));
            }
            if (lastScene.getClass() == LevelTwo.class) {
                SceneController.instance().changeScene(new LevelThree(allPoint, name));
            }
        }
        );
        buttons.get(1).setClickedActionPerformed((x, y) -> {
            AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
            SceneController.instance().changeScene(new MenuScene());
        }

        );

    }

    @Override
    public void sceneEnd() {

    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 720, null);
        for (int i = 0; i < buttons.size(); i++) {
            if (lastScene.getClass() == TutorialScene.class) {
                buttons.get(1).paint(g);
                break;
            }
            buttons.get(i).paint(g);
        }
        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.WHITE);
        g.drawString("您目前的分數是 :" + allPoint, SCREEN_X / 2 - 100, 100);
    }

    @Override
    public void update() {

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }
}
