package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import menu.BackgroundType;
import menu.BackgroundType.BackgroundImage;
import menu.CustomFont;
import menu.EditText;
import menu.Label;
import menu.Style;
import menu.Button;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;

import static utils.Global.SCREEN_X;
import static utils.Global.SCREEN_Y;

public class InputTeamNameScene extends Scene {

    private Image img;

    private Button start;
    private Button back;

    private static EditText inputName;
    private CustomFont cf;

    @Override
    public void sceneBegin() {
        cf = new CustomFont();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground()); // 替換遊戲畫面

        inputName = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 75, "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 25)));

        start = new Button(SCREEN_X / 2 + 50, SCREEN_Y / 2 + 100, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().      blankButton())))
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.white));

        start.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.pink));

        back = new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 + 100, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.white));

        back.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.pink));

        back.setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new MenuScene())
        );
    }

    @Override
    public void sceneEnd() {
        img = null;
        start = null;
        back = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 600,null);

        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.white);
        g.drawString("Please enter your team name:", SCREEN_X / 2 - 150, SCREEN_Y / 2 - 100);

        start.paint(g);
        back.paint(g);

        inputName.paint(g);

    }
  

    @Override
    public void update() {
        if (!inputName.getEditText().isEmpty()) {
            start.setClickedActionPerformed(new Label.ClickedAction() {
                @Override
                public void clickedActionPerformed(int x, int y) {
                    AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
                    SceneController.instance().changeScene(new LevelOne(inputName.getEditText()));
                }
            });
        }
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(start, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
                MouseTriggerImpl.mouseTrig(inputName, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyReleased(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyTyped(char c, long trigTime) {
                inputName.keyTyped(c);
            }
        };
    }

    public static String getTeamName(){
        return inputName.getEditText();
    }

}

