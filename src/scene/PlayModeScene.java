package scene;

import controllers.SceneController;
import menu.BackgroundType.BackgroundImage;
import menu.Button;
import menu.CustomFont;
import menu.*;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static utils.Global.SCREEN_X;
import static utils.Global.SCREEN_Y;

public class PlayModeScene extends Scene {
    private Image img;
    private ArrayList<Button> buttons;
    private CustomFont cf;


    @Override
    public void sceneBegin() {
        cf = new CustomFont();
        buttons = new ArrayList<Button>();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().titleBackground());

        buttons.add(new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 120, new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("合作模式")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 10, new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("競賽模式")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 + 100, new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("回到上一頁")
            .setTextFont(new Font("Zpix", Font.PLAIN, 30))
            .setTextColor(Color.WHITE)));
        
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Cooperation Mode")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("1V1 Mode")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(2).setStyleHover(new Style.StyleRect(300, 70, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Go Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        
        buttons.get(0).setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new InputTeamNameScene())
        );
        buttons.get(1).setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new InputNameScene())
        );
        buttons.get(2).setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new MenuScene())
        );

    }

    @Override
    public void sceneEnd() {
        img = null;
        buttons = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 600,null);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }

    }

    @Override
    public void update() {
       
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                for(int i = 0; i < buttons.size(); i++){
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };

    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

