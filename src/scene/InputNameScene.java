package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;

import static utils.Global.*;
import utils.Path;
import menu.CustomFont;
import menu.EditText;
import java.awt.*;
import java.awt.event.MouseEvent;

import menu.BackgroundType;
import menu.Button;
import menu.Style;
import menu.BackgroundType.BackgroundImage;
import menu.impl.MouseTriggerImpl;

public class InputNameScene extends Scene {

    private Image img;

    private Button start;
    private Button back;

    private EditText inputNameOne;
    private EditText inputNameTwo;
    private CustomFont cf;

    @Override
    public void sceneBegin() {
        cf = new CustomFont();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground()); // 替換遊戲畫面

        inputNameOne = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 125, "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 25)));
        inputNameTwo = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 , "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 25)));

        start = new Button(SCREEN_X / 2 + 50, SCREEN_Y / 2 + 100, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton()))) 
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.white));

        start.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.pink));

        back = new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 + 100, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.white));

        back.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.pink));

        back.setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new ConnectModeScene())
        );
    }

    @Override
    public void sceneEnd() {
        img = null;
        start = null;
        back = null;

    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 600,null);

        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.white);
        g.drawString("Please enter your name:", SCREEN_X / 2 - 150, SCREEN_Y / 2 - 150);
        g.drawString("Please enter your name:", SCREEN_X / 2 - 150, SCREEN_Y / 2 - 25);

        start.paint(g);
        back.paint(g);

        inputNameOne.paint(g);
        inputNameTwo.paint(g);

    }
  

    @Override
    public void update() {
        if (!inputNameOne.getEditText().isEmpty() && !inputNameTwo.getEditText().isEmpty()) {
            start.setClickedActionPerformed((x, y) -> {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
                SceneController.instance().changeScene(new PKScene(inputNameOne.getEditText(), inputNameTwo.getEditText()));
            });

        }
    }
    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(start, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
                MouseTriggerImpl.mouseTrig(inputNameOne, e, state);
                MouseTriggerImpl.mouseTrig(inputNameTwo, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyReleased(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyTyped(char c, long trigTime) {
                inputNameOne.keyTyped(c);
                inputNameTwo.keyTyped(c);
            }
        };
    }

    public String getName(){
        return inputNameOne.getEditText();
    }

}

