package scene;

import controllers.SceneController;
import menu.BackgroundType;
import menu.BackgroundType.BackgroundImage;
import menu.Button;
import menu.CustomFont;
import menu.EditText;
import menu.*;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import static utils.Global.*;

public class ConnectScene extends Scene {

    private Image img;

    private Button create;
    private Button back;

    private EditText inputPort;
    private EditText inputName;
    private EditText inputIp;
    private CustomFont cf;

    @Override
    public void sceneBegin() {
        cf = new CustomFont();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground()); // 替換遊戲畫面

        inputName = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 150, "Name", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 30)));
        inputIp = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 - 50, "Ip", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 30)));
        inputPort = new EditText(SCREEN_X / 2 - 150, SCREEN_Y / 2 + 50, "Port", new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundColor(Color.lightGray))
            .setBorderColor(Color.DARK_GRAY)
            .setBorderThickness(3)
            .setHaveBorder(true)
            .setTextFont(new Font("Zpix", Font.PLAIN, 30)));

        create = new Button(SCREEN_X / 2 + 50, SCREEN_Y / 2 + 150, new Style.StyleRect(100 , 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().      transparentRect())))
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));

        create.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Create")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.pink));

        back = new Button(SCREEN_X / 2 - 150, SCREEN_Y / 2 + 150, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));

        back.setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))
            .setText("Back")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.pink));

        // create.setClickedActionPerformed((int x, int y) -> 
        //     SceneController.instance().changeScene(new WaitingRoomScene(inputName.getEditText(), inputIp.getEditText(), Integer.parseInt(inputPort.getEditText())))
        // );

        back.setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new MenuScene())
        );
    }

    @Override
    public void sceneEnd() {
        img = null;
        create = null;
        back = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,null);

        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.white);
        g.drawString("Please enter your name:", SCREEN_X / 2 - 150, SCREEN_Y / 2 - 175);
        g.drawString("Please enter your ip:", SCREEN_X / 2 - 150, SCREEN_Y / 2 - 70);
        g.drawString("Please enter your port:", SCREEN_X / 2 - 150, SCREEN_Y / 2 + 30 );

        create.paint(g);
        back.paint(g);

        inputName.paint(g);
        inputIp.paint(g);
        inputPort.paint(g);

    }
  

    @Override
    public void update() {
        if((!inputName.getEditText().isEmpty()) && (!inputPort.getEditText().isEmpty()) && (!inputIp.getEditText().isEmpty())){
            create.setClickedActionPerformed((int x, int y) -> 
                SceneController.instance().changeScene(new WaitingRoomScene(inputName.getEditText(), inputIp.getEditText(), Integer.parseInt(inputPort.getEditText())))
            );
        }
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                MouseTriggerImpl.mouseTrig(create, e, state);
                MouseTriggerImpl.mouseTrig(back, e, state);
                MouseTriggerImpl.mouseTrig(inputName, e, state);
                MouseTriggerImpl.mouseTrig(inputPort, e, state);
                MouseTriggerImpl.mouseTrig(inputIp, e, state);
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            @Override
            public void keyPressed(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyReleased(int commandCode, long trigTime) {
        
            }
        
            @Override
            public void keyTyped(char c, long trigTime) {
                inputPort.keyTyped(c);
                inputIp.keyTyped(c);
                inputName.keyTyped(c);
            }
        };
    }

}

