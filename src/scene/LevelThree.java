package scene;

import animation.AnimationRole;
import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GravityState;
import game_object.Rect;
import game_object.enemy.*;
import game_object.map_object.*;
import game_object.item_object.*;
import game_object.map_object.FloorNormal;
import game_object.characters.Character;
import menu.Button;
import menu.Style;
import menu.*;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import utils.Delay;
import utils.Global;

import maploader.*;
import scene_process.Camera;
import utils.Path;

import javax.imageio.ImageIO;


public class LevelThree extends Scene {
    public LevelThree(int point, String inputName) {
        allPoint = point;
        name = inputName;
    }
    private ArrayList<FloorNormal> floors;
    private ArrayList<CrossingLineX> crossingXLines;
    private ArrayList<CrossingLineY> crossingYLines;
    private ArrayList<JumpFloor> jumpFloors;
    private ArrayList<TrapFloor> trapFloors;
    private ArrayList<GravityBall> gravityBalls;
    private ArrayList<Grass> grasses;
    private ArrayList<Tree> trees;
    private ArrayList<RebornPoint> rebornPoints;
    private VictoryPoint victoryPoint;
    private ArrayList<Item> items;
    private ArrayList<Enemy> enemies;

    private Character actor;
    private Character jimmy;
    private RebornPoint lastRebornPoint;
    private Delay delayForDead;
    private Delay delayForVictory;
    private int JimmySpeed;
    private int AmySpeed;
    private boolean pause;
    private boolean isVictory;
    private int currentPoint;

    private Camera camera;

    private Image background;
    private Image stopImg;
    private Image screenFilter;//濾鏡

    private ArrayList<Button> buttons;

    @Override
    public void sceneBegin() {
        AudioResourceController.getInstance().loop(new Path().sounds().levelThreeMusic(), -1);
        stopImg = SceneController.instance().irc().tryGetImage(new Path().img().menu().stop());

        currentPoint = 0;

        grasses = new ArrayList<>();
        floors = new ArrayList<>();
        jumpFloors = new ArrayList<>();
        trapFloors = new ArrayList<>();
        crossingXLines = new ArrayList<>();
        crossingYLines = new ArrayList<>();
        gravityBalls = new ArrayList<>();
        rebornPoints = new ArrayList<>();
        trees = new ArrayList<>();

        items = new ArrayList<>();

        enemies = new ArrayList<>();

        camera = new Camera(23500, 3600);
        camera.setSensitivityX(7,138);
        camera.setSensitivityY(7,60);
        actor = new Character(200, 300, AnimationRole.AnimationArea.AMY) {
            @Override
            public void changeDirAndSelectAnimation(int code) {
                if (code == Global.LEFT) {
                    actor.pressLeft();
                } else if (code == Global.RIGHT) {
                    actor.pressRight();
                } else if (code == Global.UP) {
                    actor.pressUp();
                } else if (code == Global.NONE) {
                    actor.releaseKey();
                } else if (code == Global.SPACE) {
                    actor.useAbility();
                }
            }
        };
        jimmy = new Character(125, 300, AnimationRole.AnimationArea.JIMMY) {
            @Override
            public void changeDirAndSelectAnimation(int code) {
                if (code == Global.P2_LEFT) {
                    jimmy.pressLeft();
                } else if (code == Global.P2_RIGHT) {
                    jimmy.pressRight();
                } else if (code == Global.P2_UP) {
                    jimmy.pressUp();
                } else if (code == Global.P2_NONE) {
                    jimmy.releaseKey();
                } else if (code == Global.CTRL) {
                    jimmy.useAbility();
                }
            }
        };

        try {
            background = ImageIO.read(Objects.requireNonNull(getClass().getResource("/resources/imgs/backgrounds/background-red.png")));
            screenFilter = ImageIO.read(Objects.requireNonNull(getClass().getResource("/resources/imgs/backgrounds/back.png")));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        actor.setPartner(jimmy);
        jimmy.setPartner(actor);
        camera.setTarget(actor);
        camera.setTarget(jimmy);
        camera.setChaseX(8);
        camera.setChaseY(8);
        mapInitialize();
        enemyInitialize();

        delayForDead = new Delay(90);
        delayForVictory = new Delay(180);

        isVictory = false;

        buttons = new ArrayList<>();
        buttons.add(new Button(Global.SCREEN_X / 2 - 150, 200, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("Continue")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.white)));
        buttons.add(new Button(Global.SCREEN_X / 2 - 150, 300, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("ReStart")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.white)));
        buttons.add(new Button(Global.SCREEN_X / 2 - 150, 400, new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("Back to Menu")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.white)));
        buttons.get(0).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button_Hover())))
                .setText("Continue")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(new Color(3, 30, 45)));
        buttons.get(1).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button_Hover())))
                .setText("Restart")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(new Color(3, 30, 45)));
        buttons.get(2).setStyleHover(new Style.StyleRect(300, 50, true, new BackgroundType.BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button_Hover())))
                .setText("Back to Menu")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(new Color(3, 30, 45)));
        buttons.get(0).setClickedActionPerformed((x, y) -> pause = !pause);
        buttons.get(1).setClickedActionPerformed((x, y) -> {
            AudioResourceController.getInstance().stop(new Path().sounds().levelThreeMusic());
            SceneController.instance().changeScene(new LevelThree(allPoint,name));
        });
        buttons.get(2).setClickedActionPerformed((x, y) -> {
            AudioResourceController.getInstance().stop(new Path().sounds().levelThreeMusic());
            SceneController.instance().changeScene(new MenuScene());
        });
    }

    @Override
    public void sceneEnd() {

    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g.setColor(new Color(0, 0, 0));
        g.fillRect(0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT);
        camera.startCamera(g2d);
        for(int i=0; i<(409*32)/3840; i++){
            g.drawImage(background, 3840*i, -400, 3840, 2160, null);
        }


        for (Grass grass : grasses) {
            grass.paint(g);
        }
        for (RebornPoint r : rebornPoints) {
            r.paint(g);
        }
        for (GravityBall gravityBall : gravityBalls) {
            gravityBall.paint(g);
        }
        for (JumpFloor j : jumpFloors) {
            j.paint(g);
        }
        for (FloorNormal floor : floors) {
            floor.paint(g);
        }
        for (CrossingLineX c : crossingXLines) {
            c.paint(g);
        }
        for (CrossingLineY cLR : crossingYLines) {
            cLR.paint(g);
        }
        for (Item item : items) {
            item.paint(g);
        }
        for (Enemy e : enemies) {
            e.paint(g);
        }
        for (TrapFloor trapFloor : trapFloors) {
            trapFloor.paint(g);
        }
        for (Tree tree : trees) {
            tree.paint(g);
        }
        victoryPoint.paint(g);

        actor.paint(g);
        jimmy.paint(g);

        camera.paint(g);

        camera.endCamera(g2d);
        g2d.drawImage(screenFilter, 0, 0, 1280, 720, null);
        g.setColor(new Color(255, 198, 0, 255));
        g.setFont(new Font("Zpix", Font.PLAIN, 30));
        g.drawString("point:" + (allPoint + currentPoint), 50, 50);

        if (actor.animationState() == Character.AnimationState.PARTNER_TOO_LEFT || actor.animationState() == Character.AnimationState.PARTNER_TOO_RIGHT) {
            g.setColor(new Color(255, 255, 255, 255));
            g.setFont(new Font("Zpix", Font.PLAIN, 30));
            g.drawString("距離太遠了!", Global.SCREEN_X / 2 - 82, 650);
        }

        if (pause) {
            g.drawImage(stopImg, 0, 0, null);
            for (Button button : buttons) {
                button.paint(g);
            }
        }
        if(isVictory){
            g.drawImage(stopImg, 0, 0, null);
            g.setColor(new Color(255, 255, 255, 255));
            g.setFont(new Font("Zpix", Font.PLAIN, 80));
            g.drawString("Victory!!!", Global.SCREEN_X / 2 - 82, Global.SCREEN_Y / 2 - 40);
        }
    }

    @Override
    public void update() {

        if (pause) {
            return;
        }
        //優先順序

        // 用for迴圈一起寫會讓一個角色判斷完所有事件後才判斷另一個角色，順序上會有問題，所以沿用舊方法，把碰撞拉出來一個一個做判斷

        /* 所有物件碰撞與計算 */
        currentPoint += actor.point();
        currentPoint += jimmy.point();
        actor.resetPoint();
        jimmy.resetPoint();
        actor.checkPosition();
        jimmy.checkPosition();
        actor.update();
        jimmy.update();
        actor.hitItem(items);
        jimmy.hitItem(items);
        checkRebornPoint(actor);
        checkRebornPoint(jimmy);
        if(touchEnemy(actor,enemies)){
            enemies.clear();
            enemyInitialize();
        }
        if(touchEnemy(jimmy,enemies)){
            enemies.clear();
            enemyInitialize();
        }
        actor.hitGravityBalls(gravityBalls);
        jimmy.hitGravityBalls(gravityBalls);
        if(actor.isCollision(victoryPoint) || jimmy.isCollision(victoryPoint)){
            delayForVictory.play();
            actor.setAnimationState(Character.AnimationState.VICTORY);
            jimmy.setAnimationState(Character.AnimationState.VICTORY);
            AudioResourceController.getInstance().stop(new Path().sounds().levelThreeMusic());
            AudioResourceController.getInstance().play(new Path().sounds().victory());
            isVictory = true;
        }

        /* 地圖物件會變更或動畫的就要update */
        for (CrossingLineX tmpCrossingX : crossingXLines) {
            tmpCrossingX.update();
        }
        for (CrossingLineY tmpCrossingY : crossingYLines) {
            tmpCrossingY.update();
        }

        for (TrapFloor tFloor : trapFloors) {
            // 當正常重力場時
            if (actor.gravityState() == GravityState.NORMAL_GRAVITY) {
                if (tFloor.sthOnTop(actor)) { // 當在上面
                    if (actor.isNextCollision(tFloor)) {
                        actor.translateY(-(actor.collider().bottom() - tFloor.collider().top()));
                        actor.setSpeedY(0);
                        actor.setCanJump(true);
                        tFloor.changeState();
                    }
                }
                if (tFloor.sthOnBottom(actor)) { //當在下面
                    if (actor.isNextCollision(tFloor)) {
                        actor.translateY(tFloor.collider().bottom() - actor.collider().top());
                        actor.setSpeedY(0);
                    }
                } else if (tFloor.sthOnRight(actor)) { //當在右邊
                    if (actor.isNextCollision(tFloor)) {
                        if (actor.collider().bottom() > tFloor.collider().top()) {
                            actor.translateX(-(actor.collider().right() - tFloor.collider().left()));
                        }
                    }
                } else if (tFloor.sthOnLeft(actor)) { //當在左邊
                    if (actor.isNextCollision(tFloor)) {
                        if (actor.collider().bottom() > tFloor.collider().top()) {
                            actor.translateX(tFloor.collider().right() - actor.collider().left());
                        }
                    }
                }
            } else {
                if (tFloor.sthOnTop(actor)) { // 當在上面
                    if (actor.isNextCollision(tFloor)) {
                        actor.translateY(-(actor.collider().bottom() - tFloor.collider().top()));
                        actor.setSpeedY(0);
                    }
                }
                if (tFloor.sthOnBottom(actor)) { //當在下面
                    if (actor.isNextCollision(tFloor)) {
                        actor.translateY(tFloor.collider().bottom() - actor.collider().top());
                        actor.setSpeedY(0);
                        actor.setCanJump(true);
                        tFloor.changeState();
                    }
                } else if (tFloor.sthOnRight(actor)) { //當在右邊
                    if (actor.isNextCollision(tFloor)) {
                        if (actor.collider().bottom() > tFloor.collider().top()) {
                            actor.translateX(-(actor.collider().right() - tFloor.collider().left()));
                        }
                    }
                } else if (tFloor.sthOnLeft(actor)) { //當在左邊
                    if (actor.isNextCollision(tFloor)) {
                        if (actor.collider().bottom() > tFloor.collider().top()) {
                            actor.translateX(tFloor.collider().right() - actor.collider().left());
                        }
                    }
                }
            }
            tFloor.update();
        }
        for (TrapFloor tFloor : trapFloors) {
            // 當正常重力場時
            if (jimmy.gravityState() == GravityState.NORMAL_GRAVITY) {
                if (tFloor.sthOnTop(jimmy)) { // 當在上面
                    if (jimmy.isNextCollision(tFloor)) {
                        jimmy.translateY(-(jimmy.collider().bottom() - tFloor.collider().top()));
                        jimmy.setSpeedY(0);
                        jimmy.setCanJump(true);
                        tFloor.changeState();
                    }
                }
                if (tFloor.sthOnBottom(jimmy)) { //當在下面
                    if (jimmy.isNextCollision(tFloor)) {
                        jimmy.translateY(tFloor.collider().bottom() - jimmy.collider().top());
                        jimmy.setSpeedY(0);
                    }
                } else if (tFloor.sthOnRight(jimmy)) { //當在右邊
                    if (jimmy.isNextCollision(tFloor)) {
                        if (jimmy.collider().bottom() > tFloor.collider().top()) {
                            jimmy.translateX(-(jimmy.collider().right() - tFloor.collider().left()));
                        }
                    }
                } else if (tFloor.sthOnLeft(jimmy)) { //當在左邊
                    if (jimmy.isNextCollision(tFloor)) {
                        if (jimmy.collider().bottom() > tFloor.collider().top()) {
                            jimmy.translateX(tFloor.collider().right() - jimmy.collider().left());
                        }
                    }
                }
            } else {
                if (tFloor.sthOnTop(jimmy)) { // 當在上面
                    if (jimmy.isNextCollision(tFloor)) {
                        jimmy.translateY(-(jimmy.collider().bottom() - tFloor.collider().top()));
                        jimmy.setSpeedY(0);
                    }
                }
                if (tFloor.sthOnBottom(jimmy)) { //當在下面
                    if (jimmy.isNextCollision(tFloor)) {
                        jimmy.translateY(tFloor.collider().bottom() - jimmy.collider().top());
                        jimmy.setSpeedY(0);
                        jimmy.setCanJump(true);
                        tFloor.changeState();
                    }
                } else if (tFloor.sthOnRight(jimmy)) { //當在右邊
                    if (jimmy.isNextCollision(tFloor)) {
                        if (jimmy.collider().bottom() > tFloor.collider().top()) {
                            jimmy.translateX(-(jimmy.collider().right() - tFloor.collider().left()));
                        }
                    }
                } else if (tFloor.sthOnLeft(jimmy)) { //當在左邊
                    if (jimmy.isNextCollision(tFloor)) {
                        if (jimmy.collider().bottom() > tFloor.collider().top()) {
                            jimmy.translateX(tFloor.collider().right() - jimmy.collider().left());
                        }
                    }
                }
            }
            tFloor.update();
        }
        for (int i = 0; i < trapFloors.size(); i++) {
            TrapFloor tf = trapFloors.get(i);
            if (tf.state() == TrapFloor.State.REMOVED) {
                trapFloors.remove(i--);
            }
        }

        for (JumpFloor jFloor : jumpFloors) {
            if (actor.gravityState() == GravityState.NORMAL_GRAVITY && jimmy.gravityState() == GravityState.ANTI_GRAVITY ||
                    actor.gravityState() == GravityState.ANTI_GRAVITY && jimmy.gravityState() == GravityState.NORMAL_GRAVITY) { //當不在同一邊
                if (jimmy.gravityState() == GravityState.NORMAL_GRAVITY && actor.isCollision(jFloor)) {
                    if (jimmy.collider().bottom() < jFloor.collider().top()) { //Amy下一楨從上面來
                        if (jimmy.collider().right() < jFloor.collider().right() && jimmy.collider().left() > jFloor.collider().left()) {// 如果Amy在地板左右範圍內
                            jFloor.setState(JumpFloor.State.JIMMY_JUMP);
                        } else {
                            jFloor.setState(JumpFloor.State.NONE);
                        }
                    }
                } else if (jimmy.gravityState() == GravityState.ANTI_GRAVITY && actor.isCollision(jFloor)) { //在下面重力場
                    if (jimmy.collider().top() > jFloor.collider().bottom()) { //下一楨從下面來
                        // 如果Amy在地板左右範圍內
                        if (jimmy.collider().right() < jFloor.collider().right() && jimmy.collider().left() > jFloor.collider().left()) {
                            jFloor.setState(JumpFloor.State.JIMMY_JUMP);
                        } else {
                            jFloor.setState(JumpFloor.State.NONE);
                        }
                    }
                }
            }
            if (jimmy.isNextCollision(jFloor)) {
                JimmySpeed = jimmy.speedY();//記錄自己的速度，並且把速度給對方
            }
            if (jimmy.isCollision(jFloor)) { // 碰到
                if (jFloor.state() == JumpFloor.State.AMY_JUMP && actor.isCollision(jFloor)) {
                    jimmy.setSpeedY(AmySpeed);
                    jimmy.setCanJump(false);
                } else {
                    if (jFloor.sthOnTop(jimmy)) {  // 當在上面
                        jimmy.translateY(-(jimmy.collider().bottom() - jFloor.collider().top()));
                        jimmy.setCanJump(false);
                        jimmy.setSpeedY(0);
                    }
                    if (jFloor.sthOnBottom(jimmy)) { //當在下面
                        jimmy.translateY(jFloor.collider().bottom() - jimmy.collider().top());
                        jimmy.setCanJump(false);
                        jimmy.setSpeedY(0);
                    }
                }
                if (!actor.isCollision(jFloor) && jFloor.state() != JumpFloor.State.AMY_JUMP) {
                    jFloor.setState(JumpFloor.State.NONE);
                }
            }
        }
        for (JumpFloor jFloor : jumpFloors) {
            //當不在同一邊
            if (actor.gravityState() == GravityState.NORMAL_GRAVITY && jimmy.gravityState() == GravityState.ANTI_GRAVITY ||
                    actor.gravityState() == GravityState.ANTI_GRAVITY && jimmy.gravityState() == GravityState.NORMAL_GRAVITY) {
                //如果Amy在上面重力場，jimmy碰到地板
                if (actor.gravityState() == GravityState.NORMAL_GRAVITY && jimmy.isCollision(jFloor)) {
                    //Amy下一楨從上面來
                    if (actor.collider().bottom() < jFloor.collider().top()) {
                        AmySpeed = 0;
                        // 如果Amy在地板左右範圍內
                        if (actor.collider().right() < jFloor.collider().right() && actor.collider().left() > jFloor.collider().left()) {
                            jFloor.setState(JumpFloor.State.AMY_JUMP);
                        } else {
                            jFloor.setState(JumpFloor.State.NONE);
                        }
                    }
                } else if (actor.gravityState() == GravityState.ANTI_GRAVITY && jimmy.isCollision(jFloor)) { //在下面重力場
                    if (actor.collider().top() > jFloor.collider().bottom()) { //下一楨從下面來
                        // 如果Amy在地板左右範圍內
                        if (actor.collider().right() < jFloor.collider().right() && actor.collider().left() > jFloor.collider().left()) {
                            jFloor.setState(JumpFloor.State.AMY_JUMP);
                        } else {
                            jFloor.setState(JumpFloor.State.NONE);
                        }
                    }
                }
            }
            if (actor.isNextCollision(jFloor)) {
                AmySpeed = actor.speedY();
            }
            if (actor.isCollision(jFloor)) { // 如果Amy碰到地板
                //如果Jimmy跳過來並且在地板上
                if (jFloor.state() == JumpFloor.State.JIMMY_JUMP && jimmy.isCollision(jFloor)) {
                    System.out.println("Jimmy Speed: " + JimmySpeed);
                    actor.setSpeedY(JimmySpeed);
                    actor.setCanJump(false);
                } else {
                    if (jFloor.sthOnTop(actor)) {  // 當在上面
                        actor.translateY(-(actor.collider().bottom() - jFloor.collider().top()));
                        actor.setCanJump(false);
                        actor.setSpeedY(0);
                    }
                    if (jFloor.sthOnBottom(actor)) { //當在下面
                        actor.translateY(jFloor.collider().bottom() - actor.collider().top());
                        actor.setCanJump(false);
                        actor.setSpeedY(0);
                    }
                }
                if (!jimmy.isCollision(jFloor) && jFloor.state() != JumpFloor.State.JIMMY_JUMP) {
                    jFloor.setState(JumpFloor.State.NONE);
                }
            }
        }

        for (GravityBall gBall : gravityBalls) {
            gBall.update();
        }
        actor.hitFloors(floors);
        jimmy.hitFloors(floors);
        standOnEachOther(actor,jimmy);
        pushEachOther(actor,jimmy);
        actor.hitCrossingFloors(crossingXLines,crossingYLines);
        jimmy.hitCrossingFloors(crossingXLines,crossingYLines);
        actor.hitTrees(trees);
        jimmy.hitTrees(trees);

        for (Enemy enemy : enemies) {
            enemy.update();
        }

        /* 更換場景 */
        if (delayForVictory.count()) {
            allPoint += currentPoint;
            ArrayList<String> list = readCoFile("src/rank/Co-opList.txt", name, allPoint);
            writeFile(list , "src/rank/Co-opList.txt");
            SceneController.instance().changeScene(new RankingScene());
        }

        /* 鏡頭更新 */
        camera.update();
    }

    private void mapInitialize() {//建構地圖
        try {
            ArrayList<MapInfo> mapInfo = new MapLoader("level3.bmp", "genMap.txt").combineInfo();
            for (MapInfo tmp : mapInfo) {  // 地圖產生器內物件的 {左, 上, 寬, 高}
                switch (tmp.getName()) {
                    case "floor_up" -> //  需中心座標
                            floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                    32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.UP, 3));
                    case "floor_down" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.DOWN, 3));
                    case "floor_left" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT, 3));
                    case "floor_right" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT, 3));
                    case "floor_left_up_convex" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_UP_CONVEX, 3));
                    case "floor_right_up_convex" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_UP_CONVEX, 3));
                    case "floor_left_down_convex" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_DOWN_CONVEX, 3));
                    case "floor_right_down_convex" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_DOWN_CONVEX, 3));
                    case "floor_left_up_concave" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_UP_CONCAVE, 3));
                    case "floor_right_up_concave" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_UP_CONCAVE, 3));
                    case "floor_left_down_concave" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_DOWN_CONCAVE, 3));
                    case "floor_right_down_concave" -> floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_DOWN_CONCAVE, 3));
                    case "cross-3x2" -> crossingXLines.add(new CrossingLineX(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 36, 32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "cross2x3" -> crossingYLines.add(new CrossingLineY(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 48, 32 * tmp.getSizeY(), 32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "coin" -> items.add(new Coin(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                    case "jumpFloor" -> jumpFloors.add(new JumpFloor(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2 - 1, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "gravity_ball" -> gravityBalls.add(new GravityBall(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "trapFloor" -> trapFloors.add(new TrapFloor(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "reborn" -> rebornPoints.add(new RebornPoint(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "axe" -> items.add(new Axe(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                    case "bell" -> items.add(new BellMagnet(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                    case "flag" -> victoryPoint = new VictoryPoint(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY());
                    case "speedDrink" -> items.add(new FruitCanSpeedUp(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                    case "tree" -> trees.add(new Tree(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                    case "star" -> items.add(new Star(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                    case "grass" -> grasses.add(new Grass(32 * tmp.getX(),
                            32 * tmp.getY(), 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TutorialScene.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*重生時怪也重生*/
    private void enemyInitialize() {
        try {
            ArrayList<MapInfo> mapInfo = new MapLoader("level3.bmp", "genMap.txt").combineInfo();
            for (MapInfo tmp : mapInfo) {  // 地圖產生器內物件的 {左, 上, 寬, 高}
                switch (tmp.getName()) {
                    case "enemy1" -> enemies.add(new EnemyHorizontal_Move(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.NORMAL_GRAVITY));
                    case "enemy" -> enemies.add(new EnemyHorizontal_Jump(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.NORMAL_GRAVITY));
                    case "enemy3" -> enemies.add(new EnemyVertical(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.NORMAL_GRAVITY));
                    case "enemy_pumpkin_down" -> enemies.add(new EnemyHorizontal_Move(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.ANTI_GRAVITY));
                    case "enemy_fly_down" -> enemies.add(new EnemyVertical(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.ANTI_GRAVITY));
                    case "enemy_slime_down" -> enemies.add(new EnemyHorizontal_Jump(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.ANTI_GRAVITY));
                    case "boss" -> enemies.add(new Enemy_Boss(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                            32 * tmp.getY() + 32 * tmp.getSizeY() / 2, GravityState.NORMAL_GRAVITY));

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TutorialScene.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*互踩function*/
    private void standOnEachOther(Character p1, Character p2) {
        if (p1.gravityState() != p2.gravityState()) {
            return;
        }
        if(p1.isTouchCrossingLine() || p2.isTouchCrossingLine()){
            return;
        }
        Rect colliderP1 = p1.collider();
        Rect colliderP2 = p2.collider();
        //判斷是否碰撞
        //相切算碰撞
        if (colliderP1.right() <= colliderP2.left()) {
            return;
        }
        if (colliderP1.left() >= colliderP2.right()) {
            return;
        }
        if (colliderP1.top() > colliderP2.bottom()) {
            return;
        }
        if (colliderP1.bottom() < colliderP2.top()) {
            return;
        }


        if (p1.gravityState() == GravityState.NORMAL_GRAVITY) {//如果這一幀是從上面穿越碰撞//actor比Jimmy高(x較小)
            if ((colliderP1.bottom() > colliderP2.top()
                    && colliderP1.bottom() < colliderP2.bottom())) {//
                p1.translateY(-(colliderP1.bottom() - colliderP2.top()));
                p1.setSpeedY(0);
                p1.setCanFalling(false);
                p1.setCanJump(true);
            }
            else if (colliderP1.top() < colliderP2.bottom()
                    && colliderP1.bottom() > colliderP2.bottom()) {//如果這一幀是從上面穿越碰撞//actor比Jimmy低(x較大)
                p2.translateY(-(colliderP2.bottom() - colliderP1.top()));
                p2.setSpeedY(0);
                p2.setCanFalling(false);
                p2.setCanJump(true);
            }
        } else {//如果下一幀是從上面穿越碰撞//amy比jimmy高(x較小)
            if (colliderP1.top() < colliderP2.bottom()
                    && colliderP1.top() > colliderP2.top()) {
                p1.translateY(-(colliderP1.top() - colliderP2.bottom()));
                p1.setSpeedY(0);
                p1.setCanFalling(false);
                p1.setCanJump(true);
            } else if (colliderP1.bottom() > colliderP2.top()
                    && colliderP1.top() < colliderP2.top()) {
                p2.translateY(-(colliderP2.top() - colliderP1.bottom()));
                jimmy.setSpeedY(0);//改1
                p2.setCanFalling(false);
                p2.setCanJump(true);
            }
        }


    }

    /*互推function*/
    private void pushEachOther(Character amy, Character jimmy) {
        if(amy.gravityState() != jimmy.gravityState()){
            return;
        }
        if(amy.isTouchCrossingLine() || jimmy.isTouchCrossingLine()){
            return;
        }
//        if(amy.crossingFrom()!= Character.CrossingState.None || jimmy.crossingFrom()!= Character.CrossingState.None){
//            return;
//        }
        if(!amy.isCollision(jimmy)){
            return;
        }
        Rect colliderAmy = amy.collider();
        Rect colliderJimmy = jimmy.collider();


        //amy上跳時//amy還沒越過jimmy 所以可以推
        if (colliderAmy.left() < colliderJimmy.left() && colliderAmy.right() > colliderJimmy.left()
                && amy.speedX() >= 0
                && colliderAmy.bottom() == colliderJimmy.bottom()//Y軸判斷
                && amy.isCollision(jimmy) //碰撞(包含相切)
        ) {
            System.out.println("媽我在這");
            amy.translateX(-(colliderAmy.right() - colliderJimmy.left()));
            jimmy.translateX(Global.ROLE_SPEED_X);
        }
        // Amy從右撞
        else if (colliderAmy.right() > colliderJimmy.right() //amy在jimmy右邊
                && colliderAmy.left() < colliderJimmy.right()
                && colliderAmy.bottom() == colliderJimmy.bottom()
                && amy.isCollision(jimmy) //碰撞
        ) {
            amy.translateX(colliderJimmy.right() - colliderAmy.left());
            jimmy.translateX(-Global.ROLE_SPEED_X);
        }
        // Jimmy從右撞(會交換)
        else if (colliderJimmy.right() > colliderAmy.left() //jimmy在amy右邊
                && colliderJimmy.left() < colliderAmy.right()
                && colliderJimmy.bottom() == colliderAmy.bottom()
                && jimmy.isCollision(amy) //碰撞
        ) {
            jimmy.translateX(colliderAmy.right() - colliderJimmy.left());
            amy.translateX(-Global.ROLE_SPEED_X);
        }
        // Jimmy從左撞(會交換)
        else if (colliderJimmy.left() < colliderAmy.left() //jimmy在amy左邊
                && colliderJimmy.right() > colliderAmy.left()
                && colliderJimmy.bottom() == colliderAmy.bottom()//y軸速度
                && jimmy.isCollision(amy) //碰撞
        ) {
            jimmy.translateX(-(colliderJimmy.right() - colliderAmy.left()));
            amy.translateX(Global.ROLE_SPEED_X);
        }
    }

    /* 碰撞重生點ok */
    private void checkRebornPoint(Character character) {
        for (RebornPoint rebornPoint : rebornPoints) {
            //如果沒有rebornPT我就存第一個(已經排序)
            if (lastRebornPoint != null && rebornPoint.collider().left() <= lastRebornPoint.collider().left()) {
                continue;
            }
            //我現在位置比其他的rp大我就做紀錄
            if (character.collider().left() > rebornPoint.collider().left()) {
                lastRebornPoint = rebornPoint;
                lastRebornPoint.rebornRecord(character);
                break;
            }
        }
    }

    /* Dead */
    private boolean touchEnemy(Character character, ArrayList<Enemy> enemies) {
        if (character.animationState() == Character.AnimationState.DEAD) {
            boolean tmp = delayForDead.count();
            if (tmp) {
                lastRebornPoint.giveBackState(actor, jimmy);
                actor.setAnimationState(Character.AnimationState.STAND);
                jimmy.setAnimationState(Character.AnimationState.STAND);
                return true;
            } else {
                return false;
            }
        }
        for (int i = 0; i < enemies.size(); i++) {
            Enemy enemy = enemies.get(i);

            if (character.isCollision(enemy)) {
                if (lastRebornPoint != null && character.animationState() != Character.AnimationState.INVINCIBLE) {
                    if (!Global.IS_DEBUG) {
                        AudioResourceController.getInstance().play(new Path().sounds().dead());
                        allPoint -=100;
                        actor.setAnimationState(Character.AnimationState.DEAD);
                        jimmy.setAnimationState(Character.AnimationState.DEAD);
                        delayForDead.play();
                        return true;
                    }
                }
            }

            if (character.isCollision(enemy.clone())) {
                AudioResourceController.getInstance().shot(new Path().sounds().beatEnemy());
                allPoint +=15;
                enemies.remove(enemy);
                break;
            }

        }
        return false;
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
            if (pause) {
                for (Button button : buttons) {
                    MouseTriggerImpl.mouseTrig(button, e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            /* 傳入相對應的按鍵code給Character執行 */
            @Override
            public void keyPressed(int commandCode, long trigTime) {
                if (!pause && !isVictory) {
                    actor.changeDirAndSelectAnimation(commandCode);
                    jimmy.changeDirAndSelectAnimation(commandCode);
                }
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {
                if (!pause && !isVictory) {
                    actor.changeDirAndSelectAnimation(Global.NONE);
                    jimmy.changeDirAndSelectAnimation(Global.P2_NONE);
                }


                if (commandCode == Global.ESC) {
                    pause = !pause;
                }
            }
            @Override
            public void keyTyped(char c, long trigTime) {

            }
        };
    }
}
