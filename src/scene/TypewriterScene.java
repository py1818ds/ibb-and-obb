package scene;

import controllers.SceneController;
import menu.BackgroundType.BackgroundImage;
import menu.Style;
import menu.Button;
import utils.CommandSolver;
import utils.Path;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static utils.Global.SCREEN_Y;

public class TypewriterScene extends Scene {

    private Image icon;
    private Image jimmy;
    private Image amy;

    private String str = "";
    private int strLength = str.length();
    private int xCharacter = 0;
    private BufferedReader br;
    private boolean changeScene;
    
    private ArrayList<Button> buttons;


    @Override
    public void sceneBegin() {
        icon = SceneController.instance().irc().tryGetImage(new Path().img().menu().classroom()); 
        jimmy = SceneController.instance().irc().tryGetImage(new Path().img().menu().jimmy());
        amy = SceneController.instance().irc().tryGetImage(new Path().img().menu().amy());

        buttons = new ArrayList<>();
        buttons.add(new Button(50, 400, new Style.StyleRect(1170, 160, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().dialogue())))));
        try {
            br = new BufferedReader(new FileReader("src/rank/Dialogue.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        changeScene = false;
        run();

    }

    @Override
    public void sceneEnd() {
    }

    public void run(){
        new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    while(br.ready()){ 
                        str += br.readLine();
                        xCharacter = 0;
                        while(xCharacter <= str.length()){
                            try{
                                Thread.sleep(100);
                                xCharacter++;
                            }
                            catch(InterruptedException e){
                                e.printStackTrace();
                            }
                            if(xCharacter == str.length()){
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        str = "";
                    }
                    changeScene = true;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    br.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }).start();
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(icon, 0, 0, 1280, 600, null);
        g.drawImage(jimmy, 200, SCREEN_Y / 2 - 150, 300, 500,null);
        g.drawImage(amy, 700, SCREEN_Y / 2 -150, null);

        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
        g.setFont(new Font("Zpix", Font.PLAIN, 25));
        g.setColor(Color.black);

        if(!changeScene){
            g.drawString(str.substring(0,xCharacter), 70, 450);
        }
    }

    @Override
    public void update() {
        if(changeScene){
            SceneController.instance().changeScene(new MenuScene());
        }
       
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;

    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

