package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import menu.BackgroundType.BackgroundImage;
import menu.Button;
import menu.CustomFont;
import menu.*;
import menu.Label;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static utils.Global.SCREEN_X;
import static utils.Global.SCREEN_Y;

public class TitleScene extends Scene {
    private Image img;
    private ArrayList<Button> buttons;
    private CustomFont cf;

    @Override
    public void sceneBegin() {
        AudioResourceController.getInstance().loop(new Path().sounds().menuMusicTwo(),5);
        cf = new CustomFont();
        // img = new ImageIcon(getClass().getResource("/resources/imgs/menu/titleBackground.gif")).getImage();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().titleBackground());
      
        buttons = new ArrayList<Button>();
        buttons.add(new Button(280, 110, new Style.StyleRect(720, 350, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().titleButton())))));
        buttons.add(new Button(320, 170, new Style.StyleRect(610, 300, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().logoText())))));
        buttons.add(new Button(310 , 130, new Style.StyleRect(80, 80, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().amyGhost())))));
        buttons.add(new Button(SCREEN_X / 2 + 270 , SCREEN_Y / 2 + 30, new Style.StyleRect(80, 80, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().jimmyGhost())))));
        buttons.add(new Button(SCREEN_X / 2 - 50, SCREEN_Y / 2 + 190, new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
            .setText("點擊開始")
            .setTextFont(new Font("Zpix", Font.BOLD, 45))
            .setTextColor(Color.white)));

        buttons.get(4).setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
            .setText("進入遊戲")
            .setTextFont(new Font("Zpix", Font.BOLD, 40))
            .setTextColor(Color.white));
        buttons.get(2).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicTwo());
                SceneController.instance().changeScene(new MenuScene());
            }
        });
        buttons.get(3).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicTwo());
                SceneController.instance().changeScene(new MenuScene());
            }
        });
        buttons.get(4).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicTwo());
                SceneController.instance().changeScene(new MenuScene());
            }
        });
        
    }

    @Override
    public void sceneEnd() {
        img = null;
        buttons = null;
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 720,null);

        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
    }

    @Override
    public void update() {  


    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                for(int i = 0; i < buttons.size(); i++){
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
        // return null;

    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

