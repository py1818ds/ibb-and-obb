package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import menu.BackgroundType.BackgroundImage;
import menu.Button;
import menu.CustomFont;
import menu.*;
import menu.Label;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static utils.Global.SCREEN_X;

public class MenuScene extends Scene {
    private Image img;
    private ArrayList<Button> buttons;
    private CustomFont cf;


    @Override
    public void sceneBegin() {
        AudioResourceController.getInstance().loop(new Path().sounds().menuMusicOne(),-1);

        cf = new CustomFont();
        buttons = new ArrayList<Button>();
        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().titleBackground());

        buttons.add(new Button(SCREEN_X / 2 - 150, 80, new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("單機模式")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25)) //Zpix SC
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 190, new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("連線模式")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 300, new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("教學模式")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 410, new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("排行榜")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE)));
        buttons.add(new Button(SCREEN_X / 2 - 150, 520, new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("離開遊戲")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25))
                .setTextColor(Color.WHITE)));
        
        buttons.get(0).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Local Mode")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(1).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Connection Mode")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(2).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Tutorial Mode")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(3).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
            .setText("Ranking")
            .setTextFont(new Font("Zpix", Font.PLAIN, 25))
            .setTextColor(Color.WHITE));
        buttons.get(4).setStyleHover(new Style.StyleRect(340, 73, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().button())))
                .setText("Exit")
                .setTextFont(new Font("Zpix", Font.PLAIN, 25))
                .setTextColor(Color.WHITE));
        
        buttons.get(0).setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new PlayModeScene())
        );
        buttons.get(1).setClickedActionPerformed((int x, int y) -> 
            SceneController.instance().changeScene(new ConnectModeScene())
        );
        buttons.get(2).setClickedActionPerformed(new Label.ClickedAction() {
            @Override
            public void clickedActionPerformed(int x, int y) {
                AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
                SceneController.instance().changeScene(new TutorialScene());
            }
        });
        buttons.get(3).setClickedActionPerformed(( x,  y) -> {
            AudioResourceController.getInstance().stop(new Path().sounds().menuMusicOne());
            SceneController.instance().changeScene(new RankingScene());
                }
        );
        buttons.get(4).setClickedActionPerformed((int x, int y) ->
                System.exit(0)
        );

    }

    @Override
    public void sceneEnd() {
        img = null;
        buttons = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 720,null);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }

    }

    @Override
    public void update() {
       
    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                for(int i = 0; i < buttons.size(); i++){
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };

    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

