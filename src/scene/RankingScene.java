package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import menu.BackgroundType.BackgroundImage;
import menu.CustomFont;
import menu.Style;
import menu.Button;
import menu.impl.MouseTriggerImpl;
import utils.CommandSolver;
import utils.CommandSolver.MouseState;
import utils.Path;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static utils.Global.SCREEN_X;
import static utils.Global.SCREEN_Y;

public class RankingScene extends Scene {

    private Image img;
    private Image amy;
    private CustomFont cf;
    private BufferedReader br;
    private ArrayList<Button> buttons;

    private ArrayList<String> co_opName;
    private ArrayList<String> co_opScore;

    private ArrayList<String> pkName;
    private ArrayList<String> pkScore;

    @Override
    public void sceneBegin() {

        AudioResourceController.getInstance().play(new Path().sounds().overScene());
        cf = new CustomFont();
        co_opName = new ArrayList<>();
        co_opScore = new ArrayList<>();
        pkName = new ArrayList<>();
        pkScore = new ArrayList<>();
        readCo_OpRanking();
        readPKRanking();
        buttons = new ArrayList<Button>();

        img = SceneController.instance().irc().tryGetImage(new Path().img().menu().darkBackground());
        amy = SceneController.instance().irc().tryGetImage(new Path().img().menu().amyGhost());

        // buttons.add(new Button(100, 30, new Style.StyleRect(500, 500, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))));
        buttons.add(new Button(340, 90, new Style.StyleRect(50, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
                .setText("Co-Op Mode Ranking List")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30)) //Nagurigaki Crayon
                .setTextColor(Color.WHITE)));
        // buttons.add(new Button(SCREEN_X / 2, 30, new Style.StyleRect(500, 500, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().transparentRect())))));
        buttons.add(new Button(SCREEN_X / 2 + 290, 90, new Style.StyleRect(50, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
                .setText("PK Mode Ranking List")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.WHITE)));
        buttons.add(new Button(80, SCREEN_Y - 60, new Style.StyleRect(50, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().amyGhost())))));
        buttons.add(new Button(SCREEN_X - 110, SCREEN_Y - 70, new Style.StyleRect(50, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().jimmyGhost())))));
        buttons.add(new Button(50, 50, new Style.StyleRect(30, 30, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
                .setText("X")
                .setTextFont(new Font("Zpix", Font.BOLD, 40))
                .setTextColor(Color.white)));

        buttons.get(2).setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().back())))
                .setText("Back")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.black));
        buttons.get(3).setStyleHover(new Style.StyleRect(100, 50, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().back())))
                .setText("Back")
                .setTextFont(new Font("Zpix", Font.PLAIN, 30))
                .setTextColor(Color.black));
        buttons.get(4).setStyleHover(new Style.StyleRect(30, 30, true, new BackgroundImage(SceneController.instance().irc().tryGetImage(new Path().img().menu().blankButton())))
                .setText("X")
                .setTextFont(new Font("Zpix", Font.PLAIN, 40))
                .setTextColor(Color.pink));

        buttons.get(2).setClickedActionPerformed((x, y) -> {
                    AudioResourceController.getInstance().stop(new Path().sounds().overScene());
                    SceneController.instance().changeScene(new MenuScene());
                }
        );
        buttons.get(3).setClickedActionPerformed((x, y) -> {
                    AudioResourceController.getInstance().stop(new Path().sounds().overScene());
                    SceneController.instance().changeScene(new MenuScene());
                }
        );
        buttons.get(4).setClickedActionPerformed((x, y) -> {
                    AudioResourceController.getInstance().stop(new Path().sounds().overScene());
                    SceneController.instance().changeScene(new MenuScene());
                }
        );


    }

    public void readCo_OpRanking() {
        ArrayList<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader("src/rank/Co-opList.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (br.ready()) {
                list.add(br.readLine());
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < list.size(); i++) {
            String[] parse = (list.get(i).split(","));
            co_opName.add(parse[0]);
            co_opScore.add(parse[1]);
        }
    }

    public void readPKRanking() {
        ArrayList<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader("src/rank/PKList.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (br.ready()) {
                list.add(br.readLine());
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < list.size(); i++) {
            String[] parse = (list.get(i).split(","));
            pkName.add(parse[0]);
            pkScore.add(parse[1]);
        }
    }

    @Override
    public void sceneEnd() {
        img = null;
        buttons = null;
    }

    //這邊誰先畫,誰在下面
    @Override
    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 1280, 600, null);
        g.setColor(Color.white);
        g.drawRect(100, 80, 500, 550);
        g.drawRect(SCREEN_X / 2 + 50, 80, 500, 550);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).paint(g);
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("Zpix", Font.PLAIN, 30));
        for (int i = 0; i < co_opName.size(); i++) {
            switch (i) {

                case 0:
                    g.drawString("No.1.", 150, 210);
                    g.drawString(co_opName.get(i), 250, 210);
                    g.drawString(co_opScore.get(i), 500, 210);
                    break;
                case 1:
                    g.drawString("No.2.", 150, 310);
                    g.drawString(co_opName.get(i), 250, 310);
                    g.drawString(co_opScore.get(i), 500, 310);
                    break;
                case 2:
                    g.drawString("No.3.", 150, 410);
                    g.drawString(co_opName.get(i), 250, 410);
                    g.drawString(co_opScore.get(i), 500, 410);
                    break;
                case 3:
                    g.drawString("No.4.", 150, 510);
                    g.drawString(co_opName.get(i), 250, 510);
                    g.drawString(co_opScore.get(i), 500, 510);
                    break;
                case 4:
                    g.drawString("No.5.", 150, 610);
                    g.drawString(co_opName.get(i), 250, 610);
                    g.drawString(co_opScore.get(i), 500, 610);
                    break;
            }
        }

        for (int i = 0; i < pkName.size(); i++) {
            switch (i) {

                case 0:
                    g.drawString("No.1.", SCREEN_X / 2 + 100, 210);
                    g.drawString(pkName.get(i), SCREEN_X / 2 + 200, 210);
                    g.drawString(pkScore.get(i), SCREEN_X / 2 + 470, 210);
                    break;
                case 1:
                    g.drawString("No.2.", SCREEN_X / 2 + 100, 310);
                    g.drawString(pkName.get(i), SCREEN_X / 2 + 200, 310);
                    g.drawString(pkScore.get(i), SCREEN_X / 2 + 470, 310);
                    break;
                case 2:
                    g.drawString("No.3.", SCREEN_X / 2 + 100, 410);
                    g.drawString(pkName.get(i), SCREEN_X / 2 + 200, 410);
                    g.drawString(pkScore.get(i), SCREEN_X / 2 + 470, 410);
                    break;
                case 3:
                    g.drawString("No.4.", SCREEN_X / 2 + 100, 510);
                    g.drawString(pkName.get(i), SCREEN_X / 2 + 200, 510);
                    g.drawString(pkScore.get(i), SCREEN_X / 2 + 470, 510);
                    break;
                case 4:
                    g.drawString("No.5.", SCREEN_X / 2 + 100, 610);
                    g.drawString(pkName.get(i), SCREEN_X / 2 + 200, 610);
                    g.drawString(pkScore.get(i), SCREEN_X / 2 + 470, 610);
                    break;
            }
        }
    }

    @Override
    public void update() {

    }

    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return new CommandSolver.MouseCommandListener() {
            @Override
            public void mouseTrig(MouseEvent e, MouseState state, long trigTime) {
                for (int i = 0; i < buttons.size(); i++) {
                    MouseTriggerImpl.mouseTrig(buttons.get(i), e, state);
                }
            }
        };
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return null;
    }


}

