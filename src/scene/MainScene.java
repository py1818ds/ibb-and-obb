//package Scene;
//
//import Controllers.MusicResourceController;
//import Controllers.SceneController;
//import GameObeject.*;
//import company.CommandSolver;
//import company.Global;
//import company.Path;
//import util.Map;
//
//import javax.swing.*;
//import java.applet.Applet;
//import java.applet.AudioClip;
//import java.awt.*;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//
//public class MainScene extends Scene {
//    private AirCraft ac;
//    private ArrayList<CEnemy> cEnemies;
//    private ArrayList<CBoom> ammo;
//    private Actor1 actor1;
//    private AudioClip bgm;
//    private Brick brick;
//    private ArrayList<Brick> block;
//    public int[][] map;
//    {
//        Map mp = new Map();
//        try {
//            map = mp.readMap();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Override
//    public void sceneBegin() {
//        ac = new AirCraft(10, 400);
//        cEnemies = new ArrayList<>();
//        ammo = new ArrayList<>();
//        actor1 = new Actor1(Global.SCREEN_X / 2, Global.SCREEN_Y / 2, 2);
//        if (bgm == null) {
//            bgm = SceneController.instance().mrc().tryGetMusic(new Path().sounds().alarm());
//        }
//        bgm.loop();
//        block = new ArrayList<>();
//        for (int i = 0; i < map.length; i++) {
//            System.out.println("map.length:" + map.length + " map[0].length:" + map[0].length);
//            for (int j = 0; j < map[0].length; j++) {
//                if (map[i][j] == 1) {
//                    System.out.println("畫磚塊 x:"+j * 30+" y:"+i * 30);
//                    brick = new Brick(j * 30, i * 30);
//                    block.add(brick);
//                }
//            }
//        }
//        //brick = new Brick(100,100);
//
//    }
//
//    @Override
//    public void sceneEnd() {
//
//    }
//
//    //這邊誰先畫,誰在下面
//    @Override
//    public void paint(Graphics g) {
//
//        ac.paint(g);
//        for (CEnemy c : cEnemies) {
//            c.paint(g);
//        }
//        for (CBoom bullet : ammo) {
//            bullet.paint(g);
//        }
//        for (Brick br: block
//             ) {
//            br.paint(g);
//        }
//        actor1.paint(g);
//        //brick.paint(g);
//    }
//
//    @Override
//    public void update() {
//
//        //隨機產生
//        if (Global.random(0, 4) == 0 && cEnemies.size() < 8) {
//            cEnemies.add(new CEnemy(Global.random(0, 600), 0, Global.random(-5, 5)));
//        }
//        ac.update();
//        for (int i = 0; i < cEnemies.size(); i++) {
//
//            if (cEnemies.get(i).outOfScreen()) {
//              //  System.out.println("out:" + cEnemies.size());
//                cEnemies.remove(i--);
//                continue;
//            }
//            if (ac.isCollision(cEnemies.get(i))) {
//            //    System.out.println("碰撞" + cEnemies.size());
//                cEnemies.remove(i--);
//             //   SceneController.instance().changeScene(new GameOverScene(this));
//                continue;
//            }
//            cEnemies.get(i).update();
//
//        }
//
//        for (int i = 0; i < ammo.size(); i++) {
//            ammo.get(i).update();
//            CBoom bullet = ammo.get(i);
//            if (bullet.getState() != CBoom.State.NORMAL) {
//                if (bullet.getState() == CBoom.State.REMOVED) {
//                    ammo.remove(i--);
//                }
//                continue;
//            }
//            if (bullet.touchTop()) {
//                ammo.remove(i--);
//                continue;
//            }
//            for (int j = 0; j < cEnemies.size(); j++) {
//                if (bullet.isCollision(cEnemies.get(j))) {
//                    bullet.setState(CBoom.State.BOOM);
//                    cEnemies.remove(j);
//                    break;
//                }
//            }
//        }
//        actor1.update();
//    }
//
//
//    @Override
//    public CommandSolver.MouseCommandListener mouseListener() {
//        return (MouseEvent e, CommandSolver.MouseState state, long trigTime) -> {
//            ac.mouseTrig(e, state, trigTime);
//            if (state == CommandSolver.MouseState.PRESSED) {
//                ammo.add(new CBoom(ac.painter().left(), ac.painter().top(), 20, 20));
//            }
//        };
//
//    }
//
//    @Override
//    public CommandSolver.KeyListener keyListener() {
//        return new CommandSolver.KeyListener() {
//            @Override
//            public void keyPressed(int commandCode, long trigTime) {
//                actor1.changeDir(commandCode);
//                ac.changeDir(commandCode);
//                if (commandCode == 5) {
//                    ammo.add(new CBoom(ac.painter().left(), ac.painter().top(), 20, 20));
//                }
//            }
//
//            @Override
//            public void keyReleased(int commandCode, long trigTime) {
//                actor1.changeDir(4);
//                actor1.jump(commandCode);
//                ac.stop();
//            }
//
//            @Override
//            public void keyTyped(char c, long trigTime) {
//
//            }
//        };
//    }
//
//
//}
