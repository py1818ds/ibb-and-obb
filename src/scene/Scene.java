package scene;

import utils.CommandSolver.*;

import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Scene {

    private boolean isPause;
    long startTime ; //取得目前系統時間的奈秒
    long currentTime; // 系統當前時間
    long endTime;
    int allPoint;
    String name;
    String p1Name;
    String p2Name;

    public Scene() {
        isPause = false;
    }

    public abstract void sceneBegin();

    public abstract void sceneEnd();

    public abstract void paint(Graphics g);

    public abstract void update();

    public abstract MouseCommandListener mouseListener();

    public abstract KeyListener keyListener();


    // 寫檔
    public void writeFile(ArrayList<String> list, String filePath){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(filePath));
            for(int i = 0; i < list.size(); i++){
                if(i == 5){
                    break;
                }

                bw.write(list.get(i));
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 合作
    public ArrayList<String> readCoFile(String filePath, String name, int score){
        ArrayList<String> newList = new ArrayList<>();
        try {
            ArrayList<String> list = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while(br.ready()){
                list.add(br.readLine());
            }
            br.close();
            list.add(name + "," + score);
            newList = sort(list);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newList;
    }
     // 競賽
     public ArrayList<String> readPkFile(String filePath, String p1Name, int p1Score, String p2Name, int p2Score){
        ArrayList<String> newList = new ArrayList<>();
        try {
            ArrayList<String> list = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while(br.ready()){
                list.add(br.readLine());
            }
            br.close();
            list.add(p1Name + "," + p1Score);
            list.add(p2Name + "," + p2Score);

            newList = sort(list);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newList;
    }
    // 排序
    public ArrayList<String> sort(ArrayList<String> list){
        ArrayList<String> newList = new ArrayList<>();
        String[][] sortList = new String[list.size()][2];
        for(int i = 0; i < list.size(); i++){
            String[] parsedData = list.get(i).split(",");
            sortList[i][0] = parsedData[0];
            sortList[i][1] = parsedData[1];
        }
        //sort
        for(int i = 0; i < list.size() - 1; i++){
            for(int j = 0; j < list.size() - 1 - i; j++){
                int a = Integer.parseInt(sortList[j][1]);
                int b = Integer.parseInt(sortList[j+1][1]);
                if( a < b){
                    String tmpName = sortList[j][0];
                    String tmpScore = sortList[j][1];
                    sortList[j][0] = sortList[j+1][0];
                    sortList[j][1] = sortList[j+1][1];
                    sortList[j+1][0] = tmpName;
                    sortList[j+1][1] = tmpScore;
                }
            }
        }
        for(int i = 0; i < sortList.length; i++){
            newList.add(sortList[i][0]+","+ sortList[i][1]);
        }
        return newList;
    }


//    public abstract void paintPlay(Graphics g);
//
//    public abstract void paintPause(Graphics g);

//    public void sceneUpdate() {
//        if (!isPause) {
//            update();
//        }
//    }
//
//    public void pause() {
//        isPause = true;
//    }
//
//    public void play() {
//        isPause = false;
//    }
}
