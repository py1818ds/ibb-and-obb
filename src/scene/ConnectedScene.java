
package scene;

import controllers.AudioResourceController;
import controllers.SceneController;
import game_object.GravityState;
import game_object.Rect;
import game_object.item_object.*;
import game_object.map_object.*;
import game_object.map_object.FloorNormal;
import game_object.characters.Character;
import internet.Client.ClientClass;
import utils.CommandSolver;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import utils.Delay;
import utils.Global;

import static utils.Global.*;

import maploader.*;
import scene_process.Camera;
import utils.Path;

import javax.imageio.ImageIO;


public class ConnectedScene extends Scene {

    private ArrayList<FloorNormal> floors;
    private ArrayList<CrossingLineX> crossingXLines;
    private ArrayList<CrossingLineY> crossingYLines;
    private ArrayList<VictoryPoint> victoryPoints;


    // private ArrayList<Enemy> enemies;
    private Character self;
    private Character player;
    private ArrayList<Character> characters;

    // private VictoryPoint victoryPoint;
    private Camera camera;
    private Image background;
    private Image back;
    // private Image stan;
    private ArrayList<Character> players;

    private Delay delayForVictory;
    private Delay delayForDead;//角色死掉for場景
    private Delay delayUpdateFrame;
    private boolean isSelfWinner;

    private int commandForSend;
    private boolean canSendJump;
    private String winner;
    //
    private ArrayList<GravityBall> gravityBalls;
    private ArrayList<Item> items;
    //

    @Override
    public void sceneBegin() {
        AudioResourceController.getInstance().loop(new Path().sounds().teachLevelMusic(),-1);
        players = SceneTool.instance().getPlayers();
        self = SceneTool.instance().getSelf();
        player = SceneTool.instance().getPlayers().get(1);
        floors = new ArrayList<>();
        victoryPoints = new ArrayList<>();
        crossingXLines = new ArrayList<>();
        crossingYLines = new ArrayList<>();
        gravityBalls = new ArrayList<>();
        items = new ArrayList<>();
        winner = "";
        delayForVictory = new Delay(180);
        camera = new Camera(23500, 3600);
        isSelfWinner = true;

        try {
            // stan = ImageIO.read(Objects.requireNonNull(getClass().getResource("/resources/imgs/actors/stan.png")));
            background = ImageIO.read(Objects.requireNonNull(getClass().getResource("/resources/imgs/backgrounds/background-dark.png")));
            back = ImageIO.read(Objects.requireNonNull(getClass().getResource("/resources/imgs/backgrounds/back.png")));
        } catch (Exception ignored) {

        }
        self.setPartner(player);
        player.setPartner(self);
        camera.setTarget(self);
        camera.setChaseX(8);
        camera.setChaseY(8);
        mapInitialize();
        delayForDead = new Delay(90);
        delayUpdateFrame = new Delay(30);
        delayUpdateFrame.loop();

    }

    @Override
    public void sceneEnd() {

    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g.setColor(new Color(17, 17, 17));
        g.fillRect(0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT);
        camera.startCamera(g2d);
        g.drawImage(background, 0, -400, 3840, 2160, null);
        g.drawImage(background, 3800, -400, 3840, 2160, null);
        // g.drawImage(background,0,1560,3840,2160,Color.BLACK,null);
        // g.drawImage(stan, 500, 770, 128, 192, null);

        for (GravityBall gravityBall : gravityBalls) {
            gravityBall.paint(g);
        }

        for (FloorNormal floor : floors) {
            floor.paint(g);
        }
        for (CrossingLineX c : crossingXLines) {
            c.paint(g);
        }

        for (CrossingLineY cLR : crossingYLines) {
            cLR.paint(g);
        }

        for (VictoryPoint v : victoryPoints) {
            v.paint(g);
        }

        for (Character player : players) {
            player.paint(g);
        }

        camera.endCamera(g2d);
//        Graphics2D g2d = (Graphics2D) g;
//        g2d.scale(1,1);
        g2d.drawImage(back, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, null);
    }

    @Override
    public void update() {
        // background.update();
        //優先順序
        //角色>地板碰撞 //因為地板碰撞會更改Y速度，會跳不起來

//        if (self.checkPosition()) {
//            ClientClass.getInstance().sent(CHECK_POSITION, bale(String.valueOf(self.animationState().animationX())));
//        }
        player.checkPosition();


        for (Character player : players) {
            player.update();
        }

        //重力球
        self.hitGravityBalls(gravityBalls);
        player.hitGravityBalls(gravityBalls);

        //  地板碰撞
        self.hitFloors(floors);
        player.hitFloors(floors);

        //角色碰撞
        standOnEachOther(self, player);
        pushEachOther(self, player);

        self.hitCrossingFloors(crossingXLines, crossingYLines);
        player.hitCrossingFloors(crossingXLines, crossingYLines);

        // 移動封包
//        if(canSendJump){
//            ClientClass.getInstance().sent(MOVE, bale("3"));
//            self.changeDirAndSelectAnimation(3);
//            canSendJump = false;
//        }

//        self.changeDirAndSelectAnimation(commandForSend);
//        ClientClass.getInstance().sent(MOVE, bale(Integer.toString(commandForSend)));
//        commandForSend = 0;


        for (CrossingLineX tmpCrossingX : crossingXLines) {
            tmpCrossingX.update();
        }
        for (CrossingLineY tmpCrossingY : crossingYLines) {
            tmpCrossingY.update();
        }
        for (GravityBall gBall : gravityBalls) {
            gBall.update();
        }

        for(VictoryPoint v : victoryPoints){
            if (self.isCollision(v)) {
                if(winner.isEmpty()){
                    delayForVictory.play();
                    winner = self.getName();
                    self.setAnimationState(Character.AnimationState.VICTORY);
                    AudioResourceController.getInstance().stop(new Path().sounds().teachLevelMusic());
                    AudioResourceController.getInstance().play(new Path().sounds().victory());
                }
            } else if (player.isCollision(v)) {
                if(winner.isEmpty()){
                    delayForVictory.play();
                    isSelfWinner = false;
                    player.setAnimationState(Character.AnimationState.VICTORY);
                    AudioResourceController.getInstance().stop(new Path().sounds().teachLevelMusic());
                    AudioResourceController.getInstance().play(new Path().sounds().victory());
                }
            }
        }

        if (delayForVictory.count()) {
            SceneController.instance().changeScene(new ConnectedSceneEnd(isSelfWinner ? self.getName() : player.getName()));
        }
        //move的封包

        ClientClass.getInstance().sent(UPDATE_FRAME,
                bale(String.valueOf(self.collider().left()),
                        String.valueOf(self.collider().top()),
                        String.valueOf(self.animationState().animationX()),
                        String.valueOf(self.gravityState().value())));
        /* 鏡頭更新 */
        camera.update();
        SceneTool.instance().consume();
    }

    private void mapInitialize() {//建構地圖
        try {
            ArrayList<MapInfo> mapInfo = new MapLoader("genMap_levelOne.bmp", "genMap.txt").combineInfo();
//            ArrayList<MapInfo> mapInfo = new MapLoader("genMap_Connect.bmp", "genMap.txt").combineInfo();
            for (MapInfo tmp : mapInfo) {  // 地圖產生器內物件的 {左, 上, 寬, 高}
                switch (tmp.getName()) {
                    case "floor_up": //  需中心座標
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.UP, 0));
                        break;
                    case "floor_down":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.DOWN, 0));
                        break;
                    case "floor_left":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT, 0));
                        break;
                    case "floor_right":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT, 0));
                        break;
                    case "floor_left_up_convex":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_UP_CONVEX, 0));
                        break;
                    case "floor_right_up_convex":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_UP_CONVEX, 0));
                        break;
                    case "floor_left_down_convex":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_DOWN_CONVEX, 0));
                        break;
                    case "floor_right_down_convex":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_DOWN_CONVEX, 0));
                        break;
                    case "floor_left_up_concave":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_UP_CONCAVE, 0));
                        break;
                    case "floor_right_up_concave":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_UP_CONCAVE, 0));
                        break;
                    case "floor_left_down_concave":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.LEFT_DOWN_CONCAVE, 0));
                        break;
                    case "floor_right_down_concave":
                        floors.add(new FloorNormal(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, FloorDirection.RIGHT_DOWN_CONCAVE, 0));
                        break;
                    case "cross-3x2":
                        crossingXLines.add(new CrossingLineX(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 36, 32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                        break;
                    case "cross2x3":
                        crossingYLines.add(new CrossingLineY(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 48, 32 * tmp.getSizeY(), 32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                        break;
                    case "coin":
                        items.add(new Coin(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                        break;

                    case "gravity_ball":
                        gravityBalls.add(new GravityBall(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                        break;

                    case "axe":
                        items.add(new Axe(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                        break;
                    case "bell":
                        items.add(new BellMagnet(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                        break;
                    case "flag":
                        victoryPoints.add( new VictoryPoint(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32 * tmp.getSizeX(), 32 * tmp.getSizeY()));
                        break;
                    case "speedDrink":
                        items.add(new FruitCanSpeedUp(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                        break;

                    case "star":
                        items.add(new Star(32 * tmp.getX() + 32 * tmp.getSizeX() / 2,
                                32 * tmp.getY() + 32 * tmp.getSizeY() / 2, 32, 32));
                        break;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TutorialScene.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*互踩function*/
    private void standOnEachOther(Character p1, Character p2) {
        if (p1.gravityState() != p2.gravityState()) {
            return;
        }
        if(p1.isTouchCrossingLine() || p2.isTouchCrossingLine()){
            return;
        }
        Rect colliderP1 = p1.collider();
        Rect colliderP2 = p2.collider();
        //判斷是否碰撞
        //相切算碰撞
        if (colliderP1.right() <= colliderP2.left()) {
            return;
        }
        if (colliderP1.left() >= colliderP2.right()) {
            return;
        }
        if (colliderP1.top() > colliderP2.bottom()) {
            return;
        }
        if (colliderP1.bottom() < colliderP2.top()) {
            return;
        }


        if (p1.gravityState() == GravityState.NORMAL_GRAVITY) {//如果這一幀是從上面穿越碰撞//actor比Jimmy高(x較小)
            if ((colliderP1.bottom() > colliderP2.top()
                    && colliderP1.bottom() < colliderP2.bottom())) {//
                p1.translateY(-(colliderP1.bottom() - colliderP2.top()));
                p1.setSpeedY(0);
                p1.setCanFalling(false);
                p1.setCanJump(true);
            }
            else if (colliderP1.top() < colliderP2.bottom()
                    && colliderP1.bottom() > colliderP2.bottom()) {//如果這一幀是從上面穿越碰撞//actor比Jimmy低(x較大)
                p2.translateY(-(colliderP2.bottom() - colliderP1.top()));
                p2.setSpeedY(0);
                p2.setCanFalling(false);
                p2.setCanJump(true);
            }
        } else {//如果下一幀是從上面穿越碰撞//amy比jimmy高(x較小)
            if (colliderP1.top() < colliderP2.bottom()
                    && colliderP1.top() > colliderP2.top()) {
                p1.translateY(-(colliderP1.top() - colliderP2.bottom()));
                p1.setSpeedY(0);
                p1.setCanFalling(false);
                p1.setCanJump(true);
            } else if (colliderP1.bottom() > colliderP2.top()
                    && colliderP1.top() < colliderP2.top()) {
                p2.translateY(-(colliderP2.top() - colliderP1.bottom()));
                p2.setSpeedY(0);//改1
                p2.setCanFalling(false);
                p2.setCanJump(true);
            }
        }


    }

    /*互推function*/
    private void pushEachOther(Character amy, Character jimmy) {
        if(amy.gravityState() != jimmy.gravityState()){
            return;
        }
        if(amy.isTouchCrossingLine() || jimmy.isTouchCrossingLine()){
            return;
        }
//        if(amy.crossingFrom()!= Character.CrossingState.None || jimmy.crossingFrom()!= Character.CrossingState.None){
//            return;
//        }
        if(!amy.isCollision(jimmy)){
            return;
        }
        Rect colliderAmy = amy.collider();
        Rect colliderJimmy = jimmy.collider();


        //amy上跳時//amy還沒越過jimmy 所以可以推
        if (colliderAmy.left() < colliderJimmy.left() && colliderAmy.right() > colliderJimmy.left()
                && amy.speedX() >= 0
                && colliderAmy.bottom() == colliderJimmy.bottom()//Y軸判斷
                && amy.isCollision(jimmy) //碰撞(包含相切)
        ) {
            amy.translateX(-(colliderAmy.right() - colliderJimmy.left()));
            jimmy.translateX(Global.ROLE_SPEED_X);
        }
        // Amy從右撞
        else if (colliderAmy.right() > colliderJimmy.right() //amy在jimmy右邊
                && colliderAmy.left() < colliderJimmy.right()
                && colliderAmy.bottom() == colliderJimmy.bottom()
                && amy.isCollision(jimmy) //碰撞
        ) {
            amy.translateX(colliderJimmy.right() - colliderAmy.left());
            jimmy.translateX(-Global.ROLE_SPEED_X);
        }
        // Jimmy從右撞(會交換)
        else if (colliderJimmy.right() > colliderAmy.left() //jimmy在amy右邊
                && colliderJimmy.left() < colliderAmy.right()
                && colliderJimmy.bottom() == colliderAmy.bottom()
                && jimmy.isCollision(amy) //碰撞
        ) {
            jimmy.translateX(colliderAmy.right() - colliderJimmy.left());
            amy.translateX(-Global.ROLE_SPEED_X);
        }
        // Jimmy從左撞(會交換)
        else if (colliderJimmy.left() < colliderAmy.left() //jimmy在amy左邊
                && colliderJimmy.right() > colliderAmy.left()
                && colliderJimmy.bottom() == colliderAmy.bottom()//y軸速度
                && jimmy.isCollision(amy) //碰撞
        ) {
            jimmy.translateX(-(colliderJimmy.right() - colliderAmy.left()));
            amy.translateX(Global.ROLE_SPEED_X);
        }
    }


    @Override
    public CommandSolver.MouseCommandListener mouseListener() {
        return null;
    }

    @Override
    public CommandSolver.KeyListener keyListener() {
        return new CommandSolver.KeyListener() {
            /* 傳入相對應的按鍵code給Character執行 */
            @Override
            public void keyPressed(int commandCode, long trigTime) {
                self.changeDirAndSelectAnimation(commandCode);
            }

            @Override
            public void keyReleased(int commandCode, long trigTime) {
                self.changeDirAndSelectAnimation(Global.NONE);
                commandForSend = NONE;
            }

            @Override
            public void keyTyped(char c, long trigTime) {
            }
        };
    }
}