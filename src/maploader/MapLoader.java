package maploader;

import game_object.GameObject;


import java.io.IOException;
import java.util.ArrayList;

public class MapLoader {

    private final ArrayList<int[][]> mapArr;
    private final ArrayList<String[]> txtArr;

    public MapLoader(String MapPath, String txtPath) throws IOException { //txt檔名  bmp檔名
        mapArr = new ReadBmp().readBmp(MapPath);
        txtArr = new ReadFile().readFile(txtPath);
    }

    public  interface CompareClass {
        GameObject compareClassName(String gameObject, String name, MapInfo mapInfo, int MapObjectSize);
    }

//    public ArrayList<GameObject> creatObjectArray(String gameObject, int mapObjectSize, ArrayList<MapInfo> mapInfo, CompareClass compare) {
//        //創建一個暫時的遊戲物件arraylist
//        ArrayList<GameObject> tmp = new ArrayList();
//        //傳進來的mapinfo Arraylist 去比較
//        mapInfo.forEach((e) -> {
//            GameObject tmpObject = compare.compareClassName(gameObject, e.getName(), e, mapObjectSize);
//            if (tmpObject != null) {
//                tmp.add(tmpObject);
//            }
//        });
//        return tmp;
//    }

    public ArrayList<MapInfo> combineInfo() {  //整合需要資料   類名  x座標  y座標 尺寸(e.g. 1 * 1)
        ArrayList<MapInfo> mapInfos = new ArrayList<MapInfo>();
        //有幾個物件
        for (int i = 0; i < mapArr.size(); i++) {
            //有幾個物件
            for (int j = 0; j < txtArr.size(); j++) {
                //               pixel                                pixel
                if (mapArr.get(i)[1][0] == Integer.parseInt(txtArr.get(j)[1])) {
                    //如果一樣
                    MapInfo tmp =
                            //                 檔案名字
                            new MapInfo(txtArr.get(j)[0],
                            //            x軸位置
                            mapArr.get(i)[0][0],
                            //            y軸位置
                            mapArr.get(i)[0][1],
                            //                      width
                            Integer.parseInt(txtArr.get(j)[2]),
                            //                       height
                            Integer.parseInt(txtArr.get(j)[3]));
                    //加入陣列
                    mapInfos.add(tmp);
                }
            }
        }
        return mapInfos;
    }

    public ArrayList<int[][]> getMapArr() {
        return mapArr;
    }

    public ArrayList getTxtArr() {
        return txtArr;
    }

}
