package maploader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadBmp {

    public  ArrayList<int[][]> readBmp(String path) {
        //放int二維陣列的arraylist
        ArrayList<int[][]> rgbArr = new ArrayList();
        BufferedImage bi = null;

        //先抓到那個bmp圖片
        try {
            bi = ImageIO.read(getClass().getResource(path));
        } catch (IOException ex) {
            Logger.getLogger(ReadBmp.class.getName()).log(Level.SEVERE, null, ex);
        }

        //設置寬高,位置
        int width = bi.getWidth();
        int height = bi.getHeight();
        int minx = bi.getMinX();
        int miny = bi.getMinY();
        //地圖的x軸從minx開始畫到width
        for (int i = minx; i < width; i++) {
            //地圖的y軸從miny開始畫到height
            for (int j = miny; j < height; j++) {
                //創建一個int的二維陣列
                int[][] rgbContent = new int[2][];
                //pixel就是 地圖bi中 x:i , y:j 那個位置的顏色色號
                int pixel = bi.getRGB(i, j);
                //二維陣列的第一格存入該圖的x,y位置
                rgbContent[0] = new int[]{i, j};
                //二維陣列的第二格存入那個色號
                rgbContent[1] = new int[]{pixel};
                //加入要回傳的 二維陣列 陣列(Arraylist)
                rgbArr.add(rgbContent);
            }
        }
        //可以得到該地圖中,每個位置(x,y)對應的色號(pixel)
        return rgbArr;
    }
}
