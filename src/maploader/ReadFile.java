package maploader;

import java.io.*;
import java.util.ArrayList;
import java.util.function.Consumer;

public class ReadFile {

    public ArrayList<String[]> readFile(String path) throws IOException {
        //創建一個字串陣列
        ArrayList<String> tmp = new ArrayList();
        //pathName = 檔案路徑
        String pathName = MapLoader.class.getResource(path).getFile();
        pathName = java.net.URLDecoder.decode(pathName, "utf-8");
        //找到那個檔案
        File filename = new File(pathName);
        //讀取那個檔案
        InputStreamReader reader = new InputStreamReader(new FileInputStream(filename));
        //讀取那個檔名
        BufferedReader br = new BufferedReader(reader);
        BufferedReader brr = new BufferedReader(new InputStreamReader(new FileInputStream(new File(pathName))));
        String line = "";
        //從該檔案讀取到的東西存入字串陣列
        while ((line = br.readLine()) != null) {
            tmp.add(line);
            // line = block1, -52 , C:\Users\ASUS\Documents\bgimgs\block-81.gif , 1, 1
            //        檔案名字  色號            檔案路徑                            檔案大小(w,h)
        }
        //要回傳的陣列 , 裡面每個元素都是一個字串陣列
        ArrayList<String[]> filterArr = new ArrayList();

        tmp.forEach(new Consumer() {
            @Override
            public void accept(Object a) {
                String[] tmp = new String[4];
                tmp[0] = ((String) a).split(",")[0];//存入檔案名字
                tmp[1] = ((String) a).split(",")[1];//存入色號
                tmp[2] = ((String) a).split(",")[3];//存入width
                tmp[3] = ((String) a).split(",")[4];//存入height
                filterArr.add(tmp);//進入陣列
            }
        });
        return filterArr;
    }
}
