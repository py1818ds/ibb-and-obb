package menu;

import utils.Delay;

import java.awt.*;
import java.awt.event.KeyEvent;

import static utils.Global.UPDATE_TIME_PER_SEC;

public class EditText extends Label {

    private String defaultText;
    private boolean isEditable; //是否為可編輯狀態

    //cursor
    private Delay cursorSpeed;
    private Color cursorColor;
    private boolean isCursorLight;
    private int cursorWidth;
    private int cursorHeight;

    //edit
    private int editLimit;
    private String editText;

    private void init(String hint) {
        isEditable = true;
        editLimit = 20;
        editText = "";
        this.defaultText = hint;
        //cursor
        cursorWidth = 3;
        cursorHeight = 15;
        cursorColor = Color.BLACK;
        cursorSpeed = new Delay(UPDATE_TIME_PER_SEC / 2);
        cursorSpeed.loop();
        isCursorLight = false;
    }

    public EditText(int x, int y, String hint, Style styleRect) {
        super(x, y, styleRect);
        init(hint);
    }

    public EditText(int x, int y, String hint, Theme theme) {
        super(x, y, theme);
        init(hint);
    }

    public EditText(int x, int y, String hint) {
        super(x, y);
        init(hint);
    }

    @Override
    public void isFocus() {
        super.isFocus();
    }
    public void changeCursorIsLight() {
        isCursorLight = !isCursorLight;
    }

    //設輸入字數限制
    public void setEditLimit(int n) {
        editLimit = n;
    }

    private boolean isOverEditLimit() {
        return !(editLimit == -1 || editText.length() < editLimit);
    }

    private boolean isexcepion(char c) {
        if (c >= 33 && c <= 47
                || c >= 48 && c <= 64
                || c >= 91 && c <= 96
                || c >= 123 && c < 126) {
            return true;
        }
        return false;
    }

    private boolean isLegalRange(char c) {
        if (c >= 30 && c <= 39) {
            return true;
        }
        if (c >= 65 && c <= 90) {
            return true;
        }
        return c >= 97 && c <= 122;
    }

    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setFont(this.getPaintStyle().getTextFont()); //預設提示文字與輸入文字字型大小一樣
        g.setColor(Color.BLACK);
        g.drawString((editText.length() == 0 && getIsFocus()) ? defaultText : editText, super.getX(), super.getY() + super.height() / 2 + g.getFontMetrics().getDescent());

        if (super.getIsFocus() && isEditable) {
            if (cursorSpeed.count()) {
                changeCursorIsLight();
            }
            if (isCursorLight) {
                g.setColor(cursorColor);
                int stringWidth = g.getFontMetrics().stringWidth(editText);
                cursorHeight = g.getFontMetrics().getAscent();
                g.fillRect(super.getX() + stringWidth, super.getY() + super.height() / 2 - (cursorHeight + g.getFontMetrics().getDescent()) / 2, cursorWidth, cursorHeight);
            }
        }
    }

    public String getEditText(){
        return this.editText;
    }
        
    @Override
    public void update() {

    }

    public void keyTyped(char c) {
        if (getIsFocus() && isEditable) {
            if (c == KeyEvent.VK_ENTER) {
                super.unFocus();
            }
            if (isexcepion(c)) {
                editText = editText + c;
            }else if (c == KeyEvent.VK_BACK_SPACE) {
                if (editText.length() > 0) {
                    editText = editText.substring(0, editText.length() - 1);
                }
            } else if (!isOverEditLimit() && isLegalRange(c) && Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
                editText = editText + c;
            } else if (!isOverEditLimit() && c >= 65 && c <= 90) {
                editText = editText + (char) (c + 32);
            }
        }
    }
}
