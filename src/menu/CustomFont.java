package menu;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class CustomFont {

    private Font paperCutFont;
    public Font chalkFont;
    private Font jfFont;
    private Font xiaolaiFont;
    private Font pixelFont;

    public CustomFont(){
        try {
            File paperCutFile = new File("src/resources/font/paperCut.ttf");
            paperCutFont = Font.createFont(Font.TRUETYPE_FONT, paperCutFile);
            File chalkFile = new File("src/resources/font/Chalk.ttf");
            chalkFont = Font.createFont(Font.TRUETYPE_FONT, chalkFile);
            File jfFile = new File("src/resources/font/jf.ttf");
            jfFont = Font.createFont(Font.TRUETYPE_FONT, jfFile);
            File xiaolaiFile = new File("src/resources/font/Xiaolai.ttf");
            xiaolaiFont = Font.createFont(Font.TRUETYPE_FONT, xiaolaiFile);
            File pixelFile = new File("src/resources/font/pixel.ttf");
            pixelFont = Font.createFont(Font.TRUETYPE_FONT, pixelFile);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(paperCutFont);
        ge.registerFont(chalkFont);
        ge.registerFont(jfFont);
        ge.registerFont(xiaolaiFont);
        ge.registerFont(pixelFont);
    }
}



