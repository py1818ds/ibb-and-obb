package scene_process;

import utils.Global;
import game_object.*;
import utils.CommandSolver;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;


public class Camera extends GameObject implements CommandSolver.MouseCommandListener, CommandSolver.KeyCommandListener {

    private enum State {
        TARGET_MODE, //鏡頭追焦物件
        MOUSE_MODE //滑鼠控制鏡頭縮放
    }

    /**
     * 主要屬性
     */

    private final int imgWidth;//背景圖寬度(最大的那張底圖)
    private final int imgHeight;//背景圖高度(最大的那張底圖)

    private double chaseX;//追蹤除數(x軸)
    private double chaseY;//追蹤除數(y軸)

    private double sensitivityX;//靈敏度(x軸) 越小越放大越快
    private double sensitivityY;//靈敏度(y軸) 越小越放大越快
    private double thresholdX;
    private double thresholdY;



    private State cameraState;//鏡頭控制狀態

    //    private GameObject target;//主要追蹤對象
    private ArrayList<GameObject> targets;
    private int centerX;
    private int centerY;

    /**
     * 次要屬性
     */
    private double multiple;//放大倍率

    /**
     * 設定螢幕移動距離 與判定範圍
     */
    private int screenRange;
    private int screenStep;

    /**
     * 預設鏡頭為全景
     */
    public Camera(int inputImageWidth, int inputImageHeight) {
        super(0, 0, Global.SCREEN_X, Global.SCREEN_Y);
        this.chaseX = 1; //追焦初始值
        this.chaseY = 1; //追焦初始值
        this.imgWidth = inputImageWidth;
        this.imgHeight = inputImageHeight;
        this.multiple = 1d;//預設初始倍率
        this.cameraState = State.MOUSE_MODE;//預設初始狀態為滑鼠控制
        this.targets = new ArrayList<>();

    }

    /**
     * 可設定鏡頭大小
     */
    public Camera(int inputImageWidth, int inputImageHeight, int x, int y, int cameraWidth, int cameraHeight) {
        //需要兩個以上的鏡頭，可以設定該鏡頭的座標與寬高
        super(x, y, cameraWidth, cameraHeight);
        this.chaseX = 1; //追焦初始值
        this.chaseY = 1; //追焦初始值
        this.imgWidth = inputImageWidth;
        this.imgHeight = inputImageHeight;
        this.cameraState = State.MOUSE_MODE;//預設初始狀態為滑鼠控制
        //放大功能
        this.multiple = 1d;//預設初始倍率
        //捲軸功能
        this.screenRange = 100;
        this.screenStep = 10;
    }

    /**
     * 主要追蹤角色
     */
    public void setTarget(GameObject inputTrackTarget) {
//        this.target = inputTrackTarget;
        this.cameraState = State.TARGET_MODE;
        targets.add(inputTrackTarget);
    }

    /**
     * 追蹤X&Y軸的速度除數
     */ //數字越大移動越慢
    public void setChase(int inputChaseX, int inputChaseY) {
        this.chaseX = inputChaseX;
        this.chaseY = inputChaseY;
    }

    /* 追蹤X,Y軸的速度除數 數字越大移動越慢 */
    public void setChaseX(int chaseX) {
        this.chaseX = chaseX;
    }
    public void setChaseY(int chaseY) {
        this.chaseY = chaseY;
    }

    /* 放大倍率靈敏度參數設定 */
    public void setSensitivityX(int sensitivityX, int thresholdX){
        this.sensitivityX = sensitivityX;
        this.thresholdX = thresholdX;
    }
    public void setSensitivityY(int sensitivityY, int thresholdY){
        this.sensitivityY = sensitivityY;
        this.thresholdY = thresholdY;
    }
    public void setSensitivity(int sensitivity, int threshold){
        sensitivityX = sensitivity;
        sensitivityY = sensitivity;
        thresholdX = threshold;
        thresholdY = threshold;
    }


    /* 開始追焦目標物件 */
    public void trackTarget() {

        int dX = (int) ((centerX - collider().centerX()) / chaseX);//鏡頭X軸移動距離
        int dY = (int) ((centerY - collider().centerY()) / chaseY);//鏡頭Ｙ軸移動距離

        //鏡頭還沒碰到最左邊＆最右邊
        if (collider().left() + dX >= 0 && collider().right() + dX <= imgWidth) {
            translateX(dX);
        }

        //鏡頭還沒碰到最上面＆最下面
        if (collider().top() + dY >= 0 && collider().bottom() + dY <= imgHeight) {
            translateY(dY);
        }

        //如果一開始就破圖>>移回來
        if (collider().left() < 0) {
            translateX(-collider().left());
        }

        if (collider().right() > imgWidth) {
            translateX(imgWidth - collider().right());
        }

        if (collider().top() < 0) {
            translateY(-collider().top());
        }

        if (collider().bottom() > imgHeight) {
            translateY(imgHeight - collider().bottom());
        }
    }

    /**
     * 開始使用鏡頭
     */
    public void startCamera(Graphics2D g2d) {
        countMultiple();
//        Graphics2D g2d = (Graphics2D) g;
        g2d.scale(multiple, multiple);

//        System.out.println("置中偏移量 x :  " + (Global.SCREEN_X - Global.SCREEN_X * multiple) / 2);
        g2d.translate(-painter().left(), -painter().top());
        int dx = (int) ((Global.WINDOW_WIDTH - Global.WINDOW_WIDTH * multiple) / 2);
        int dy = (int) ((Global.WINDOW_HEIGHT - Global.WINDOW_HEIGHT * multiple) / 2);
//        g2d.translate(dx/multiple, dy/multiple);
        g2d.translate(dx/multiple*(2d/3d), dy/multiple);
    }

    /**
     * 結束鏡頭
     */ //畫布回歸原始位置
    public void endCamera(Graphics2D g2d) {
//        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(painter().left(), painter().top());
//        g2d.translate(painter().left() * multiple, painter().top() * multiple);
        double dx = ((Global.SCREEN_X - Global.SCREEN_X * multiple) / 2);
        double dy = ((Global.SCREEN_Y - Global.SCREEN_Y * multiple) / 2);

//        g2d.translate(-dx, -dy);
        g2d.translate(-dx/multiple*(2d/3d), -dy/multiple);
        g2d.scale(1 / multiple, 1 / multiple);
//        g2d.dispose();
    }


    //陽春寫法，待重構
    public void countMultiple() {
        if (targets.size() <= 1 || sensitivityX<=0 || sensitivityY<=0) {
            return;
        }
        int maxX = 0;
        int minX = 99999999;
        int maxY = 0;
        int minY = 99999999;
        for (GameObject target : targets) {
            maxX = Math.max(maxX, target.painter().centerX());
            minX = Math.min(minX, target.painter().centerX());
            maxY = Math.max(maxY, target.painter().centerY());
            minY = Math.min(minY, target.painter().centerY());
        }

//        double exp = Math.max((maxX - minX) / 10d - 200, (maxY - minY) / 7d - 20);
        double exp = Math.max((maxX - minX) / sensitivityX - thresholdX, (maxY - minY) / sensitivityY - thresholdY);
        multiple = Math.pow(0.995, Math.max(exp, 0));
//        multiple = 1 - exp * 0.003;
    }

    ;

    /**
     * 每次移動鏡頭邏輯更新
     */
    @Override
    public void update() {
        if (cameraState == State.TARGET_MODE) {
            centerX = 0;
            centerY = 0;
            for (GameObject target : targets) {
                centerX += target.painter().centerX();
                centerY += target.painter().centerY();
            }
            centerX /= targets.size();
            centerY /= targets.size();
            trackTarget(); // 追焦功能
        }
        //  System.out.println("倍率:" + multiple);
    }

    @Override
    public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
        if (this.cameraState == State.MOUSE_MODE) {
            if (state == CommandSolver.MouseState.MOVED) {
                // 在MOUSE_MODE中，預設滑鼠在地圖四角移動，鏡頭朝該方向移動 -> 設定偵測範圍&移動距離
                moveScreen(e);
            }
        }
    }

    @Override
    public void keyPressed(int commandCode, long trigTime) {
    }

    @Override
    public void keyReleased(int commandCode, long trigTime) {
//        if(commandCode == KeyEvent.VK_Z) {
//            //按下Z放大畫面
//            multiple += 0.1;
//        }
//        if(commandCode == KeyEvent.VK_X) {
//            //按下X縮小畫面
//            multiple -= 0.1;
//        }
//        if(commandCode == KeyEvent.VK_A) {
//            //按下A切換鏡頭追焦畫面
//            if(cameraState == State.MOUSE_MODE) {
//                this.cameraState = State.TARGET_MODE;
//            } else {
//                this.cameraState = State.MOUSE_MODE;
//            }
//        }
    }

    /**
     * 在Debug模式時，畫出鏡頭外框
     */
    @Override
    public void paintComponent(Graphics g) {
        if(Global.IS_DEBUG){
        g.setColor(Color.RED);
        g.drawRect(painter().left(), painter().top(), painter().getWidth(), painter().getHeight());
        }
        g.setColor(new Color(255, 255, 255, 255));
        g.setFont(new Font("Zpix", Font.PLAIN, 20));
        if (targets.get(0).painter().bottom() < painter().top()) {
            g.drawString("JIMMY", targets.get(0).painter().centerX(), painter().top() + 50);
        }

        if (targets.get(0).painter().top() > painter().bottom()) {
            g.drawString("JIMMY", targets.get(0).painter().centerX(), painter().bottom() - 70);
        }

        if (targets.get(1).painter().bottom() < painter().top()) {
            g.drawString("AMY", targets.get(1).painter().centerX(), painter().top() + 50);
        }

        if (targets.get(1).painter().top() > painter().bottom()) {
            g.drawString("AMY", targets.get(1).painter().centerX(), painter().bottom() - 50);
        }
//        g.drawString("AMY", targets.get(1).painter().centerX(), 1500);

    }

    /**
     * 設定螢幕移動距離 與判定範圍
     */
    public void setScreenMovingRange(int screenRange, int screenStep) {
        this.screenRange = screenRange;
        this.screenStep = screenStep;
    }

    /**
     * 移動螢幕
     */
    private void moveScreen(MouseEvent e) {
        int dX = 0;
        int dY = 0;
        if (e.getX() > Global.SCREEN_X - screenRange) {
            dX = screenStep;
        }
        if (e.getX() < screenRange) {
            dX = -screenStep;
        }
        if (e.getY() > Global.SCREEN_Y - screenRange) {
            dY = screenStep;
        }
        if (e.getY() < screenRange) {
            dY = -screenStep;
        }
        translate(dX, dY);

        if (collider().left() <= 0) {
            translateX(-dX);
        }
        if (collider().right() >= imgWidth) {
            translateX(-dX);
        }
        if (collider().top() <= 0) {
            translateY(-dY);
        }
        if (collider().bottom() >= imgHeight) {
            translateY(-dY);
        }
    }
}