package game;

import utils.GI;
import utils.GameKernel;
import utils.Global;
import utils.Path;

import javax.swing.*;
import java.awt.event.KeyEvent;

import static utils.Global.*;

public class Main {

    public static void main(String[] args) {

        System.out.println(new Path().img().objs().gravityBall());

        JFrame jFrame = new JFrame();// 創建JFrame物件
        jFrame.setTitle("ibb & obb");// 設定視窗上方的標題
        jFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);// 設定視窗大小
        jFrame.setResizable(false);// 鎖定視窗大小避免自行修改

        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 當按下右上角的關閉時會關閉整個程式，否則程式會繼續運行

        int[][] commands = {
                {KeyEvent.VK_LEFT, LEFT},
                {KeyEvent.VK_RIGHT, RIGHT},
                {KeyEvent.VK_UP, UP},
                {KeyEvent.VK_SPACE, SPACE},
                {KeyEvent.VK_A, P2_LEFT},
                {KeyEvent.VK_D, P2_RIGHT},
                {KeyEvent.VK_W, P2_UP},
                {KeyEvent.VK_CONTROL, CTRL},
                {KeyEvent.VK_ESCAPE,ESC },
        };


        GI gi = new GI(); //遊戲本體(邏輯+畫面處理)

        GameKernel gk = new GameKernel.Builder(gi, LIMIT_DELTA_TIME, NANOSECOND_PER_UPDATE)
                .initListener(commands) //鍵盤指令集
                .enableMouseTrack(gi) //滑鼠追蹤
                .enableKeyboardTrack(gi)
                .trackChar()
                .keyCleanMode()
                .gen();

        jFrame.add(gk); //視窗本體

        jFrame.setVisible(true);// 顯示視窗

        gk.run(IS_DEBUG);
    }
}
