package controllers;

import scene.Scene;
import utils.CommandSolver;

import java.awt.*;

public class SceneController {

    private static SceneController sceneController;

    private SceneController() {
        lastIrc = new ImageResourceController();
        currentIrc = new ImageResourceController();
        lastMrc = new MusicResourceController();
        currentMrc = new MusicResourceController();
    }

    public static SceneController instance() {
        if (sceneController == null) {
            sceneController = new SceneController();
        }
        return sceneController;
    }

    private Scene currentScene;
    private Scene lastScene;
    private ImageResourceController lastIrc;
    private ImageResourceController currentIrc;
    private MusicResourceController currentMrc;
    private MusicResourceController lastMrc;

    public void changeScene(Scene newScene) {
        if (newScene != null) {
            newScene.sceneBegin();
        }
        lastScene = currentScene;

        ImageResourceController tmp = currentIrc;
        currentIrc = lastIrc;
        lastIrc = tmp;

        currentScene = newScene;
    }

    public Scene getCurrentScene() {
        return currentScene;
    }


    public void paint(Graphics g) {
        if (currentScene != null) {
            currentScene.paint(g);
        }
    }
    public void update() {
        if (lastScene != null) {
            lastScene.sceneEnd();
            lastIrc.clear();
            lastScene = null;
        }
        if (currentScene != null) {
            currentScene.update();
        }
    }


    public CommandSolver.MouseCommandListener mouseListener() {
        return currentScene.mouseListener();
    }


    public CommandSolver.KeyListener keyListener() {
        return currentScene.keyListener();
    }

    public ImageResourceController irc() {
        return currentIrc;
    }

    public MusicResourceController mrc() {
        return currentMrc;
    }


}
