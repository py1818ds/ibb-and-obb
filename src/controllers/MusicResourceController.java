package controllers;

import utils.Global;

import java.applet.Applet;
import java.applet.AudioClip;
import java.util.ArrayList;

public class MusicResourceController {
    //內部類別 存放路徑跟音樂實體
    private static class KeyPair {
        private String path;
        private AudioClip bgm;

        //path = 路徑
        public KeyPair(String path, AudioClip bgm) {
            this.path = path;
            this.bgm = bgm;
        }
    }

    //內容
    private ArrayList<KeyPair> bgmPairs;

    //private建構子, 因為希望用單例來創建
    public MusicResourceController() {
        bgmPairs = new ArrayList<>();
    }


    public AudioClip tryGetMusic(String path) {
        KeyPair pair = findKeyPair(path);
        if (pair == null) {
            return addMusic(path);
        }
        return pair.bgm;
    }

    private AudioClip addMusic(String path) {
        try {
            if (Global.IS_DEBUG) {
                System.out.println("load img from:" + path);
            }
            AudioClip bgm = Applet.newAudioClip(getClass().getResource(path));
            bgmPairs.add(new KeyPair(path, bgm));
            return bgm;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private KeyPair findKeyPair(String path) {
        for (int i = 0; i < bgmPairs.size(); i++) {
            KeyPair pair = bgmPairs.get(i);
            if (pair.path.equals(path)) {
                return pair;
            }
        }
        return null;
    }

    public void clear(){
        bgmPairs.clear();
    }
}
