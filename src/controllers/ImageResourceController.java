package controllers;


import utils.Global;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;


public class ImageResourceController {
    //內部類別 存放路徑跟圖片實體
    private static class KeyPair {
        private String path;
        private Image img;

        //path = 路徑
        public KeyPair(String path, Image img) {
            this.path = path;
            this.img = img;
        }
    }

    //內容
    private ArrayList<KeyPair> imgPairs;

    //private建構子, 因為希望用單例來創建
    public ImageResourceController() {
        imgPairs = new ArrayList<>();
    }


    public Image tryGetImage(String path) {
        KeyPair pair = findKeyPair(path);
        if (pair == null) {
            return addImage(path);
        }
        return pair.img;
    }

    private Image addImage(String path) {
        try {
            if (Global.IS_DEBUG) {
                System.out.println("load img from:" + path);
            }
            Image img = ImageIO.read(getClass().getResource(path));
            imgPairs.add(new KeyPair(path, img));
            return img;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private KeyPair findKeyPair(String path) {
        for (int i = 0; i < imgPairs.size(); i++) {
            KeyPair pair = imgPairs.get(i);
            if (pair.path.equals(path)) {
                return pair;
            }
        }
        return null;
    }

    public void clear(){
        imgPairs.clear();
    }

}
