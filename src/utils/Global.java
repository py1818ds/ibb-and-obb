package utils;

import java.util.ArrayList;

public class Global {
    public enum Direction {
        UP(3),
        DOWN(0),
        LEFT(1),
        RIGHT(2),
        NONE(4);

        private int value;

        Direction(int value) {
            this.value = value;
        }

        public int getValue(){
            return value;
        }
    }


    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int UP = 3;
    public static final int NONE = 4;
    public static final int SPACE = 5;
    public static final int P2_LEFT = 11;
    public static final int P2_RIGHT = 12;
    public static final int P2_UP = 13;
    public static final int P2_NONE = 14;
    public static final int CTRL = 15;
    public static final int ESC = 16;

    public static final boolean IS_DEBUG = true;

    //地圖物件單位大小
    public static final int UNIT_X = 32;
    public static final int UNIT_Y = 32;
    public static final int MAP1_X = 23040;
    public static final int MAP2_Y = 1920;

    //角色相關常數
    public static final int JIMMY_X = 48;
    public static final int MAX_DISTANCE = 1500;

    public static final int ROLE_SPEED_X = 10;

    //地圖單位大小
    public static final int MAP_UNIT_X = 32;
    public static final int MAP_UNIT_Y = 32;

    //視窗大小
    public static final int WINDOW_WIDTH = 1280;
    public static final int WINDOW_HEIGHT = 720;
    public static final int SCREEN_X = WINDOW_WIDTH - 8 - 8;
    public static final int SCREEN_Y = WINDOW_HEIGHT - 31 - 8;

    // 資料刷新時間
    public static final int UPDATE_TIME_PER_SEC = 50; //每秒更新60次遊戲邏輯
    public static final int NANOSECOND_PER_UPDATE = 1000000000 / UPDATE_TIME_PER_SEC;//每一次要花費的奈秒數
    // 畫面更新時間
    public static final int FRAME_LIMIT = 50;
    public static final int LIMIT_DELTA_TIME = 1000000000 / FRAME_LIMIT;

    //連線相關常數
    public static final int CONNECT = 1;
    public static final int MOVE = 2;
    public static final int HIT_ITEMS = 3;
    public static final int ENEMY_COLLISION = 4;
    public static final int CHECK_POSITION = 5;
    public static final int HIT_TREES = 6;
    public static final int HIT_FLOORS = 7;
    public static final int HIT_CROSSING_FLOORS = 8;
    public static final int HIT_JUMP_FLOORS = 9;
    public static final int HIT_TRAP_FLOORS = 10;
    public static final int CHARACTER_COLLISION =11;
    public static final int HIT_GRAVITYBALLS = 12;
    public static final int HIT_REBORN_POINTS = 13;
    public static final int UPDATE_FRAME = 14;
    public static final int STAND_ON_EACH_OTHER = 15;
    public static final int PUSH_EACH_OTHER = 16;
    public static final int START = 17;

    public static ArrayList<String> bale(String ...str){
        ArrayList<String> tmp=new ArrayList<String>();
        for(int i =0;i<str.length;i++){
            tmp.add(str[i]);
        }
        return tmp;
    }


    public static int random(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }
}
