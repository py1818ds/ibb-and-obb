package utils;

public class Delay {
    private int count;
    private int countLimit;
    private boolean isPause;
    private boolean isLoop;

    public Delay(int countLimit) {
        this.countLimit = countLimit;
        count = 0;
        isPause = true;
        this.isLoop = false;
    }

    public void setCountLimit(int limit) {
        this.countLimit = limit;
    }

    public void stop() {
        isPause = true;
        count = 0;
    }

    public void loop() {
        isPause = false;
        isLoop = true;
    }

    public void pause() {
        isPause = true;
    }

    public void play() {
        isPause = false;
        isLoop = false;
    }

    public boolean count() {
        if (isPause) {
            return false;
        }
        //count加到大於limit就歸0
        if (count >= countLimit) {
            if (isLoop) {
                count = 0;
            }else {
                stop();
            }
            return true;
        }
        count++;
        return false;
    }
}
