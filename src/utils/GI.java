package utils;

import controllers.SceneController;
import scene.*;

import java.awt.*;
import java.awt.event.MouseEvent;

public class GI implements GameKernel.GameInterface, CommandSolver.MouseCommandListener, CommandSolver.KeyListener {


    public GI() {
        SceneController.instance().changeScene(new MenuScene());

    }

    public Scene getScene() {
        return SceneController.instance().getCurrentScene();
    }

    @Override
    public void paint(Graphics g) {
        SceneController.instance().paint(g);
    }

    @Override
    public void update() {
        SceneController.instance().update();
    }

    @Override
    public void mouseTrig(MouseEvent e, CommandSolver.MouseState state, long trigTime) {
        CommandSolver.MouseCommandListener m1 = SceneController.instance().mouseListener();
        if (m1 != null) {
            m1.mouseTrig(e, state, trigTime);
        }
    }

    @Override
    public void keyPressed(int commandCode, long trigTime) {
        CommandSolver.KeyCommandListener k1 = SceneController.instance().keyListener();
        if (k1 != null) {
            k1.keyPressed(commandCode, trigTime);
        }

    }

    @Override
    public void keyReleased(int commandCode, long trigTime) {
        CommandSolver.KeyCommandListener k1 = SceneController.instance().keyListener();
        if (k1 != null) {
            k1.keyReleased(commandCode, trigTime);
        }
    }

    @Override
    public void keyTyped(char c, long trigTime) {
        CommandSolver.TypedListener t1 = SceneController.instance().keyListener();
        if (t1 != null) {
            t1.keyTyped(c, trigTime);
        }
    }
}
