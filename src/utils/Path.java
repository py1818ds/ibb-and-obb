package utils;


public class Path {

    public static abstract class Flow {
        private String path;

        public Flow(String path) {
            this.path = path;
        }

        public Flow(Flow flow, String path) {
            this.path = flow.path + path;
        }

        @Override
        public String toString() {
            return path;
        }
    }

    public static final String RESOURCES_FOLDER = "/resources";

    public static class Resources extends Flow {


        public Resources() {
            super("/resources");
        }

    }

    public static class ImageFile extends Flow {

        private ImageFile() {
            super(new Resources(), "/imgs");
        }

        public static class MenuFile extends Flow {
            private MenuFile(Flow flow) {
                super(flow, "/menu");
            }

            public String titleBackground() {
                return this + "/title-background.png";
            }

            public String darkBackground() {
                return this + "/background-dark.png";
            }

            public String titleButton() {
                return this + "/button.png";
            }

            public String button() {
                return this + "/button-back.png";
            }

            public String button_Hover() {
                return this + "/button-back-white.png";
            }

            public String title() {
                return this + "/logo_test.png";
            }

            public String rankingBackground() {
                return this + "/ranking.gif";
            }

            public String blankButton() {
                return this + "/blank.png";
            }

            public String transparentRect() {
                return this + "/rect.png";
            }

            public String rectHover() {
                return this + "/RectHover.png";
            }

            public String menuBG() {
                return this + "/city.jpeg";
            }

            public String createRoomBg() {
                return this + "/hell.jpeg";
            }

            public String jimmy() {
                return this + "/jimmy.png";
            }

            public String amy() {
                return this + "/amy.png";
            }

            public String dialogue() {
                return this + "/dialogue.png";
            }

            public String classroom() {
                return this + "/classroom.jpeg";
            }

            public String rankBackground() {
                return this + "/rankbg.jpeg";
            }

            public String amyGhost() {
                return this + "/amy_ghost.png";
            }

            public String crown() {
                return this + "/crown.png";
            }

            public String jimmyGhost() {
                return this + "/jimmy_ghost.png";
            }

            public String logoText() {
                return this + "/logoText.png";
            }

            public String back() {
                return this + "/back.png";
            }

            public String stop() {
                return this + "/back_alpha.png";
            }

        }

        public static class ActorFile extends Flow {
            private ActorFile(Flow flow) {
                super(flow, "/actors");
            }


            public String enemy_Fly() {
                return this + "/enemy_fly.png";
            }

            public String enemyChaser() {
                return this + "/enemy1.png";
            }

            public String enemy_Pumpkin() {
                return this + "/enemy_pumpkin.png";
            }

            public String enemyHorizontal() {
                return this + "/enemy_slime.png";
            }

            public String white() {
                return this + "/white.png";
            }

            public String amy() {
                return this + "/amy.png";
            }

            public String animationRole() {
                return this + "/animation_role.png";
            }

            public String jimmy() {
                return this + "/jimmy.png";
            }

            public String boss() {
                return this + "/boss.png";
            }

        }

        public static class ObjectFile extends Flow {
            private ObjectFile(Flow flow) {
                super(flow, "/objs");
            }

            public String coin() {
                return this + "/coin-type.png";
            }

            public String gravityBall() {
                return this + "/gravity_ball.png";
            }

            public String trapFloor() {
                return this + "/brick.png";
            }

            public String bell() {
                return this + "/bell.png";
            }

            public String star() {
                return this + "/star.png";
            }

            public String alcohol() {
                return this + "/alcohol.png";
            }

            public String axe() {
                return this + "/axe.png";
            }

            public String hook() {
                return this + "/hook.png";
            }

        }


        public static class BackgroundFile extends Flow {
            private BackgroundFile(Flow flow) {
                super(flow, "/backgrounds");
            }

            public String floor() {
                return this + "/floor_type12.png";
            }

            public String jumpFloor() {
                return this + "/jumpFloor.png";
            }

            public String canCross() {
                return this + "/cross-3x2.png";
            }

            public String blackHole() {
                return this + "/blackhole.png";
            }

            public String tree() {
                return this + "/tree.png";
            }

            public String flag() {
                return this + "/flag.png";
            }

            public String grass() {
                return this + "/grass.png";
            }


        }

        public ActorFile actors() {
            return new ActorFile(this);
        }

        public ObjectFile objs() {
            return new ObjectFile(this);
        }

        public MenuFile menu() {
            return new MenuFile(this);
        }

        public BackgroundFile backgrounds() {
            return new BackgroundFile(this);
        }
    }

    public static class Sounds extends Flow {
        private Sounds() {
            super(new Resources(), "/sounds");
        }

        public String gravityBallSound() {
            return this + "/GravityBallSound.wav";
        }

        public String jump() {
            return this + "/jump.wav";
        }

        public String coin() {
            return this + "/coin.wav";
        }

        public String bell() {
            return this + "/bell.wav";
        }

        public String dead() {
            return this + "/Darkness4.wav";
        }

        public String beatEnemy() {
            return this + "/beatEnemy.wav";
        }

        public String victory() {
            return this + "/Victory1.wav";
        }

        public String teachLevelMusic() {
            return this + "/teachLevel.wav";
        }

        public String levelOneMusic() {
            return this + "/levelOne.wav";
        }

        public String levelTwoMusic() {
            return this + "/levelTwo.wav";
        }

        public String levelThreeMusic() {
            return this + "/levelThree.wav";
        }

        public String menuMusicOne() {
            return this + "/menuMusic1.wav";
        }

        public String menuMusicTwo() {
            return this + "/menuMusic2.wav";
        }

        public String overScene() {
            return this + "/overScene.wav";
        }

        public String competitionLevelMusic() {
            return this + "/competitionLevel.wav";
        }

        public String powerUp() {
            return this + "/Powerup.wav";
        }

        public String speedUP() {
            return this + "/drinkSpeed.wav";
        }

        public String equip() {
            return this + "/Equip.wav";
        }

        public String die() {
            return this + "/Darkness4.wav";
        }

        public String useAxe() {
            return this + "/swing2.wav";
        }


    }

    public ImageFile img() {
        return new ImageFile();
    }

    public Sounds sounds() {
        return new Sounds();
    }
}
