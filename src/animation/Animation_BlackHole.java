package animation;

import controllers.SceneController;
import utils.Delay;
import utils.Path;

import java.awt.*;

public class Animation_BlackHole {

    private final Image img;
    private final Delay delay;
    private int count;

    public Animation_BlackHole() {
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().blackHole());
        delay = new Delay(20);
        delay.loop();
        count = 0;

    }


    public void paint(int left, int top, int right, int bottom, Graphics g) {
        g.drawImage(img, left, top, right, bottom,
                64 * count,
                0,
                64 + 64 * count,
                96,
                null);
    }


    public void update() {
        if (delay.count()) {
            count++;
            count %= 3;
        }
      //  System.out.println("c="+count);

    }
}
