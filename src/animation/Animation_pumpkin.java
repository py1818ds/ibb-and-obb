package animation;

import controllers.SceneController;
import utils.Delay;
import utils.Path;

import java.awt.*;

public class Animation_pumpkin {

    private final Image img;
    private final Delay delay;
    private int count;

    public Animation_pumpkin() {
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().enemy_Pumpkin());
        delay = new Delay(20);
        delay.loop();
        count = 0;

    }


    public void paint(int left, int top, int right, int bottom, Graphics g) {
        g.drawImage(img, left, top, right, bottom,
                32 * count,
                0,
                32 + 32 * count,
                32,
                null);
    }


    public void update() {
        if (delay.count()) {
            count++;
            count %= 2;
        }


    }
}
