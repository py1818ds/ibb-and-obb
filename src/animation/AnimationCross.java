package animation;

import controllers.SceneController;
import utils.Delay;
import utils.Global;
import utils.Path;

import java.awt.*;

public class AnimationCross {

    private final Image img;
    private final Delay delay;
    private int count;

    public AnimationCross() {
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().canCross());
        delay = new Delay(10);
        delay.loop();
        count = 0;

    }


    public void paint(int left, int top, int right, int bottom, Graphics g) {
        g.drawImage(img, left, top, right, bottom,
                Global.MAP_UNIT_X*3 * count,
                0,
                Global.MAP_UNIT_X*3 + Global.MAP_UNIT_X*3 * count,
                Global.MAP_UNIT_Y*2,
                null);
    }


    public void update() {
        if (delay.count()) {
            count++;
            count %= 3;
        }
      //  System.out.println("c="+count);

    }
}
