package animation;

import controllers.SceneController;
import utils.Global;
import utils.Path;

import java.awt.*;

public class Animation_Floor {

    // type :
    // 0 -> 水平
    // 1 -> 垂直面右
    // 2 -> 左上角
    // 3 -> 右下角
    // 4 -> 垂直面左
    // 5 -> 右上角
    // 6 -> 左下角

    private final Image img;
    private int type;

    public Animation_Floor(int type) {
        img = SceneController.instance().irc().tryGetImage(new Path().img().backgrounds().floor());
        this.type = type;
    }

    public void paint(int left, int top, int right, int bottom, Graphics g) {
        g.drawImage(img, left, top, right, bottom,
                type  * (Global.MAP_UNIT_X ),
                0,
                type  * (Global.MAP_UNIT_X ) + Global.MAP_UNIT_X,
                Global.MAP_UNIT_Y,
                null);
    }


    public void update() {

    }
}
