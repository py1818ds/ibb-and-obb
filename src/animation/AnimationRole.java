package animation;

import controllers.SceneController;
import utils.Delay;
import utils.Global;
import utils.Path;

import game_object.characters.Character.AnimationState;

import java.awt.*;



//切割圖片用
public class AnimationRole {
    /* Enum */

    public enum AnimationArea{
        JIMMY(0),
        AMY(1);
        private final int animationArea;
        AnimationArea(int inputAnimationArea) {
            this.animationArea = inputAnimationArea;
        }
        public int area(){
            return animationArea;
        }
    }

    private final Image img;
    private final Delay delay;
    private final int shakeNumberLength;

    /* 根據狀態抓圖 */
    private final AnimationArea animationArea;//不同的角色使用不同的區域圖 固定起來
    private int shakeNumber;

    public AnimationRole(AnimationArea animationArea) {
        //給一張所有角色方向狀態合併的圖
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().animationRole());
        delay = new Delay(20);
        delay.loop();
        this.animationArea = animationArea;
        shakeNumberLength = 2;
        shakeNumber = 0;
    }

    //把後面的圖畫在前面給的位置
    public void paint(AnimationState animationState, int left, int top, int right, int bottom, Graphics g) {
        int initialCoordinateY = animationArea.area() * shakeNumberLength * Global.UNIT_Y;
        //左上的點
        int x1 = Global.UNIT_X * animationState.animationX();
        int y1 = initialCoordinateY + Global.UNIT_Y * shakeNumber;

        //右下的點
        int x2 = x1 + Global.UNIT_X;
        int y2 = y1 + Global.UNIT_Y;

        g.drawImage(img, left, top, right, bottom, x1, y1, x2, y2,null);
    }


    public void update() {
        //抖動用
        if (delay.count()) {
            shakeNumber++;
            shakeNumber %= shakeNumberLength;//根據抖動圖的數量
        }
    }
}