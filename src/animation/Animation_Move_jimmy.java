package animation;

import controllers.SceneController;
import utils.Delay;
import utils.Global;
import utils.Path;

import java.awt.*;

public class Animation_Move_jimmy {

    public enum State {
        UP(1),
        DOWN(0);

        private int value;

        State(int value) {
            this.value = value;
        }
    }

    private final Image img;
    private final Delay delay;
    private int count;
    private State state;
    private int type;

    public Animation_Move_jimmy(int type, State state) {
        img = SceneController.instance().irc().tryGetImage(new Path().img().actors().jimmy());
        delay = new Delay(20);
        delay.loop();
        count = 0;
        this.state = state;
        this.type = type;
    }

    public final void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void paint(Global.Direction dir, int left, int top, int right, int bottom, Graphics g) {
        int v = 0;
        if (dir.getValue() == 3) {
            v = 1;
        }

        g.drawImage(img, left, top, right, bottom,
                (Global.UNIT_X * count),
                Global.UNIT_Y * dir.getValue() * state.value ,
                Global.UNIT_X + (Global.UNIT_X * count),
                Global.UNIT_Y + Global.UNIT_Y * dir.getValue() * state.value ,
                null);
    }


    public void update() {

        if (delay.count()) {
            count++;
            count %= 2;
        }

    }
}
